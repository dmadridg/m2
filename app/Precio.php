<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Precio extends Model {

    /**
     * Generated
     */

    protected $table = 'precios';
    protected $fillable = ['IdPrecio', 'IdModelo', 'Precio1', 'Precio2', 'Precio3', 'Precio4', 'Precio5'];



}
