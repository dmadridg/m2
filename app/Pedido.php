<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model {

    /**
     * Generated
     */

    protected $table = 'pedidos';
    protected $fillable = ['IdPedido', 'IdCotizacion', 'IdCliente', 'IdTransportista', 'FechaPedido', 'Estatus', 'Referencia', 'Observaciones', 'Lote'];



}
