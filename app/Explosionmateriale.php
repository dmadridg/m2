<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Explosionmateriale extends Model {

    /**
     * Generated
     */

    protected $table = 'explosionmateriales';
    protected $fillable = ['IdExplosionMaterial', 'Lote', 'SubLote', 'IdModelo', 'IdMaterial', 'IdProceso', 'Cantidad', 'EsTablero', 'Corte', 'Router', 'Curvos', 'Puertas', 'Molduras', 'Costados', 'Pantallas', 'Entrepanos', 'Divisiones', 'Archiveros', 'Chapas', 'PVC', 'Barrenos', 'Zoclos', 'Lado', 'Color1', 'Color2', 'Color3'];



}
