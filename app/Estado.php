<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model {

    /**
     * Generated
     */

    protected $table = 'estado';
    protected $fillable = ['IdEstado', 'NombreEstado', 'IdPais'];



}
