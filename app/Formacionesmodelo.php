<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Formacionesmodelo extends Model {

    /**
     * Generated
     */

    protected $table = 'formacionesmodelos';
    protected $fillable = ['IdFormacionModelo', 'IdModelo', 'IdMaterial', 'IdProceso', 'Cantidad', 'EsTablero', 'Corte', 'Router', 'Curvos', 'Puertas', 'Molduras', 'Costados', 'Pantallas', 'Entrepanos', 'Divisiones', 'Archiveros', 'Chapas', 'PVC', 'Barrenos', 'Zoclos','IdCategoriaColor'];
    protected $primaryKey = 'IdFormacionModelo';
    public $timestamps = false;
    public function process(){
        return $this->belongsTo('App\Proceso','IdProceso','IdProceso');
    }
    public function material(){
        return $this->belongsTo('App\Materiales','IdMaterial','IdMaterial');
    }
    public function categoriaColor() {
        return $this->belongsTo('App\Categoriascolores','IdCategoriaColor','IdCategoriaColor');
    }
}
