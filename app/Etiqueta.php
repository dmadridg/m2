<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Etiqueta extends Model {

    /**
     * Generated
     */

    protected $table = 'etiquetas';
    protected $fillable = ['IdEtiqueta', 'CodigoEtiqueta', 'IdCliente', 'IdTransportista'];



}
