<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Formacionesbasedetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'formacionesbasedetalle';
    protected $fillable = ['IdFormacionBaseDetalle', 'IdFormacionBase', 'IdMaterial', 'IdProceso', 'Cantidad', 'EsTablero', 'Corte', 'Router', 'Curvos', 'Puertas', 'Molduras', 'Costados', 'Pantallas', 'Entrepanos', 'Divisiones', 'Archiveros', 'Chapas', 'PVC', 'Barrenos', 'Zoclos'];
    protected $primaryKey = 'IdFormacionBaseDetalle';
    public $timestamps = false;
    public function process(){
        return $this->belongsTo('App\Proceso','IdProceso','IdProceso');
    }
    public function material(){
        return $this->belongsTo('App\Materiales','IdMaterial','IdMaterial');
    }
}
