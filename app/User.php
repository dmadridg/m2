<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;
    protected  $table = 'empleados';
    protected  $primaryKey = 'IdEmpleado';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email','nogin', 'password',
    ];
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%")->orderBy('IdEmpleado');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
