<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoriascolores extends Model {

    /**s
     * Generated
     */

    protected $table = 'categoriascolores';
    protected $fillable = ['IdCategoriaColor', 'Clave', 'Nombre', 'Estatus'];
    protected $primaryKey = 'IdCategoriaColor';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%")->orderBy('Clave');
    }
    public function colors(){
        return $this->belongsToMany('App\Colore', 'colorescategoriascolores', 'IdCategoriaColor', 'IdColor');
    }



}
