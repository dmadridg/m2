<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model {

    /**
     * Generated
     */

    protected $table = 'procesos';
    protected $fillable = ['IdProceso', 'NombreProceso'];
    protected $primaryKey = 'IdProceso';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('NombreProceso','LIKE',"%$name%");
    }


}
