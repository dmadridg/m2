<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model {

    /**
     * Generated
     */

    protected $table = 'lineas';
    protected $fillable = ['IdLinea', 'Codigo', 'Nombre', 'Descuento', 'Incremento'];
    protected $primaryKey = 'IdLinea';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%");
    }
}
