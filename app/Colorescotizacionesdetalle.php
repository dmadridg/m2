<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Colorescotizacionesdetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'colorescotizacionesdetalle';
    protected $fillable = ['IdColorCotizacionDetalle', 'IdCotizacionDetalle', 'IdCategoriasColores', 'IdColores', 'NombreColor'];



}
