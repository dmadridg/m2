<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Colore extends Model {

    /**
     * Generated
     */

    protected $table = 'colores';
    protected $fillable = ['IdColor', 'Nombre', 'Descripcion'];
    protected $primaryKey = 'IdColor';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%");
    }
}
