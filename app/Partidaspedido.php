<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Partidaspedido extends Model {

    /**
     * Generated
     */

    protected $table = 'partidaspedidos';
    protected $fillable = ['IdPartidaPedido', 'IdPartida', 'IdPedido'];



}
