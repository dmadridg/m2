<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Modelosmateriale extends Model {

    /**
     * Generated
     */

    protected $table = 'modelosmateriales';
    protected $fillable = ['IdModeloMaterial', 'IdModelo', 'IdMaterial'];



}
