<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Perfilespermiso extends Model {

    /**
     * Generated
     */

    protected $table = 'perfilespermisos';
    protected $fillable = ['IdPerfilesPermisos', 'IdPerfil', 'IdPermiso'];



}
