<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Formacionesbase extends Model {

    /**
     * Generated
     */

    protected $table = 'formacionesbase';
    protected $fillable = ['IdFormacionBase', 'Clave', 'Nombre', 'Estatus'];
    protected $primaryKey = 'IdFormacionBase';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%")->orderBy('Clave');
    }
    public function detail(){
        return $this->hasMany('App\Formacionesbasedetalle','IdFormacionBase','IdFormacionBase');
    }
}
