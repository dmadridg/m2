<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

    /**
     * Generated
     */

    protected $table = 'clientes';
    protected $primaryKey = 'IdCliente';
    protected $fillable = [ 'ClaveCliente', 'Nombre', 'TipoCliente', 'Descuento', 'IdEstado', 'IdCiudad', 'Telefono', 'RFC', 'Email', 'IdAgente', 'IdTransportista', 'Precio', 'DomicilioFiscal', 'CP', 'CURP', 'Contacto', 'Consignar', 'Zona', 'DomicilioEntrega', 'Condiciones'];
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%")->orderBy('ClaveCliente');
    }
    public function city(){
        return $this->belongsTo('App\Ciudad','IdCiudad','IdCiudad');
    }
    public function state(){
        return $this->belongsTo('App\Estado','IdEstado','IdEstado');
    }
    public function carrier() {
        return $this->belongsTo('App\Transportista','IdTransportista','IdTransportista');
    }
}
