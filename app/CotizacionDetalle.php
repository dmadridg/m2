<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CotizacionDetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'cotizacionesdetalle';
    protected $primaryKey = 'IdCotizacionDetalle';

    protected $fillable = ['IdCotizacion', 
        'IdModelo', 
        'Cantidad',
        'Precio', 
        'Area', 
        'Lados', 
        'Descripcion',
        'Descuento',
        'Adicional', 
        'SubTotal', 
        'Total', 
        'Observaciones', 
        'descripcionCategoriasColor', 
        'Medida', 
        'Tipo', 
        'Image',
        'colorBase', 
        'colorCombiando'];
    public function model(){
        return $this->belongsTo('App\Modelo','IdModelo','IdModelo');
    }


}
