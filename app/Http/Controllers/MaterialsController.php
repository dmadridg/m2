<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materiales;
class MaterialsController extends Controller
{
    public $materials;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->materials = Materiales::search($request->clave)->paginate(15)->appends(request()->query());
        foreach($this->materials as $key => $val) {
            if($this->materials[$key]->tablero != null) {
                if($this->materials[$key]->TipoMaterial == 'Componente') {
                    $costoTablero = (integer)$this->materials[$key]->tablero->Costo;
                    $this->materials[$key]->costo_calculado = (float)( ((integer)$this->materials[$key]->Largo * (integer)$this->materials[$key]->Ancho) * $costoTablero ) / 1000000;
                    $this->materials[$key]->costo_calculado = number_format($this->materials[$key]->costo_calculado,2,'.',',');
                }
            }
        }
        $this->materials->setPath('materials');
        $this->numRecords = count($this->materials);

        $this->data = ['materials'=>$this->materials,'title'=>'Listado de materiales','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron materiales';
        }
        return view('main.materials.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $materialTmp = Materiales::where('Clave',$request->Clave)->first();
        if($materialTmp != null) {
            return response()->json(['msg'=>'La clave que ingresaste para el material ya existe, intenta con una diferente'],401);
        }
        $material = new Materiales();
        if($request->typeMaterial == 'HERRAJE'){
            $material->fill($request->except(['Ancho','Largo','IdTablero']));
            $material->tipoMaterial = 'Herraje';
        }else{
            $material->fill($request->except(['IdUnidadMedida','CostoPorUnidad']));
            $material->tipoMaterial = 'Componente';
        }
        if ($material->save()){
            return response()->json(['msg'=>'Material creado correctamente']);
        }else{
            return response()->json(['msg'=>'No fue posible crear el material'],500);
        }
        return response()->json($material);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $material = Materiales::where('IdMaterial',$id)->with('tablero')->first();
            if($material->tablero != null) {
                if($material->TipoMaterial == 'Componente') {
                    $costoTablero = (integer)$material->tablero->Costo;
                    $material->costo_calculado = (float)( ((integer)$material->Largo * (integer)$material->Ancho) * $costoTablero ) / 1000000;
                    $material->costo_calculado = number_format($material->costo_calculado,2,'.',',');
                }
            }
            if($material != null){
                return response()->json($material);
            }else{
                return response()->json(['msg'=>"Material no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $material = Materiales::find($id);
        if($material != null){
            if($request->typeMaterial == 'HERRAJE'){

                $material->fill($request->except(['Ancho','Largo','IdTablero']));
                $material->tipoMaterial = 'Herraje';
            }else{

                $material->fill($request->except(['IdUnidadMedida','CostoPorUnidad']));
                $material->tipoMaterial = 'Componente';
            }
            if ($material->save()){
                return response()->json(['msg'=>'Material actualizado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible actualizar el material'],500);
            }
            return response()->json($material);
        }else{
            return response()->json(['msg'=>'Material no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
