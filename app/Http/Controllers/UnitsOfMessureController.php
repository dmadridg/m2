<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unidadesmedida;
class UnitsOfMessureController extends Controller
{
    public $units;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->units = Unidadesmedida::search($request->name)->paginate(5);
        $this->units->setPath('units');
        $this->numRecords = count($this->units);

        $this->data = ['units'=>$this->units,'title'=>'Listado de Unidades de medida','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron unidades de medida';
        }
        return view('main.units.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $unit = new Unidadesmedida();
            $unit->fill($request->all());
            if ($unit->save()){
                return response()->json(['msg'=>'Unidad de medida creada correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear la unidad de medida'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $unit = Unidadesmedida::find($id);
            if($unit != null){
                return response()->json($unit);
            }else{
                return response()->json(['msg'=>"Unida de medida no encontrada"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = Unidadesmedida::find($id);
        if($unit != null){
            $unit->fill($request->all());
            $unit->save();
            return response()->json(['msg'=>'La unidad de medida ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Unidad de medida no encontrada'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
