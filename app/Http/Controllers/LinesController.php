<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Linea;
class LinesController extends Controller
{
    public $lines;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->lines = Linea::search($request->name)->paginate(5);
        $this->lines->setPath('lines');
        $this->numRecords = count($this->lines);

        $this->data = ['lines'=>$this->lines,'title'=>'Listado de Lineas','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron lineas';
        }
        return view('main.lines.index')->with($this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $line = new Linea();
            $line->fill($request->all());
            if ($line->save()){
                return response()->json(['msg'=>'Linea creada correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear la linea'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $line = Linea::find($id);
            if($line != null){
                return response()->json($line);
            }else{
                return response()->json(['msg'=>"Linea no encontrada"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $line = Linea::find($id);
        if($line != null){
            $line->fill($request->all());
            $line->save();
            return response()->json(['msg'=>'La linea ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Linea no encontrada'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
