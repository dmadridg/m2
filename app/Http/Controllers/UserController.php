<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public $users;
    public $user;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->users = User::search($request->name)->paginate(10);
        $this->users->setPath('users');
        $this->numRecords = count($this->users);

        $this->data = ['users'=>$this->users,'title'=>'Listado de usuarios','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron usuarios';
        }
        return view('main.users.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main.users.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
           $this->user = new User();
           $this->user->Nombre = $request->Nombre;
           $this->user->login = $request->Email;
           $this->user->Email = $request->Email;
           $this->user->password = bcrypt($request->Password);
           $this->user->Cargo = $request->Cargo;
           $this->user->save();
           return response()->json(['msg'=>'Usuario  creado correctamente']);
       } catch(Exception $e) {
           return response()->json($e->getMessage(),500);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::find($id);
            if($user != null){
                return response()->json($user);
            }else{
                return response()->json(['msg'=>"Usuario no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if($user != null){
            $email = User::where('Email','=',$request->Email)->where('IdEmpleado','!=',$user->IdEmpleado)->first();
            if($email != null) {
                return response()->json(['msg'=>'El Email ingresado ya existe, intenta con otro.'],400);
            }
            $user->Nombre = $request->Nombre;
            $user->Email = $request->Email;
            $user->Cargo = $request->Cargo;

            if($request->EditPassword) {
                if(!$request->Password || trim($request->Password) == '') {
                    return response()->json(['msg'=>'No has enviado la nueva contraseña'],400);
                }
                $user->password = bcrypt($request->Password);
            }
            $user->save();
            return response()->json(['msg'=>'El usuario ha sido actualizado correctamente'],200);
        }else{
            return response()->json(['msg'=>'Usuario no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
