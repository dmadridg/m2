<?php

namespace App\Http\Controllers;

use Faker\Provider\Color;
use Illuminate\Http\Request;
use App\Colore;
class ColorsController extends Controller
{
    public $colors;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->colors = Colore::search($request->name)->paginate(10);
        $this->colors->setPath('colors');
        $this->numRecords = count($this->colors);

        $this->data = ['colors'=>$this->colors,'title'=>'Listado de Colores','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron colores';
        }
        return view('main.colors.index')->with($this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $color = new Colore();
            $color->fill($request->all());
            if ($color->save()){
                return response()->json(['msg'=>'Color creado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear Color'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $color = Colore::find($id);
            if($color != null){
                return response()->json($color);
            }else{
                return response()->json(['msg'=>"Color no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $color = Colore::find($id);
        if($color != null){
            $color->fill($request->all());
            $color->save();
            return response()->json(['msg'=>'El color ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Color no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
