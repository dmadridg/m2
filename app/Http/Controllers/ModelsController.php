<?php

namespace App\Http\Controllers;

use App\Formacionesbase;
use App\Formacionesmodelo;
use App\Http\Requests\ModelsRequest;
use Illuminate\Http\Request;
use App\Modelo;
use App\CotizacionDetalle;
use Illuminate\Http\File;
use Storage;
use Ramsey\Uuid\Uuid;
class ModelsController extends Controller
{
    public $models;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->models = Modelo::search($request->clave)->paginate(20)->appends(request()->query());
        $this->models->setPath('models');
        $this->numRecords = count($this->models);

        $this->models->getCollection()->transform(function ($model,$key) {
            $price10Percent = $model->Precio1 * .10;
            $price15Percent = $model->Precio1 * .15;
            $price20Percent = $model->Precio1 * .20;
            $model->Precio2 = $model->Precio1 + $price10Percent;
            $model->Precio3 = $model->Precio1 + $price15Percent;
            $model->Precio4 = $model->Precio1 + $price20Percent;
            $model->Precio1 = number_format($model->Precio1,2,'.',',');
            $model->Precio2 = number_format($model->Precio2,2,'.',',');
            $model->Precio3 = number_format($model->Precio3,2,'.',',');
            $model->Precio4 = number_format($model->Precio4,2,'.',',');
            return $model;
        });
        $this->data = ['models'=>$this->models,'title'=>'Listado de Productos','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron Productos';
        }
        return view('main.models.index')->with($this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function updateImage(Request $request,$id) {
      try {
          $model = Modelo::find($id);
          if($model != null){
              Storage::disk('modelsImages')->delete($model->Imagen);
              $file =  $request->file('file');
              $fileExtension = $file->getClientOriginalExtension();
              $filename = Uuid::uuid4().'.'.$fileExtension;
              Storage::disk('modelsImages')->putFileAs('/', $file,$filename);
              $model->update(['Imagen'=>$filename]);
              return response()->json(['msg'=>'La imagen ha sido actualizada correctamente']);

          }else{
              return response()->json(['msg'=>'Modelo no encontrado'],404);
          }
      }
        catch(Exception $e) {
            return response()->json($e->getMessage(),500);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModelsRequest $request)
    {
        try{

            $materialTmp = Modelo::where('Clave',$request->Clave)->first();
            if($materialTmp != null) {
                return response()->json(['msg'=>'La clave que ingresaste para el modelo ya existe, intenta con una diferente'],401);
            }
            $model = new Modelo();
            $model->fill($request->all(),['except'=>['file']]);
            //
            $file =  $request->file('file');

            $fileExtension = $file->getClientOriginalExtension();
            $filename = Uuid::uuid4().'.'.$fileExtension;
            Storage::disk('modelsImages')->putFileAs('/', $file,$filename);
            $model->Imagen = $filename;
            if ($model->save()){
                return response()->json(['msg'=>'modelo creado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear Modelo'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $model = Modelo::where('IdModelo','=',$id)->with(['formations','formations.process','formations.material'])->first();
            if($model != null){
                $price10Percent = $model->Precio1 * .10;
                $price15Percent = $model->Precio1 * .15;
                $price20Percent = $model->Precio1 * .20;
                $model->Precio2 = $model->Precio1 + $price10Percent;
                $model->Precio3 = $model->Precio1 + $price15Percent;
                $model->Precio4 = $model->Precio1 + $price20Percent;
                return response()->json($model);
            }else{
                return response()->json(['msg'=>"Modelo no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }
    public function detailForQuotation($id) {
        try {
            $model = Modelo::where('IdModelo','=',$id)
                ->with(['formations','line','formations','formations.categoriaColor','formations.categoriaColor.colors'])->first();
            return response()->json($model);
        }
        catch (Exception $e) {
            return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function removeModelFormation($idModel,$idModelFormation){
        try{
            $deleted = Formacionesmodelo::where('IdModelo','=',$idModel)->where('IdFormacionModelo','=',$idModelFormation)->delete();
            if($deleted){
                return response()->json(["msg"=>'Registro borrado correctamente']);
            }else{
                return response()->json(["msg"=>'Ocurrió un error, intentelo más tarde'],500);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
        }
    }
    public function changeQuantityModelFormation($idModel,$idModelFormation,$quantity){
        try {
            $model = Formacionesmodelo::where('IdModelo','=',$idModel)->where('IdFormacionModelo','=',$idModelFormation)->first();
            if($model != null){
                //$model
                $model->Cantidad = $quantity;
                $model->save();
                return response()->json(["msg"=>'Cantidad actualizada correctamente a '.$quantity]);

            }else{
                return response()->json(['msg'=>'Modelo no encontrado'],404);
            }
        }
        catch(Exception $e) {
            return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
        }
    }
    public function addModelFormation(Request $request, $id) {
        $model = Modelo::where('IdModelo','=',$id)->first();
        if($model == null) {
            return response()->json(['msg'=>'Modelo no encontrado'],404);
        }
        if($request->typeModelRadios == 'formacion'){
            $formation = Formacionesbase::where('IdFormacionBase','=',$request->formacion)->with('detail')->first();
            $details = $formation->detail;
            foreach ($details as $detail){
                $modelFormation = new Formacionesmodelo();
                $modelFormation->fill($detail->toArray());
                $modelFormation->IdModelo = $id;
                $modelFormation->save();
            }

        }elseif($request->typeModelRadios == 'material'){
            $modelFormation = new Formacionesmodelo();
            $modelFormation->IdModelo = $id;
            $modelFormation->IdMaterial = $request->material;
            $modelFormation->Cantidad = $request->Cantidad;
            $modelFormation->IdProceso = $request->IdProceso;
            $modelFormation->EsTablero = $request->EsTablero;
            $modelFormation->IdCategoriaColor = $request->IdCategoriaColor;

            if(isset($request->Corte)){
                $modelFormation->Corte = 1;
            }else{
                $modelFormation->Corte = 0;
            }
            if(isset($request->Router)){
                $modelFormation->Router = 1;
            }else{
                $modelFormation->Router = 0;
            }
            if(isset($request->Curvos)){
                $modelFormation->Curvos = 1;
            }else{
                $modelFormation->Curvos = 0;
            }
            if(isset($request->Puertas)){
                $modelFormation->Puertas = 1;
            }else{
                $modelFormation->Puertas = 0;
            }
            if(isset($request->Molduras)){
                $modelFormation->Molduras = 1;
            }else{
                $modelFormation->Molduras = 0;
            }
            if(isset($request->Costados)){
                $modelFormation->Costados = 1;
            }else{
                $modelFormation->Costados = 0;
            }
            if(isset($request->Pantallas)){
                $modelFormation->Pantallas = 1;
            }else{
                $modelFormation->Pantallas = 0;
            }
            if(isset($request->Entrepanos)){
                $modelFormation->Entrepanos = 1;
            }else{
                $modelFormation->Entrepanos = 0;
            }
            if(isset($request->Divisiones)){
                $modelFormation->Divisiones = 1;
            }else{
                $modelFormation->Divisiones = 0;
            }
            if(isset($request->Archiveros)){
                $modelFormation->Archiveros = 1;
            }else{
                $modelFormation->Archiveros = 0;
            }
            if(isset($request->Barrenos)){
                $modelFormation->Barrenos = 1;
            }else{
                $modelFormation->Barrenos = 0;
            }
            if(isset($request->Zoclos)){
                $modelFormation->Zoclos = 1;
            }else{
                $modelFormation->Zoclos = 0;
            }
            if(!$modelFormation->EsTablero) {
                $modelFormation->Chapas = null;
                $modelFormation->PVC = null;
            }else{
                $modelFormation->Chapas = $request->Chapas;
                $modelFormation->PVC = $request->PVC;
            }

            $modelFormation->save();
        }
        $model->load('formations','formations.process','formations.material');
        return response()->json(["msg"=>"Formación de modelo guardada correctamente",'model'=>$model]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Modelo::find($id);
        if($model != null){
            $model->fill($request->all());
            $model->save();
            return response()->json(['msg'=>'El modelo ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Modelo no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->all());
        $model = Formacionesmodelo::where('IdModelo', '=', $request->get('id'))->first();
        $cotizacionesDetalle = CotizacionDetalle::where('IdModelo', '=', $request->get('id'))->first();
        
        if(is_null($model)){
            if(is_null($cotizacionesDetalle)){
                $modelo = Modelo::find($request->get('id'));
                $modelo->delete();
                return response()->json(['success'=>true]);
            }else{
                return response()->json(['success'=>false]);
            }
        }else{
            return response()->json(['success'=>false]);
        }
    }
}
