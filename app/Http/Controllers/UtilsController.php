<?php

namespace App\Http\Controllers;

use App\Almacene;
use App\Categoriascolores;
use App\Cliente;
use App\Colore;
use App\Formacionesbase;
use App\Linea;
use App\Materiales;
use App\Proceso;
use App\Tablero;
use App\Unidadesmedida;
use Illuminate\Http\Request;
use App\Agente;
use App\Estado;
use App\Ciudad;
use App\Transportista;
use App\Empleado;
use App\Modelo;
use Exception;


class UtilsController extends Controller
{
    public function getCatalogsForNewClient(){
        try {
            $agents = Empleado::all();
            $states = Estado::all();
            $carriers = Transportista::all();
            return response()->json(['agents'=>$agents,'states'=>$states,'carriers'=>$carriers],200);
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function getColors(){
        try{
            $colors = Colore::all();
            return response()->json($colors);
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function getModels(Request $request) {
        try {
            $models = Modelo::searchModelQuotation($request->nombre,$request->clave)->get();
            return response()->json($models);
        }
        catch(Exception $e) {
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function getLines(){
        try{
            $lines = Linea::all();
            return response()->json($lines);
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function catalogsForNewFormation(){
        try{
            $processes = Proceso::all();
            $materials = Materiales::all();
            $categories = Categoriascolores::all();
            return response()->json(['processes'=>$processes,'materials'=>$materials,'categories'=>$categories]);
        }catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function catalogsNewQuotation(){
        try{
            $clients = Cliente::with('city')->get();
            $colors = Colore::all();
            return response()->json(['clients'=>$clients,'colors'=>$colors]);
        }catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function catalogsForNewModelFormation(){
        try{
            $processes = Proceso::all();
            $materials = Materiales::all();
            $baseFormations = Formacionesbase::all();
            $categories = Categoriascolores::all();
            $lines = Linea::all();
            return response()->json(['processes'=>$processes,'materials'=>$materials,'baseFormations'=>$baseFormations,
                'categories'=>$categories,
                'lines'=>$lines]);

        }catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function catalogsForNewMaterial(){
        try{
            $warehouses = Almacene::all();
            $categories = Categoriascolores::all();
            $boards  = Tablero::all();
            $units = Unidadesmedida::all();

            return response()->json(['warehouses'=>$warehouses,'categories'=>$categories,'boards'=>$boards,'units'=>$units]);

        }catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function getWarehouses(){
        try{
            $warehouses = Almacene::all();
            return response()->json($warehouses);

        }catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }

    }
    public function getCarrier(){
        try {
            $carriers = Transportista::all();
            return response()->json($carriers);
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function getCities($state_id){
        try {
            $cities = Ciudad::where('idEstado','=',$state_id)->get();
            return response()->json($cities);
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
    public function getAgents(){
        try{
            $agents = Agente::all();
            return response()->json($agents);
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],$e->getCode());
        }
    }
}