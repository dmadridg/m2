<?php

namespace App\Http\Controllers;

use Doctrine\DBAL\Schema\Table;
use Illuminate\Http\Request;
use App\Tablero;
class BoardController extends Controller
{
    public $boards;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->boards = Tablero::search($request->name)->paginate(5);
        $this->boards->setPath('boards');
        $this->numRecords = count($this->boards);

        $this->data = ['boards'=>$this->boards,'title'=>'Listado de Tableros','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron tableros';
        }
        return view('main.boards.index')->with($this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $board = new Tablero();
            $board->fill($request->all());
            if ($board->save()){
                return response()->json(['msg'=>'Tablero creado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear el tablero'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $board = Tablero::find($id);
            if($board != null){
                return response()->json($board);
            }else{
                return response()->json(['msg'=>"Tablero no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $board = Tablero::find($id);
        if($board != null){
            $board->fill($request->all());
            $board->save();
            return response()->json(['msg'=>'El tablero ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Tablero no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
