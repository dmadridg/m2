<?php

namespace App\Http\Controllers;

use App\Proceso;
use Illuminate\Http\Request;

class ProcessesController extends Controller
{
    public $processes;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->processes = Proceso::search($request->name)->paginate(5);
        $this->processes->setPath('processes');
        $this->numRecords = count($this->processes);

        $this->data = ['processes'=>$this->processes,'title'=>'Listado de Procesos','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron procesos';
        }
        return view('main.processes.index')->with($this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $process = new Proceso();
            $process->fill($request->all());
            if ($process->save()){
                return response()->json(['msg'=>'Proceso creado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear el proceso'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $process = Proceso::find($id);
            if($process != null){
                return response()->json($process);
            }else{
                return response()->json(['msg'=>"Proceso no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $process = Proceso::find($id);
        if($process != null){
            $process->fill($request->all());
            $process->save();
            return response()->json(['msg'=>'El Proceso ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Proceso no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
