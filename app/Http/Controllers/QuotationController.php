<?php

namespace App\Http\Controllers;

use App\Colorescotizacionesdetalle;
use App\Cotizacion;
use App\CotizacionDetalle;
use App\Modelo;

use Carbon\Carbon;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use DB;
// use Storage;
use Illuminate\Support\Facades\Storage;
use PHPMailer\PHPMailer;

class QuotationController extends Controller
{
    public $quotations;
    public $quotation;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->quotations = Cotizacion::search($request->clave)->paginate(10)->appends(request()->query());
        $this->quotations->setPath('quotations');
        $this->numRecords = count($this->quotations);

        $this->data = ['quotations'=>$this->quotations,'title'=>'Listado de Cotizaciones','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron cotizaciones';
        }
        return view('main.quotations.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
           DB::beginTransaction();
           $quotationData = ($request->all());
           $this->quotation = new Cotizacion();
           $this->quotation->IdPedido = 0;
           $this->quotation->IdCliente = $quotationData['client']['IdCliente'];
           $this->quotation->IdTransportista = $quotationData['client']['IdTransportista'];
           $this->quotation->FechaCotizacion = Carbon::now()->toDateTimeString();
           $this->quotation->Estatus = 1;
           $this->quotation->Referencia = '';
           $this->quotation->Observaciones = $quotationData['observations'];
           // $this->quotation->save();

           // dd($newQuotation);

           if($this->quotation->save()){

            if(isset($quotationData['products'])){
                foreach($quotationData['products'] as $product) {
                    // dd($product);
                    $cotizacionDetalle = new CotizacionDetalle();
                    $cotizacionDetalle->IdModelo = $product['model']['IdModelo'];
                    $cotizacionDetalle->IdCotizacion = $this->quotation->IdCotizacion;
                    $cotizacionDetalle->Cantidad = $product['quantity'];
                    $cotizacionDetalle->Precio = $product['price'];
                    $cotizacionDetalle->Area = $product['area'];
                    $cotizacionDetalle->Lados = $product['side'];
                    $cotizacionDetalle->Descuento = $product['discount'];
                    $cotizacionDetalle->SubTotal = $product['subtotal'];
                    $cotizacionDetalle->Total = $product['total'];
                    $cotizacionDetalle->Descripcion = $product['description'];
                    $cotizacionDetalle->Medida = $product['metering'];
                    $cotizacionDetalle->Observaciones = $quotationData['observations'];

                    if($product['model']['IdModelo'] == '1452'){
                        $cotizacionDetalle->Image = isset( $product['image'] ) ? $product['image'] : '';
                        $cotizacionDetalle->colorBase = isset($product['colorBaseId']) ? $product['colorBaseId'] : 1;
                        $cotizacionDetalle->colorCombiando = isset($product['colorCombId']) ? $product['colorCombId'] : 1;
                    }
 
                    $cotizacionDetalle->save();

                    // dd($cotizacionDetalle);
 
                    $descripcionColores = '';
 
                    if($product['prodEsp'] == 'yes') {
                        $descripcionColores.= $product['colorBase'].', '.$product['colorComb'];
                    }
                    if(isset($product["colorCategories"])){
                        foreach($product['colorCategories'] as $colorCategory) {
                            $descripcionColores .=  $colorCategory['nameCategoryColor'].'-'.$colorCategory['colorName'].',';
                            $coloresCotizacionesDetalle = new Colorescotizacionesdetalle();
                            $coloresCotizacionesDetalle->IdCotizacionDetalle = $cotizacionDetalle->IdCotizacionDetalle;
                            $coloresCotizacionesDetalle->IdCategoriasColores = $colorCategory['categoryColor'];
                            $coloresCotizacionesDetalle->IdColores = $colorCategory['colorId'];
                            $coloresCotizacionesDetalle->NombreCategoria = $colorCategory['nameCategoryColor'];
                            $coloresCotizacionesDetalle->NombreColor = $colorCategory['colorName'];
                            $coloresCotizacionesDetalle->save();
                        }
                    }
 
                    $descripcionColores = substr($descripcionColores,0, strlen($descripcionColores)-1);
                    $cotizacionDetalle->where('IdCotizacionDetalle',$cotizacionDetalle->IdCotizacionDetalle)->update(['descripcionCategoriasColor'=>$descripcionColores]);
 
                }
            }
               
           }
           DB::commit();
           return response()->json(['msg'=>'La cotizacion '.$this->quotation->id.' se almacenó correctamente'],200);

       }
       catch(Exception $e) {
           DB::rollBack();
           return response()->json(['msg'=>$e->getMessage(),'line'=>$e->getLine()]);
       }
    }
    public function printQuotation($id) {
        try {
            $quotation = Cotizacion::with(['detail','detail.model','client','client.city','client.state'])->where('IdCotizacion',$id)->first();
            if($quotation == null) {
                return response()->json(['msg'=>'Cotización no encontrada']);
            }
            $pdf = new FpdfQuotation();
            $pdf->SetAutoPageBreak(true,30);
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Image(public_path('/img/logoeuronegro.jpg'),5,5,35,35);
            $pdf->setX(140);
            $pdf->Cell(50, 25, utf8_decode('Cotización No. '.$quotation->IdCotizacion));
            $pdf->SetXY(10,45);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(130, 6, utf8_decode('Nombre:   '.$quotation->client->Nombre),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Fecha: '.Carbon::now()->toDateString()),0,1,1);

            $pdf->Cell(130, 6, utf8_decode('Contacto: '.$quotation->client->Contacto),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Fecha Cotización: '.Carbon::createFromFormat('Y-m-d H:s:i',$quotation->FechaCotizacion)->toDateString()),0,1,1);

            $pdf->Cell(130, 6, utf8_decode('Dirección:'.($quotation->client->DomicilioFiscal ?? 'No disponible')),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Ciudad: '.($quotation->client->city->NombreCiudad ?? 'No disponible')),0,1,1);

            $pdf->Cell(130, 6, utf8_decode('Teléfono: '.$quotation->client->Telefono),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Estado: '.($quotation->client->state->NombreEstado ?? 'No disponible')),0,1,1);

            $pdf->Cell(130, 6, '',0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Referencia: '.$quotation->Referencia),0,1,1);

            $pdf->SetY($pdf->GetY()+5);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(50, 6, utf8_decode('Estimado(a): '.$quotation->client->Contacto),0,1,1);

            $pdf->SetFont('Arial', '', 10);
            $pdf->MultiCell(190, 4, utf8_decode('Ponemos a su consideración la siguiente cotización de mobiliario el cual estamos seguros sera de su conformidad, quedando a sus ordenes para cualquier duda o aclaración.'),0,1,0);
            $pdf->SetY($pdf->GetY() + 5);
            $groupedModels = $quotation->detail->groupBy('Area');

            $totalDiscount = 0;
            $subtotal = 0;
            $total = 0;
            $iva = 0.16;
	    $pageHeight = 279.4;
            $limitPage  = $pageHeight - 40;
            foreach($groupedModels as $key => $value) {

                $pdf->SetFont('Arial', 'B', 9);
                $pdf->SetTextColor(134,12,12);
                $pdf->Cell(150, 6, utf8_decode('Partida: '.$key),0,1,1);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(192,192,192);
                $pdf->Cell(25, 5, utf8_decode('CODIGO'),1,0,'C',1);
                $pdf->Cell(23, 5, utf8_decode('IMAGEN'),1,0,'C',1);
                $pdf->Cell(82, 5, utf8_decode('DESCRIPCION'),1,0,'C',1);
                $pdf->Cell(10, 5, utf8_decode('CANT.'),1,0,'C',1);
                $pdf->Cell(15, 5, utf8_decode('P.UNIT'),1,0,'C',1);
                $pdf->Cell(10, 5, utf8_decode('DESC'),1,0,'C',1);
                $pdf->Cell(25, 5, utf8_decode('IMPORTE'),1,1,'C',1);
                $pdf->SetFont('Arial', '', 7.5);
                foreach ($groupedModels[$key] as $k=>$v) {

                    $currentModel = $groupedModels[$key][$k];
                    $totalDiscount += $currentModel->SubTotal * ($currentModel->Descuento / 100);
                    $subtotal += $currentModel->Total;

                    

                    $pdf->SetFont('Arial', '', 7.5);
                    $beforeCell = $pdf->GetY();
		            if($beforeCell  > $limitPage) {
                        $pdf->AddPage();
                        $beforeCell = $pdf->GetY();
                    }
                    $pdf->SetX(58);
                    $pdf->MultiCell(82, 4, utf8_decode($currentModel->Descripcion."\n".
                        "Medidas:".$currentModel->Medida."\n".
                        "Colores:".$currentModel->descripcionCategoriasColor."\n"." "),1,'L',0);
                    $pdf->SetFont('Arial', '', 7.5);

                    $afterCell = $pdf->GetY();
                    $tmpY = $pdf->GetY() - ($afterCell - $beforeCell);
                    $height = ($afterCell - $beforeCell);
                    $pdf->SetXY(10,$tmpY);
                    $pdf->Cell(25, $height, ($currentModel->model->Clave),1,0,'C');

                    // dd($currentModel);

                    if($currentModel->IdModelo == 1452){
                        // $explodeString  = explode('/',$currentModel->Image);
                        // $endPosition = end($explodeString);
                        $file = 'storage/'.$currentModel->Image;
                        
                        // dd(asset($file));
                    }else{
                        $explodeString  = explode('/',$currentModel->model->Imagen);
                        $endPosition = end($explodeString);
                        $file = 'assets/models/'.$endPosition;
                    }
                    $widthImage = 20;
                    $middleYImage  = $widthImage / 2;
                    $heightImage = $height / 2;
                    $middleXImage = $heightImage / 2;
                    if($currentModel->model->Imagen != null) {
                        $spacing = ($height - 18) / 2;
                        if($height <= 18 ) {
                            
                            $pdf->Image(public_path($file),$pdf->GetX()-$middleYImage / 2,($pdf->GetY()+$spacing),20,18);    
                            //$pdf->Image(public_path($file),$pdf->GetX()+$middleYImage / 2,$pdf->GetY(),$widthImage / 2,$height);    
                        } else {
                            
                            $pdf->Image(public_path($file),$pdf->GetX(),($pdf->GetY()+$spacing),20,18);   
                            
                            //$pdf->Image(public_path($file),$pdf->GetX()+ $middleYImage / 2,$pdf->GetY()+$middleXImage,$widthImage / 2,$heightImage);    
                        }
                    }else if($currentModel->Image != ""){
                        $spacing = (($height - 18) / 2+1);
                        if($height <= 18 ) {
                            // dd($pdf->GetX()-$middleYImage / 2);
                            $pdf->Image(public_path($file),($pdf->GetX()-$middleYImage / 2)+7,($pdf->GetY()+$spacing),18,15);
                            // $pdf->Image(asset($file),$pdf->GetX()-$middleYImage / 2,($pdf->GetY()+$spacing),20,18);
                        } else {
                            $pdf->Image(public_path($file),$pdf->GetX(),($pdf->GetY()+$spacing),18,15);
                            // $pdf->Image(asset($file),$pdf->GetX()-$middleYImage / 2,($pdf->GetY()+$spacing),20,18);
                        }
                    }
                    $pdf->Cell(23, $height, '',1,0,'C');
                    $pdf->SetXY(140,$tmpY);
                    $pdf->Cell(10, $height, ($currentModel->Cantidad),1,0,'C');
                    $pdf->SetFont('Arial', '', 6);
                    $pdf->Cell(15, $height, '$'.number_format($currentModel->Precio,'2','.',','),1,0,'C');
                    $pdf->SetFont('Arial', '', 7.5);
                    $pdf->Cell(10, $height, utf8_decode($currentModel->Descuento.'%'),1,0,'C');
                    $pdf->Cell(25, $height, '$'.number_format($currentModel->Total,'2','.',','),1,1,'C');
                }
                $pdf->SetY($pdf->GetY() + 4);
            }
            $totalIva = $subtotal * $iva;
            $granTotal = $subtotal + $totalIva;
            $pdf->SetFont('Arial', 'B', 6);
            $pdf->Cell(140, 6, (''),0,0,'L');
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->MultiCell(45, 5, "Descuento $".number_format((integer)$totalDiscount,2,'.',',')."\n".
                "Subtotal $ ".number_format($subtotal,2,'.',',')."\n".
                "IVA $ ".number_format($totalIva,2,'.',',')."\n".
                "Total $".number_format($granTotal,2,'.',','),0,'R',0);
            $pdf->SetFont('Arial', '', 6);
            $pdf->SetY($pdf->GetY()-10);
            $pdf->MultiCell(140, 4, utf8_decode(''),0,1,0);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->SetLineWidth(0.08);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
            $pdf->SetY($pdf->GetY());
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(140, 6, ('Condiciones de venta:'),0,1,'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->MultiCell(140, 4, utf8_decode($quotation->Observaciones),0,1,0);
            $pdf->MultiCell(140, 4, utf8_decode(""),0,1,0);
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());

            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(140, 6, ('CONDICIONES GENERALES:'),0,1,'L');
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
            $pdf->SetFont('Arial', '', 10);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->MultiCell(190, 4, utf8_decode("- Una vez aceptada la cotizaciòn y entregada la mercancia no se acepta ningun tipo de devolución por cambio de color, medida o lado\n".
                "- Se hará un cargo del 20% sobre el valor de la mercancia facturada en caso de devolucón\n".
                "- En sistema de mamparas y mobiliario en general no incluye ningún tipo de instalaciòn electrica, voz o datos asi como ningún accesorio no cotizado.\n".
                "- La instalaciòn de cualquier tipo de accesorio eléctrico, voz o datos corre por cuenta del cliente.\n".
                "- Vigencia de la cotizacion es de 30 dias.\n".
                "- Estos precios pueden variar sin previo aviso.\n".
                "- Garantia de 10 años contra defectos de fabricacion en mobiliario (ver garantia)\n".
                "- Todos los productos 'especiales' requieren de su previa aprobación.\n".
                "- Los productos especiales tienen un tiempo de entrega mayor a la de los productos de linea y se requiere del 70% de anticipo junto con la orden de".
                "compra y el resto contra aviso de embarque.\n".
                "- Los productos especiales en caso de ser aprobados se deben de enviar en una orden de compra por separado a la orden de compra de los".
                "productos de linea."),0,1,0);
            $pdf->Line(10,$pdf->GetY()+3,200,$pdf->GetY()+3);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetY($pdf->GetY()+15);
            $pdf->Cell(140, 6, ('DEPOSITOS:'),0,1,'L');
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->MultiCell(190, 4, utf8_decode("En caso de ser aceptada la cotizacion favor de emitir cheque a nombre de Euroespacio s.a. de c.v. o hacer deposito o transferencia a las".
                " siguientes cuentas:"),0,1,0);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->MultiCell(190, 4, utf8_decode("- RFC: EUR940714-SW7\n".
                "- Banorte cuenta: 0129637132 | clabe 072320001296371328\n".
                "- Banamex cuenta: 2555469287 | clabe 002320025554692876"),0,1,0);
            $pdf->SetY($pdf->GetY()+25);

            $pdf->Cell(90, 6, ('______________________________________'),0,0,'C');
            $pdf->Cell(90, 6, ('______________________________________'),0,1,'C');
            $pdf->Cell(90, 6, utf8_decode($quotation->client->Nombre),0,0,'C');
            $pdf->Cell(90, 6, (auth()->user()->Nombre),0,1,'C');
            $pdf->Cell(90, 6, 'NOMBRE Y FIRMA DEL CLIENTE',0,0,'C');
            $pdf->Cell(90, 6, 'EUROESPACIO S.A. DE C.V.',0,1,'C');
            $pdf->Output('D','cotizacion.pdf');
        }
        catch(Exception $e) {
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->quotation = Cotizacion::where('IdCotizacion',$id)->with(['client','client.city','detail','detail.model'])->first();
            if($this->quotation != null) {
                return response()->json($this->quotation);
            }
            return response()->json(['msg'=>'Cotizacion no encontrada'],404);
        }
        catch(Exception $e) {
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try {
            // dd($request->all());
           DB::beginTransaction();
           $this->quotation = Cotizacion::where('IdCotizacion',$id)->first();
           if ($this->quotation != null) {
               $quotationData = ($request->all());
               if(isset($quotationData['products'])){
                    foreach($quotationData['products'] as $product) {
                        $idProduct = isset( $product['idProduct'] ) ? $product['idProduct'] : 0;

                            // dd($product);

                        $cotizacionDetalle = CotizacionDetalle::firstOrCreate([
                            'IdCotizacionDetalle' => $idProduct, 
                            'IdCotizacion' => $id, 
                            'IdModelo' => $product['model']['IdModelo']
                            ]);
                            $cotizacionDetalle->Area = $product['area'];
                            $cotizacionDetalle->Cantidad = $product['quantity'];
                            $cotizacionDetalle->Precio = $product['price'];
                            $cotizacionDetalle->Lados = $product['side'];
                            $cotizacionDetalle->Descuento = $product['discount'];
                            $cotizacionDetalle->SubTotal = $product['subtotal'];
                            $cotizacionDetalle->Lados = $product['side'];
                            $cotizacionDetalle->Total = $product['total'];
                            $cotizacionDetalle->Descripcion = $product['description'];
                            $cotizacionDetalle->Medida = $product['metering'];
                            $cotizacionDetalle->Observaciones = $quotationData['observations'];
                            $cotizacionDetalle->colorBase = isset( $product['colorBaseId'] ) ? $product['colorBaseId'] : 0;
                            $cotizacionDetalle->colorCombiando = isset( $product['colorCombId'] ) ? $product['colorCombId'] : 0;
                            if($product['model']['IdModelo'] == '1452'){
                                $cotizacionDetalle->Image = isset( $product['image'] ) ? $product['image'] : '';
                            }
                        //    dd($cotizacionDetalle);
                            $cotizacionDetalle->save();

                        $descripcionColores = '';

                        if($product['prodEsp'] == 'yes') {
                            $colorb = isset( $product['colorBase'] ) ? $product['colorBase'] : '';
                            $colorC = isset( $product['colorComb'] ) ? $product['colorComb'] : '';
                            $descripcionColores.= $colorb .', '. $colorC;
                            
                        }
                        if(isset($product["colorCategories"])){
                            foreach($product['colorCategories'] as $colorCategory) {
                                $descripcionColores .=  $colorCategory['nameCategoryColor'].'-'.$colorCategory['colorName'].',';
                                $coloresCotizacionesDetalle = new Colorescotizacionesdetalle();
                                $coloresCotizacionesDetalle->IdCotizacionDetalle = $cotizacionDetalle->IdCotizacionDetalle;
                                $coloresCotizacionesDetalle->IdCategoriasColores = $colorCategory['categoryColor'];
                                $coloresCotizacionesDetalle->IdColores = $colorCategory['colorId'];
                                $coloresCotizacionesDetalle->NombreCategoria = $colorCategory['nameCategoryColor'];
                                $coloresCotizacionesDetalle->NombreColor = $colorCategory['colorName'];
                                $coloresCotizacionesDetalle->save();
                            }
                        }

                        $descripcionColores = substr($descripcionColores,0, strlen($descripcionColores)-1);
                        $cotizacionDetalle->where('IdCotizacionDetalle',$cotizacionDetalle->IdCotizacionDetalle)->update(['descripcionCategoriasColor'=>$descripcionColores]);
                    }                 
               }
               
               DB::commit();
               $currentQuotation = Cotizacion::where('IdCotizacion',$id)->first();
               $currentQuotation->Observaciones = $quotationData['observations'];
               $currentQuotation->save();
               return response()->json(['msg'=>'La cotizacion '.$this->quotation->IdCotizacion.' se actualizó correctamente'],200);
           }
           return response()->json(['msg'=>'Cotización no encontrada']);

       }
       catch(Exception $e) {
           DB::rollBack();
           return response()->json(['msg'=>$e->getMessage(),'line'=>$e->getLine()]);
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    function saveImage(Request $request)
    {

        if($request->file('file')){
            $path = $request->file('file')->getRealPath();
            $extension = $request->file('file')->getClientOriginalExtension();

            $name = rand() . "." . $extension;

            $url = public_path('').'/storage/'.$name;

            move_uploaded_file($_FILES['file']['tmp_name'], $url);

            return $name;
            // $file = file_get_contents($url, FILE_USE_INCLUDE_PATH);

        }else{
            return '';
        }
    }

    function getModel(Request $request)
    {
        // dd($request->all());
        $model = Modelo::where('IdModelo', '=', $request->get('idModel'))->first();
        // dd($model);
        return array('data'=>$model);
    }

    public function sendEmail(Request $request)
    {
        try {



            $quotation = Cotizacion::with(['detail','detail.model','client','client.city','client.state'])->where('IdCotizacion',$request->get('id'))->first();
            if($quotation == null) {
                return response()->json(['msg'=>'Cotización no encontrada']);
            }
            $pdf = new FpdfQuotation();
            $pdf->SetAutoPageBreak(true,30);
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Image(public_path('/img/logoeuronegro.jpg'),5,5,35,35);
            $pdf->setX(140);
            $pdf->Cell(50, 25, utf8_decode('Cotización No. '.$quotation->IdCotizacion));
            $pdf->SetXY(10,45);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(130, 6, utf8_decode('Nombre:   '.$quotation->client->Nombre),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Fecha: '.Carbon::now()->toDateString()),0,1,1);

            $pdf->Cell(130, 6, utf8_decode('Contacto: '.$quotation->client->Contacto),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Fecha Cotización: '.Carbon::createFromFormat('Y-m-d H:s:i',$quotation->FechaCotizacion)->toDateString()),0,1,1);

            $pdf->Cell(130, 6, utf8_decode('Dirección:'.($quotation->client->DomicilioFiscal ?? 'No disponible')),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Ciudad: '.($quotation->client->city->NombreCiudad ?? 'No disponible')),0,1,1);

            $pdf->Cell(130, 6, utf8_decode('Teléfono: '.$quotation->client->Telefono),0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Estado: '.($quotation->client->state->NombreEstado ?? 'No disponible')),0,1,1);

            $pdf->Cell(130, 6, '',0,0,1);
            $pdf->Cell(50, 6, utf8_decode('Referencia: '.$quotation->Referencia),0,1,1);

            $pdf->SetY($pdf->GetY()+5);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(50, 6, utf8_decode('Estimado(a): '.$quotation->client->Contacto),0,1,1);

            $pdf->SetFont('Arial', '', 10);
            $pdf->MultiCell(190, 4, utf8_decode('Ponemos a su consideración la siguiente cotización de mobiliario el cual estamos seguros sera de su conformidad, quedando a sus ordenes para cualquier duda o aclaración.'),0,1,0);
            $pdf->SetY($pdf->GetY() + 5);
            $groupedModels = $quotation->detail->groupBy('Area');

            $totalDiscount = 0;
            $subtotal = 0;
            $total = 0;
            $iva = 0.16;
	       $pageHeight = 279.4;
            $limitPage  = $pageHeight - 40;
            foreach($groupedModels as $key => $value) {

                $pdf->SetFont('Arial', 'B', 9);
                $pdf->SetTextColor(134,12,12);
                $pdf->Cell(150, 6, utf8_decode('Partida: '.$key),0,1,1);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(192,192,192);
                $pdf->Cell(25, 5, utf8_decode('CODIGO'),1,0,'C',1);
                $pdf->Cell(23, 5, utf8_decode('IMAGEN'),1,0,'C',1);
                $pdf->Cell(82, 5, utf8_decode('DESCRIPCION'),1,0,'C',1);
                $pdf->Cell(10, 5, utf8_decode('CANT.'),1,0,'C',1);
                $pdf->Cell(15, 5, utf8_decode('P.UNIT'),1,0,'C',1);
                $pdf->Cell(10, 5, utf8_decode('DESC'),1,0,'C',1);
                $pdf->Cell(25, 5, utf8_decode('IMPORTE'),1,1,'C',1);
                $pdf->SetFont('Arial', '', 7.5);
                foreach ($groupedModels[$key] as $k=>$v) {

                    $currentModel = $groupedModels[$key][$k];
                    $totalDiscount += $currentModel->SubTotal * ($currentModel->Descuento / 100);
                    $subtotal += $currentModel->Total;

                    $pdf->SetFont('Arial', '', 7.5);
                    $beforeCell = $pdf->GetY();
		            if($beforeCell  > $limitPage) {
                        $pdf->AddPage();
                        $beforeCell = $pdf->GetY();
                    }
                    $pdf->SetX(58);
                    $pdf->MultiCell(82, 4, utf8_decode($currentModel->Descripcion."\n".
                        "Medidas:".$currentModel->Medida."\n".
                        "Colores:".$currentModel->descripcionCategoriasColor."\n"." "),1,'L',0);
                    $pdf->SetFont('Arial', '', 7.5);

                    $afterCell = $pdf->GetY();
                    $tmpY = $pdf->GetY() - ($afterCell - $beforeCell);
                    $height = ($afterCell - $beforeCell);
                    $pdf->SetXY(10,$tmpY);
                    $pdf->Cell(25, $height, ($currentModel->model->Clave),1,0,'C');
                    
                    if($currentModel->IdModelo == 1452){
                        // $explodeString  = explode('/',$currentModel->Image);
                        // $endPosition = end($explodeString);
                        $file = 'storage/'.$currentModel->Image;
                        
                        // dd(asset($file));
                    }else{
                        $explodeString  = explode('/',$currentModel->model->Imagen);
                        $endPosition = end($explodeString);
                        $file = 'assets/models/'.$endPosition;
                    }
                    $widthImage = 20;
                    $middleYImage  = $widthImage / 2;
                    $heightImage = $height / 2;
                    $middleXImage = $heightImage / 2;
                    if($currentModel->model->Imagen != null) {
                        $spacing = ($height - 18) / 2;
                        if($height <= 18 ) {
                            
                            $pdf->Image(public_path($file),$pdf->GetX()-$middleYImage / 2,($pdf->GetY()+$spacing),20,18);    
                            //$pdf->Image(public_path($file),$pdf->GetX()+$middleYImage / 2,$pdf->GetY(),$widthImage / 2,$height);    
                        } else {
                            
                            $pdf->Image(public_path($file),$pdf->GetX(),($pdf->GetY()+$spacing),20,18);   
                            
                            //$pdf->Image(public_path($file),$pdf->GetX()+ $middleYImage / 2,$pdf->GetY()+$middleXImage,$widthImage / 2,$heightImage);    
                        }
                    }else if($currentModel->Image != ""){
                        $spacing = (($height - 18) / 2+1);
                        if($height <= 18 ) {
                            // dd($pdf->GetX()-$middleYImage / 2);
                            $pdf->Image(public_path($file),($pdf->GetX()-$middleYImage / 2)+7,($pdf->GetY()+$spacing),18,15);
                            // $pdf->Image(asset($file),$pdf->GetX()-$middleYImage / 2,($pdf->GetY()+$spacing),20,18);
                        } else {
                            $pdf->Image(public_path($file),$pdf->GetX(),($pdf->GetY()+$spacing),18,15);
                            // $pdf->Image(asset($file),$pdf->GetX()-$middleYImage / 2,($pdf->GetY()+$spacing),20,18);
                        }
                    }

                    $pdf->Cell(23, $height, '',1,0,'C');
                    $pdf->SetXY(140,$tmpY);
                    $pdf->Cell(10, $height, ($currentModel->Cantidad),1,0,'C');
                    $pdf->SetFont('Arial', '', 6);
                    $pdf->Cell(15, $height, '$'.number_format($currentModel->Precio,'2','.',','),1,0,'C');
                    $pdf->SetFont('Arial', '', 7.5);
                    $pdf->Cell(10, $height, utf8_decode($currentModel->Descuento.'%'),1,0,'C');
                    $pdf->Cell(25, $height, '$'.number_format($currentModel->Total,'2','.',','),1,1,'C');
                }
                $pdf->SetY($pdf->GetY() + 4);
            }
            $totalIva = $subtotal * $iva;
            $granTotal = $subtotal + $totalIva;
            $pdf->SetFont('Arial', 'B', 6);
            $pdf->Cell(140, 6, (''),0,0,'L');
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->MultiCell(45, 5, "Descuento $".number_format((integer)$totalDiscount,2,'.',',')."\n".
                "Subtotal $ ".number_format($subtotal,2,'.',',')."\n".
                "IVA $ ".number_format($totalIva,2,'.',',')."\n".
                "Total $".number_format($granTotal,2,'.',','),0,'R',0);
            $pdf->SetFont('Arial', '', 6);
            $pdf->SetY($pdf->GetY()-10);
            $pdf->MultiCell(140, 4, utf8_decode(''),0,1,0);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->SetLineWidth(0.08);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
            $pdf->SetY($pdf->GetY());
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(140, 6, ('Condiciones de venta:'),0,1,'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->MultiCell(140, 4, utf8_decode($quotation->Observaciones),0,1,0);
            $pdf->MultiCell(140, 4, utf8_decode(""),0,1,0);
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());

            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(140, 6, ('CONDICIONES GENERALES:'),0,1,'L');
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
            $pdf->SetFont('Arial', '', 10);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->MultiCell(190, 4, utf8_decode("- Una vez aceptada la cotizaciòn y entregada la mercancia no se acepta ningun tipo de devolución por cambio de color, medida o lado\n".
                "- Se hará un cargo del 20% sobre el valor de la mercancia facturada en caso de devolucón\n".
                "- En sistema de mamparas y mobiliario en general no incluye ningún tipo de instalaciòn electrica, voz o datos asi como ningún accesorio no cotizado.\n".
                "- La instalaciòn de cualquier tipo de accesorio eléctrico, voz o datos corre por cuenta del cliente.\n".
                "- Vigencia de la cotizacion es de 30 dias.\n".
                "- Estos precios pueden variar sin previo aviso.\n".
                "- Garantia de 10 años contra defectos de fabricacion en mobiliario (ver garantia)\n".
                "- Todos los productos 'especiales' requieren de su previa aprobación.\n".
                "- Los productos especiales tienen un tiempo de entrega mayor a la de los productos de linea y se requiere del 70% de anticipo junto con la orden de".
                "compra y el resto contra aviso de embarque.\n".
                "- Los productos especiales en caso de ser aprobados se deben de enviar en una orden de compra por separado a la orden de compra de los".
                "productos de linea."),0,1,0);
            $pdf->Line(10,$pdf->GetY()+3,200,$pdf->GetY()+3);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetY($pdf->GetY()+15);
            $pdf->Cell(140, 6, ('DEPOSITOS:'),0,1,'L');
            $pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->MultiCell(190, 4, utf8_decode("En caso de ser aceptada la cotizacion favor de emitir cheque a nombre de Euroespacio s.a. de c.v. o hacer deposito o transferencia a las".
                " siguientes cuentas:"),0,1,0);
            $pdf->SetY($pdf->GetY()+5);
            $pdf->MultiCell(190, 4, utf8_decode("- RFC: EUR940714-SW7\n".
                "- Banorte cuenta: 0129637132 | clabe 072320001296371328\n".
                "- Banamex cuenta: 2555469287 | clabe 002320025554692876"),0,1,0);
            $pdf->SetY($pdf->GetY()+25);

            $pdf->Cell(90, 6, ('______________________________________'),0,0,'C');
            $pdf->Cell(90, 6, ('______________________________________'),0,1,'C');
            $pdf->Cell(90, 6, utf8_decode($quotation->client->Nombre),0,0,'C');
            $pdf->Cell(90, 6, (auth()->user()->Nombre),0,1,'C');
            $pdf->Cell(90, 6, 'NOMBRE Y FIRMA DEL CLIENTE',0,0,'C');
            $pdf->Cell(90, 6, 'EUROESPACIO S.A. DE C.V.',0,1,'C');
            
            $pdfName = rand() . ".pdf";
            $pdf->Output("F",storage_path().'/'.$pdfName); 

            $url = storage_path().'/'.$pdfName;
            
            $file = file_get_contents($url, FILE_USE_INCLUDE_PATH);
            // dd($url);
            
            // Storage::put('public/'.$pdfName.".pdf", $pdf);
            
            // $contents = Storage::get("".$pdfName);
            // dd($file);

            $msg  = 'Hello World';
            $subj = 'Test mail message';
            $to   = 'crackermix1@gmail.com';
            $from = 'dmadridgarate@example.com';
            $name = 'Name';

            $text             = 'Cotizacion';
            $mail             = new PHPMailer\PHPMailer(); // create a n
            $mail->SMTPDebug  = 2; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth   = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host       = "smtp.gmail.com";
            $mail->Port       = 465; // or 587
            $mail->IsHTML(true);
            $mail->Username = "changoleon@gmail.com";
            $mail->Password = "iktioscambridg3";
            $mail->SetFrom("dmadridgarate@gmail.com", 'Daniel Madrid');
            $mail->Subject = "Cotizacion";
            $mail->Body    = $text;
            $mail->addAttachment($url, "Cotizacion.pdf", $encoding = 'base64', $type = 'application/octet-stream');
            $mail->AddAddress("crackermix1@gmail.com", "Daniel Garate");
            // $mail->AddAddress($quotation->client->Email, $quotation->client->Nombre);
            if ($mail->Send()) {
                return json_encode(array("success"=>true));
                // return response()->json(['success'=>true],500);
            } else {
                return json_encode(array("success"=>false));
                // return false;
                // return response()->json(['success'=>false],500);
            }


            // $pdf->Output('D','cotizacion.pdf');
        }
        catch(Exception $e) {
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }
}
class FpdfQuotation extends Fpdf {
    function footer() {
        $this->SetY(-20);
        $this->SetLineWidth(0.08);
        $this->SetDrawColor(242,242,242);
        $this->Line(10,270,200,270);
        $this->SetFont('Arial','',8);
        $this->Cell(173,5, utf8_decode('Euroespacio sa de cv Nva. Orleans No 1172 Col. Rincòn del agua azul c.p. 44460 Guadalajara, jalisco Mexico'), 0, 1,'C');
        $this->Cell(173,5, utf8_decode('Tel. 01.33.36503915 01.33.36190311 | www.euroespacio.com | ventas@euroespacio.com'), 0, 1,'C');
        $this->SetTextColor(0,0,0);
        $this->SetX(190);
        $this->Cell(0,10,$this->PageNo().'/{nb}',0,0,'C');
    }
}
