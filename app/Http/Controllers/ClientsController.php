<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use Exception;
class ClientsController extends Controller
{
    public $clients;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->clients = Cliente::search($request->name)->paginate(5);
        $this->clients->setPath('clients');
        $this->numRecords = count($this->clients);

        $this->data = ['clients'=>$this->clients,'title'=>'Listado de clientes','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron clientes';
        }
        return view('main.clients.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $client = new Cliente();
            $client->fill($request->all());
            if ($client->save()){
                $client->update(['ClaveCliente'=>$client->IdCliente]);
                return response()->json(['msg'=>'Cliente creado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear el cliente'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $client = Cliente::find($id);
            if($client != null){
                return response()->json($client);
            }else{
                return response()->json(['msg'=>"Cliente no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Cliente::find($id);
        if($client != null){
            $client->fill($request->all());
            $client->save();
            return response()->json(['msg'=>'El cliente ha sido actualizado correctamente'],200);
        }else{
            return response()->json(['msg'=>'Cliente no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
