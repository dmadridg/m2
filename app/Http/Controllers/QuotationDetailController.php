<?php

namespace App\Http\Controllers;

use App\CotizacionDetalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuotationDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quotationDetail = CotizacionDetalle::where('IdCotizacionDetalle',$id)->first();
        // dd(Storage::url($quotationDetail['Image']));
        // $contents = Storage::url();
        $file = 'assets/models/specials/'.$quotationDetail['Image'];
        
        // dd(public_path($file));

        $quotationDetail['ImagePath'] = asset('storage') .'/'.$quotationDetail['Image'];

        return response()->json(['data'=>$quotationDetail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quotationDetail = CotizacionDetalle::where('IdCotizacionDetalle',$id);
        if($quotationDetail != null){
            $quotationDetail->delete();
        }
        return response()->json(['msg'=>'Producto eliminado correctamente']);
    }
}
