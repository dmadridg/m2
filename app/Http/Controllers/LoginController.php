<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    public function getLogin(){
        if (!Auth::check()) {
            return view('auth.login');
        }
    }
    public function authenticate(Request $request){
        $login = $request->email;
        $password = ($request->password);
        if (Auth::attempt(['Email' => $login,'password' => $password])) {

            if (Auth::user()){
                    return view('main.dashboard');
            }
        }
        else{
            return redirect()->route('login')->withErrors(['msg' => 'Usuario o contraseña incorrectos'])->withInput($request->except('password'));
        }
        return redirect()->route('login')->withErrors(['msg' => 'Usuario o contraseña incorrectos'])->withInput($request->except('password'));
    }

}
