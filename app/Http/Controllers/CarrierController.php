<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transportista;
class CarrierController extends Controller
{
    public $carriers;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->carriers = Transportista::search($request->name)->paginate(5);
        $this->carriers->setPath('carriers');
        $this->numRecords = count($this->carriers);

        $this->data = ['carriers'=>$this->carriers,'title'=>'Listado de Transportistas','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron transportistas';
        }
        return view('main.carriers.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $carrier = new Transportista();
            $carrier->fill($request->all());
            if ($carrier->save()){
                return response()->json(['msg'=>'Transportista creado correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear el transportista'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $carrier = Transportista::find($id);
            if($carrier != null){
                return response()->json($carrier);
            }else{
                return response()->json(['msg'=>"Transportista no encontrado"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carrier = Transportista::find($id);
        if($carrier != null){
            $carrier->fill($request->all());
            $carrier->save();
            return response()->json(['msg'=>'El Transportista ha sido actualizado correctamente'],200);
        }else{
            return response()->json(['msg'=>'Transportista no encontrado'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
