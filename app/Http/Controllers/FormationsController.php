<?php

namespace App\Http\Controllers;

use App\Formacionesbasedetalle;
use Illuminate\Http\Request;
use App\Formacionesbase;
class FormationsController extends Controller
{
    public $formations;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->formations = Formacionesbase::search($request->name)->paginate(5);
        $this->formations->setPath('formations');
        $this->numRecords = count($this->formations);

        $this->data = ['formations'=>$this->formations,'title'=>'Listado de Formaciones Base','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron Formaciones base';
        }
        return view('main.formations.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $formation = new Formacionesbase();
            $formation->fill($request->all());
            if ($formation->save()){
                return response()->json(['msg'=>'Formacion base creada correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear la formación base'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $formation = Formacionesbase::where('IdFormacionBase','=',$id)->with('detail','detail.process','detail.material')->first();
            if($formation != null){
                return response()->json($formation);
            }else{
                return response()->json(['msg'=>"Formación base no encontrada"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formation = Formacionesbase::find($id);
        if($formation != null){
            $formation->fill($request->all());
            $formation->save();
            return response()->json(['msg'=>'La formación base ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Formación base no encontrada'],404);
        }
    }
    public function removeDetailFormation($idFormation,$idDetail){
        try{
            $deleted = Formacionesbasedetalle::where('idFormacionBase','=',$idFormation)->where('idFormacionBaseDetalle','=',$idDetail)->delete();
            if($deleted){
                return response()->json(["msg"=>'Registro borrado correctamente']);
            }else{
                return response()->json(["msg"=>'Ocurrió un error, intentelo más tarde'],500);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
        }
    }

    public function addBaseFormation(Request $request,$id){

      try{
          $formation = Formacionesbase::find($id);
          if($formation == null){
              return response()->json(['msg'=>'Formacion no encontrada'],404);
          }
          $formationDetail = new Formacionesbasedetalle();
          $formationDetail->IdFormacionBase = $id;
          $formationDetail->IdMaterial = $request->IdMaterial;
          $formationDetail->IdProceso = $request->IdProceso;
          $formationDetail->Cantidad = $request->Cantidad;
          $formationDetail->EsTablero = $request->EsTablero;
          $formationDetail->IdCategoriaColor = $request->IdCategoriaColor;
          if(isset($request->Corte)){
              $formationDetail->Corte = 1;
          }else{
              $formationDetail->Corte = 0;
          }
          if(isset($request->Router)){
              $formationDetail->Router = 1;
          }else{
              $formationDetail->Router = 0;
          }
          if(isset($request->Curvos)){
              $formationDetail->Curvos = 1;
          }else{
              $formationDetail->Curvos = 0;
          }
          if(isset($request->Puertas)){
              $formationDetail->Puertas = 1;
          }else{
              $formationDetail->Puertas = 0;
          }
          if(isset($request->Molduras)){
              $formationDetail->Molduras = 1;
          }else{
              $formationDetail->Molduras = 0;
          }
          if(isset($request->Costados)){
              $formationDetail->Costados = 1;
          }else{
              $formationDetail->Costados = 0;
          }
          if(isset($request->Pantallas)){
              $formationDetail->Pantallas = 1;
          }else{
              $formationDetail->Pantallas = 0;
          }
          if(isset($request->Entrepanos)){
              $formationDetail->Entrepanos = 1;
          }else{
              $formationDetail->Entrepanos = 0;
          }
          if(isset($request->Divisiones)){
              $formationDetail->Divisiones = 1;
          }else{
              $formationDetail->Divisiones = 0;
          }
          if(isset($request->Archiveros)){
              $formationDetail->Archiveros = 1;
          }else{
              $formationDetail->Archiveros = 0;
          }
          if(isset($request->Barrenos)){
              $formationDetail->Barrenos = 1;
          }else{
              $formationDetail->Barrenos = 0;
          }
          if(isset($request->Zoclos)){
              $formationDetail->Zoclos = 1;
          }else{
              $formationDetail->Zoclos = 0;
          }

              $formationDetail->Chapas = $request->Chapas;

              $formationDetail->PVC = $request->PVC;

          $formationDetail->save();
          $formation->load(['detail','detail.process','detail.material']);
          return response()->json(['msg'=>'La formación agregada correctamente','formation'=>$formation],200);
      }catch (Exception $e){
          return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
