<?php

namespace App\Http\Controllers;
use App\Categoriascolores;
use Illuminate\Http\Request;

class ColorsCategoryController extends Controller
{
    public $categories;
    public $numRecords;
    public $data;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->categories = Categoriascolores::search($request->name)->with(['colors'])->paginate(10);
        $this->categories->setPath('categories');
        $this->numRecords = count($this->categories);

        $this->data = ['categories'=>$this->categories,'title'=>'Listado de Categorias de colores','records'=>$this->numRecords];
        if($this->numRecords == 0 ){
            $this->data['error_msg']  = 'No se encontraron categorias';
        }
        return view('main.colors.categories.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $color = new Categoriascolores();
            $color->fill($request->all());
            if ($color->save()){
                return response()->json(['msg'=>'Categoria creada correctamente']);
            }else{
                return response()->json(['msg'=>'No fue posible crear la categoría'],500);
            }
        }
        catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $color = Categoriascolores::where('IdCategoriaColor','=',$id)->with(['colors'])->first();
            if($color != null){
                return response()->json($color);
            }else{
                return response()->json(['msg'=>"Categoria no encontrada"],404);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $color = Categoriascolores::find($id);
        if($color != null){
            $color->fill($request->all());
            $color->save();
            return response()->json(['msg'=>'El color ha sido actualizada correctamente'],200);
        }else{
            return response()->json(['msg'=>'Categoría no encontrada'],404);
        }
    }
    public function getColors($id){
        try{
            $category = Categoriascolores::where('IdCategoriaColor','=',$id)->with(['colors'])->get();
            if($category == null){
                return response()->json(['msg'=>'No se encontró la categoría solicitada']);
            }
            return response()->json(['category'=>$category]);
        }
        catch(Exception $e){

        }
    }
    public function addColor(Request $request){
        try{
            $category = Categoriascolores::find($request->idCategory);
            if($category != null){
                if($category->colors->contains($request->color)){
                    return response()->json(['msg'=>'Ya has asociado este color a la categoria'],401);
                }
                $category->colors()->attach($request->color);
                $category->load('colors');
                return response()->json(['msg'=>'Color asociado correctamente','colors'=>$category->colors]);
            }else {
                return response()->json(['msg'=>'categoria no encontrada'],404);
            }
        }
        catch(Exeption $e){
        return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
        }
    }
    public function removeColor($id,$idColor){
        try{
            $category = Categoriascolores::find($id);
            if($category != null) {
                $category->colors()->detach($idColor);
                $category->load('colors');
                return response()->json(['msg'=>'El color ha sido borrado correctamente','colors'=>$category->colors]);
            }
        }
        catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage(),'status'=>false],$e->getCode());
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
