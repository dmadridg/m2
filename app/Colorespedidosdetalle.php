<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Colorespedidosdetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'colorespedidosdetalle';
    protected $fillable = ['IdColorPedidoDetalle', 'IdPedidoDetalle', 'IdCategoriasColores', 'IdColores', 'NombreColor'];



}
