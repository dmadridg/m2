<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Agente extends Model {

    /**
     * Generated
     */

    protected $table = 'agentes';
    protected $fillable = ['IdAgente', 'Nombre', 'ApellidoPaterno', 'ApellidoMaterno', 'Edad'];



}
