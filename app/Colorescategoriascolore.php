<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Colorescategoriascolore extends Model {

    /**
     * Generated
     */

    protected $table = 'colorescategoriascolores';
    protected $fillable = ['IdColorCategoriaColor', 'IdCategoriaColor', 'IdColor'];
    protected  $primaryKey = 'IdColorCategoriaColor';

    public function colors(){
        return $this->hasMany('App\Colore','IdColor','IdColor');
    }
}
