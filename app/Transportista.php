<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportista extends Model {

    /**
     * Generated
     */

    protected $table = 'transportistas';
    protected $fillable = ['IdTransportista', 'Codigo', 'Nombre'];
    protected $primaryKey = 'IdTransportista';

    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%");
    }
}
