<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tablero extends Model {

    /**
     * Generated
     */

    protected $table = 'tableros';
    protected $fillable = ['IdTablero', 'Nombre', 'Descripcion', 'Espesor', 'Costo', 'Tipo'];
    protected $primaryKey = 'IdTablero';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%");
    }


}
