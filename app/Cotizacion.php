<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'IdCotizacion';
    protected $table = 'cotizaciones';
    protected $fillable = ['IdPedido', 'IdCliente', 'IdTransportista', 'FechaCotizacion', 'Estatus', 'Referencia', 'Observaciones'];

    public function scopeSearch($query,$nameOrId){
          $search = trim($nameOrId);
        $query->join('clientes','cotizaciones.IdCliente','=','clientes.IdCliente');
        $query->where(function($query) use ($search) {
            $query->orWhere('IdCotizacion', 'like', '%' . $search . '%');
            $query->orWhere('clientes.Nombre','like','%'. $search .'%');
        });
        return $query;
    }
    public function client(){
        return $this->belongsTo('App\Cliente','IdCliente','IdCliente');
    }
    public function detail() {
        return $this->hasMany('App\CotizacionDetalle','IdCotizacion','IdCotizacion');
    }
}
