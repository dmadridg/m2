<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Materiales extends Model {

    /**
     * Generated
     */

    protected $table = 'materiales';
    protected $fillable = ['IdMaterial', 'TipoMaterial', 'Clave', 'NombreComercial', 'Clasificacion', 'IdCategoriaColor', 'Familia', 'IdUnidadMedida', 'CostoPorUnidad', 'IdTablero', 'Ancho', 'Largo', 'CostoCalculado', 'IdAlmacen'];
    protected $primaryKey = 'IdMaterial';
    public $timestamps = false;

    public function scopeSearch($query,$name){
        return $query->where('Clave','LIKE',"%$name%")->with(['tablero'])->orderBy('Clave');
    }
    public function tablero(){
        return $this->belongsTo('App\Tablero','IdTablero','IdTablero');
    }

}
