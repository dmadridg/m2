<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model {

    /**
     * Generated
     */

    protected $table = 'ciudad';
    protected $fillable = ['IdCiudad', 'NombreCiudad', 'IdEstado'];



}
