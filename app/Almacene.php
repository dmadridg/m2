<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacene extends Model {

    /**
     * Generated
     */

    protected $table = 'almacenes';
    protected $fillable = ['IdAlmacen', 'Clave', 'NombreAlmacen'];



}
