<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class PedidosBak extends Model {

    /**
     * Generated
     */

    protected $table = 'pedidos_bak';
    protected $fillable = ['IdPedido', 'FolioPedido', 'IdCotizacion', 'IdCliente'];



}
