<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Empleado extends Authenticatable {

    /**
     * Generated
     */
    use Notifiable;
    protected $table = 'empleados';
    protected $primaryKey = "IdEmpleado";
    protected $fillable = ['IdEmpleado', 'nombre', 'cargo', 'email', 'login', 'password'];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
