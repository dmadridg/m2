<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidadesmedida extends Model {

    /**
     * Generated
     */

    protected $table = 'unidadesmedidas';
    protected $fillable = ['IdUnidadMedida', 'Nombre', 'Descripcion'];
    protected $primaryKey = 'IdUnidadMedida';
    public $timestamps = false;
    public function scopeSearch($query,$name){
        return $query->where('Nombre','LIKE',"%$name%");
    }
}
