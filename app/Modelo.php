<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model {

    /**
     * Generated
     */

    protected $table = 'modelos';
    protected $fillable = ['IdModelo', 'Nombre', 'Clave', 'Descripcion', 'IdLinea', 'Precio1','Imagen'];
    protected $primaryKey = 'IdModelo';
    protected  $appends = ['Imagen'];
    public $timestamps = false;
    private static $basePath = '/assets/models/';
    public function scopeSearch($query,$name){
        return $query->where('Clave','LIKE',"%$name%")->with('line')->orderBy('Clave');
    }
    public function getImagenAttribute(){
        if($this->attributes['Imagen'] !="" || $this->attributes['Imagen'] !=null){
            return asset(self::$basePath.$this->attributes['Imagen']);
        }
        return null;
    }
    public function scopeSearchModelQuotation($query,$name,$clave) {
        if($name != '') {
            $query->where('Descripcion','LIKE',"%$name%");
        }
        if($clave != ''){
            $query->where('Clave','LIKE',"%$clave%");
        }
        return $query->with('line');
    }
    public function line(){
        return $this->hasOne('App\Linea','IdLinea','IdLinea');
    }
    public function formations(){
        return $this->hasMany('App\Formacionesmodelo','IdModelo','IdModelo');
    }
}
