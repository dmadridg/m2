<?php namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Pedidosdetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'pedidosdetalle';
    protected $fillable = ['IdPedidoDetalle', 'IdPedido', 'IdModelo', 'Cantidad', 'Precio', 'Area', 'Lados', 'Descuento', 'Adicional', 'SubTotal', 'Total', 'Observaciones', 'Tipo', 'SubLote'];



}
