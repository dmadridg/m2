(function(){
    var units = {
        id:null,
        init:function () {

            $('#buttonNewUnit').on('click',function(){
                $('#modalNewUnit').modal()
            });
            this.setValidator();
            this.saveUnit();
            this.updateUnit();

            $('.editUnit').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');

                $.ajax({
                    url:'units/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var unit = response
                        units.id = unit.IdUnidadMedida;
                        for (var property in unit){
                            $('#'+property+'Edit').val(unit[property])
                        }
                        $("#modalEditUnit").modal();
                    },error:function(){

                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },

        updateUnit:function(){
            $("#editUnitForm").validate({
                rules: {
                    Descripcion: {required: true},
                    Nombre:{required:true},
                },submitHandler: function() {
                    var data = $('#editUnitForm').serialize();
                    $.ajax({
                        url:'units/'+units.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewUnit").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveUnit:function(){

            $("#newUnitForm").validate({
                rules: {
                    Nombre: {required: true},
                    Descripcion:{required:true},

                },submitHandler: function() {
                    var data = $('#newUnitForm').serialize();
                    $.ajax({
                        url:'units',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewUnit").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        units.init()
    })
})();