(function(){
    var categories = {
        id:null,
        colors:[],
        init:function () {

            $('#idColor').select2({
                theme:'bootstrap'
            });
            $('#buttonNewCategory').on('click',function(){
                $('#modalNewCategory').modal()
            });
            this.getCatalogs();
            this.setValidator();
            this.saveCategory();
            this.updateCategory();
            $('.buttonAddColor').on('click',function(e){
               e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url:'categories/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var category = response;
                        $('#ClaveColor').val(category.Clave);
                        $('#NombreColor').val(category.Nombre);
                        categories.id = category.IdCategoriaColor;
                        var colorsConcat = '';
                        category.colors.forEach(function(color){
                            colorsConcat +='<tr id="row'+color.IdColor+'">' +
                                '<td>'+ color.Nombre +'</td>' +
                                '<td class="text-center"> <button class="btn btn-dark buttonDeleteColor" data-id ="'+color.IdColor+'" ><i class="fa fa-trash"></i></button></td>'+
                                '</tr>';

                        });
                        $('#colorsInCategory').html(colorsConcat);
                        $('.buttonDeleteColor').on('click',function(){
                            var idColor = $(this).data('id');
                            swal({
                                title: "¿Seguro?",
                                text: "Deseas eliminar el color de esta categoría?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Si, estoy seguro!',
                                cancelButtonText: "No, cancelar"
                            },function(isConfirm){
                                if(isConfirm){
                                    $.ajax({
                                        url:'removeColorFromCategory/'+id+'/color/'+idColor,
                                        type:'delete',
                                        data:{},
                                        dataType:'json',
                                        success:function(response){
                                            swal({
                                                title : "Exito",
                                                text : response.msg,
                                                type : "success"
                                            },function(){});
                                            $('#row'+idColor).remove();
                                        },error:function(){}
                                    })
                                }
                            })

                        });
                        $("#modalAddColorCategory").modal();
                    },error:function(){

                    }
                });
            });
            $('#buttonConfirmColor').on('click',function(){
               var colorSelected = $('#idColor').val();
                $.ajax({
                    url:'addColor',
                    type:'post',
                    data:{'color':colorSelected,'idCategory':categories.id},
                    dataType:'json',
                    success:function(response){
                        swal("Exito!", response.msg, "success");
                        $('#colorsInCategory').html('');
                        var colorsConcat = '';
                        response.colors.forEach(function(color){
                                colorsConcat +='<tr id="row'+color.IdColor+'">' +
                                '<td>'+ color.Nombre +'</td>' +
                                '<td class="text-center"> <button class="btn btn-dark buttonDeleteColor" data-id ="'+color.IdColor+'" ><i class="fa fa-trash"></i></button></td>'+
                                '</tr>';

                        });
                        $('#colorsInCategory').html(colorsConcat);
                        $('.buttonDeleteColor').on('click',function(){
                            var idColor = $(this).data('id');
                            swal({
                                title: "¿Seguro?",
                                text: "Deseas eliminar el color de esta categoría?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Si, estoy seguro!',
                                cancelButtonText: "No, cancelar"
                            },function(isConfirm){
                                if(isConfirm){
                                    $.ajax({
                                        url:'removeColorFromCategory/'+id+'/color/'+idColor,
                                        type:'delete',
                                        data:{},
                                        dataType:'json',
                                        success:function(response){
                                            swal({
                                                title : "Exito",
                                                text : response.msg,
                                                type : "success"
                                            },function(){});
                                            $('#row'+idColor).remove();
                                        },error:function(){}
                                    })
                                }
                            })

                        });
                    },error:function(response){

                        swal("Atención!", response.responseJSON.msg, "warning");
                    }
                });
            });
            $('.editCategory').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');

                $.ajax({
                    url:'categories/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var category = response
                        categories.id = category.IdCategoriaColor;
                        for (var property in category){
                            $('#'+property+'Edit').val(category[property])
                        }
                        $("#modalEditCategory").modal();
                    },error:function(){

                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        getCatalogs:function(){
            $.ajax({
                url:baseUrl + 'catalogColors',
                type:'get',
                data:{},
                dataType:'json',
                success:function(response){
                    categories.colors = response;
                    categories.colors.forEach(function(color){
                        $('#idColor').append(new Option(color.Nombre, color.IdColor, false, false));
                    });
                },
                error:function(response){

                }
            })
        },
        updateCategory:function(){
            $("#editCategoryForm").validate({
                rules: {
                    Clave: {required: true},
                    Nombre:{required:true},
                },submitHandler: function() {
                    var data = $('#editCategoryForm').serialize();
                    $.ajax({
                        url:'categories/'+categories.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewCategory").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveCategory:function(){

            $("#newCategoryForm").validate({
                rules: {
                    Nombre: {required: true},
                    Clave:{required:true},

                },submitHandler: function() {
                    var data = $('#newCategoryForm').serialize();
                    $.ajax({
                        url:'categories',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewCategory").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){
                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        categories.init()
    })
})();