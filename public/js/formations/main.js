(function(){
    var formations = {
        id:null,
        init:function () {
            $('#PVC').attr('disabled',true);
            $('#Chapas').attr('disabled',true);
            $('#buttonNewFormation').on('click',function(){
                $('#modalNewFormation').modal()
            });
            this.setValidator();
            this.saveFormation();
            this.addFormation();
            this.updateFormation();
            this.getCatalogs();
            $('#IdMaterial').select2({
                theme:'bootstrap'
            });

            $('#ChapasChk').on('change',function(){
                var isChecked = $(this).is(":checked");
                if(!isChecked){
                    $('#Chapas').attr('disabled',true);
                }else{
                    $('#Chapas').attr('disabled',false);
                }
            });
            $('#ChapasPVC').on('change',function(){
                var isChecked = $(this).is(":checked");
                if(!isChecked){
                    $('#PVC').attr('disabled',true);
                }else{
                    $('#PVC').attr('disabled',false);
                }
            });

            $('.buttonAddFormation').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');
                $.ajax({
                    url:'formations/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        formations.id = response.IdFormacionBase;
                        $('#NombreFormacion').val(response.Nombre);
                        $('#ClaveFormacion').val(response.Clave);

                        var details = response.detail;
                        var detailConcat = '';
                        details.forEach(function(detail){
                            detailConcat +='<tr id="row'+detail.IdFormacionBaseDetalle+'"><td>'+ response.Clave +'</td>'+
                                '<td>'+detail.material.NombreComercial+'</td>'+
                                '<td>'+detail.process.NombreProceso+'</td>'+
                                '<td>'+detail.Cantidad+'</td>'+
                                '<td>'+detail.EsTablero+'</td>'+
                                '<td>'+detail.material.Largo+'x'+detail.material.Ancho+'</td>'+
                                '<td> <button class="btn btn-dark buttonDeleteDetail" data-id ="'+detail.IdFormacionBaseDetalle+'" ><i class="fa fa-trash"></i></button></td>'+
                                +'</tr>';
                        });
                        $('#detailsInFormation').html(detailConcat);
                        $('.buttonDeleteDetail').on('click',function(){
                            var id = $(this).data('id');
                            $.ajax({
                                url:'remove/'+formations.id+'/detail/'+id,
                                type:'delete',
                                data:{},
                                dataType:'json',
                                success:function(response){
                                    swal({
                                        title : "Exito",
                                        text : response.msg,
                                        type : "success"
                                    },function(){

                                    });
                                    $('#row'+id).remove();
                                },error:function(){

                                }
                            })
                        });

                        $("#modalAddFormation").modal();
                    },error:function(){

                    }
                });
            });
            $('.editFormation').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');
                $.ajax({
                    url:'formations/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var formation = response
                        formations.id = formation.IdFormacionBase;
                        for (var property in formation){
                            $('#'+property+'Edit').val(formation[property])
                        }
                        $("#modalEditFormation").modal();
                    },error:function(){

                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        getCatalogs:function(){
            $.ajax({
                url:baseUrl + 'catalogsNewFormation',
                type:'get',
                data:{},
                dataType:'json',
                success:function(response){
                    var processes = response.processes;
                    var materials = response.materials;
                    var category = response.categories;
                    processes.forEach(function(process){
                        $('#IdProceso').append(new Option(process.NombreProceso, process.IdProceso, false, false));
                    });
                    materials.forEach(function(material){
                        $('#IdMaterial').append(new Option(material.Clave+' - '+material.NombreComercial, material.IdMaterial, false, false));
                    });
                    category.forEach(function(category){
                        $('#IdCategoriaColor').append(new Option(category.Nombre, category.IdCategoriaColor, false, false));
                        $('#IdCategoriaColorEdit').append(new Option(category.Nombre, category.IdCategoriaColor, false, false));
                    });
                },
                error:function(response){

                }
            })
        },
        updateFormation:function(){
            $("#editFormationForm").validate({
                rules: {
                    Clave: {required: true},
                    Nombre:{required:true},
                },submitHandler: function() {
                    var data = $('#editFormationForm').serialize();
                    $.ajax({
                        url:'formations/'+formations.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewFormation").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        addFormation:function(){
            $("#addFormationForm").validate({
                rules: {
                    Cantidad: {required: true}
                },submitHandler: function() {
                    var data = $('#addFormationForm').serialize();
                    $.ajax({
                        url:'addBaseFormation/'+formations.id,
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){

                            $('#detailsInFormation').html('');
                            var details = response.formation.detail;
                            var detailConcat = '';
                            details.forEach(function(detail){
                                detailConcat +='<tr id="row'+detail.IdFormacionBaseDetalle+'"><td>'+ response.formation.Clave +'</td>'+
                                    '<td>'+detail.material.NombreComercial+'</td>'+
                                    '<td>'+detail.process.NombreProceso+'</td>'+
                                    '<td>'+detail.Cantidad+'</td>'+
                                    '<td>'+detail.EsTablero+'</td>'+
                                    '<td>'+detail.material.Largo+'x'+detail.material.Ancho+'</td>'+
                                    '<td> <button  class="btn btn-dark buttonDeleteDetail" data-id ="'+detail.IdFormacionBaseDetalle+'"  ><i class="fa fa-trash"></i></button></td>'+
                                    +'</tr>';



                            });
                            $('#detailsInFormation').html(detailConcat);
                            $('.buttonDeleteDetail').on('click',function(){
                                var id = $(this).data('id');
                                $.ajax({
                                    url:'remove/'+formations.id+'/detail/'+id,
                                    type:'delete',
                                    data:{},
                                    dataType:'json',
                                    success:function(response){
                                        swal({
                                            title : "Exito",
                                            text : response.msg,
                                            type : "success"
                                        },function(){

                                        });
                                        $('#row'+id).remove();

                                    },error:function(){

                                    }
                                })
                            });

                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                               // $("#modalAddFormation").modal('hide');
                                //location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveFormation:function(){

            $("#newFormationForm").validate({
                rules: {
                    Nombre: {required: true},
                    Clave:{required:true},

                },submitHandler: function() {
                    var data = $('#newFormationForm').serialize();
                    $.ajax({
                        url:'formations',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewFormation").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        formations.init()
    })
})();