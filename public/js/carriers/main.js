(function(){
    var carriers = {
        id:null,
        init:function () {

            $('#buttonNewCarrier').on('click',function(){
                $('#modalNewCarrier').modal()
            });
            this.setValidator();
            this.saveCarrier();
            this.updateCarrier();

            $('.editCarrier').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');
                $.ajax({
                    url:'carriers/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var carrier = response
                        carriers.id = carrier.IdTransportista;
                        for (var property in carrier){
                            $('#'+property+'Edit').val(carrier[property])
                        }
                        $("#modalEditCarrier").modal();
                    },error:function(){

                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },

        updateCarrier:function(){
            $("#editCarrierForm").validate({
                rules: {
                    Codigo: {required: true},
                    Nombre:{required:true},
                },submitHandler: function() {
                    var data = $('#editCarrierForm').serialize();
                    $.ajax({
                        url:'carriers/'+carriers.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewCarrier").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveCarrier:function(){

            $("#newCarrierForm").validate({
                rules: {
                    Nombre: {required: true},
                    Codigo:{required:true},

                },submitHandler: function() {
                    var data = $('#newCarrierForm').serialize();
                    $.ajax({
                        url:'carriers',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewCarrier").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        carriers.init()
    })
})();