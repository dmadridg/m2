(function(){
    var lines = {
        id:null,
        init:function () {

            $('#buttonNewLine').on('click',function(){
                $('#modalNewLine').modal()
            });
            this.setValidator();
            this.saveLine();
            this.updateLine();

            $('.editLine').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');

                $.ajax({
                    url:'lines/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var line = response
                        lines.id = line.IdLinea;
                        for (var property in line){
                            $('#'+property+'Edit').val(line[property])
                        }
                        $("#modalEditLine").modal();
                    },error:function(){

                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },

        updateLine:function(){
            $("#editLineForm").validate({
                rules: {
                    Incremento: {required: true},
                    Descuento: {required: true},
                    Nombre:{required:true}
                },submitHandler: function() {
                    var data = $('#editLineForm').serialize();
                    $.ajax({
                        url:'lines/'+lines.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewLine").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveLine:function(){

            $("#newLineForm").validate({
                rules: {
                    Incremento: {required: true},
                    Descuento: {required: true},
                    Nombre:{required:true}

                },submitHandler: function() {
                    var data = $('#newLineForm').serialize();
                    $.ajax({
                        url:'lines',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewLine").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        lines.init()
    })
})();