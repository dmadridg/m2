(function(){
    var users = {
        id:null,
        init:function () {

            $('#buttonNewUser').on('click',function(){
                $('#modalNewUser').modal()
            });
            this.setValidator();
            this.saveUser();
            this.updateUser();

            $('.editUser').on('click',function(e){
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url:'users/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){
                        var user = response;
                        users.id = user.IdEmpleado;
                        for (var property in user){
                            $('#'+property+'Edit').val(user[property])
                        }
                        $("#modalEditUser").modal();
                    },error:function(error){
                        console.log(error);
                        var error = error.msg;
                        console.log(error);
                        swal({
                            title : "Atención",
                            text : error,
                            type : "warning"
                        });
                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        updateUser:function(){
            $("#editUserForm").validate({
                rules: {
                    Nombre: {required: true},
                    Email:{required:true},
                    Cargo:{required:true}
                },submitHandler: function() {
                    var data = $('#editUserForm').serialize();
                    $.ajax({
                        url:'users/'+users.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalEditUser").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(error){

                            var myError = error.responseJSON.msg;
                            swal({
                                title : "Atención",
                                text : myError,
                                type : "warning"
                            });
                        }
                    })
                }
            });
        },
        saveUser:function(){

            $("#newUserForm").validate({
                rules: {
                    Nombre: {required: true},
                    Email:{required:true,email:true},
                    Cargo:{required: true},
                    Password:{required: true}
                },submitHandler: function() {
                    var data = $('#newUserForm').serialize();
                    $.ajax({
                        url:'users',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewUser").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        users.init()
    })
})();