(function(){
    var materials = {
        id:null,
        tabSelected:1,
        init:function () {

            $('#buttonNewMaterial').on('click',function(){
                $('#modalNewMaterial').modal();
                $('#typeMaterial').val('HERRAJE');
                $('#typeMaterialEdit').val('HERRAJE');
                materials.tabSelected = 1;
                $('#pills-herrajes-tab').addClass('active');
                $('#pills-componentes-tab').removeClass('active');
            });
            this.getCatalogs();
            this.setValidator();
            this.saveMaterial();
            this.updateMaterial();
            $('#pills-herrajes-tab,#pills-componentes-tab,#pills-herrajes-tabEdit,#pills-componentes-tabEdit').on('click',function(){
                var selected = $(this).data('selected');
                materials.typeClicked(selected)
            });
            $('#pills-herrajes-tabEdit,#pills-componentes-tabEdit').on('click',function(){
                var selected = $(this).data('selected');
                materials.typeClickedEdit(selected)
            });
            $('.editMaterial').on('click',function(e){
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    url:'materials/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var material = response;
                        if(material.TipoMaterial == 'Componente'){
                            $('#typeMaterialEdit').val('COMPONENTE');
                            materials.tabSelected = 2;
                            $('#pills-herrajes-tabEdit').removeClass('active');
                            $('#pills-herrajesEdit').removeClass('active show');

                            $('#pills-componentes-tabEdit').addClass('active');
                            $('#pills-componentesEdit').addClass('active show');
                        }else{
                            $('#typeMaterialEdit').val('HERRAJE');
                            materials.tabSelected = 1;
                            $('#pills-herrajesEdit').addClass('active show');
                            $('#pills-herrajes-tabEdit').addClass('active');
                            $('#pills-componentes-tabEdit').removeClass('active');
                            $('#pills-componentesEdit').removeClass('active show');

                        }
                        materials.id = material.IdMaterial;
                        for (var property in material){
                            $('#'+property+'Edit').val(material[property])
                        }
                        $('#CostoCalculadoEdit').val('$'+material.costo_calculado);
                        $("#modalEditMaterial").modal();
                    },error:function(){

                    }
                })
            })
        },
        typeClickedEdit:function(selected){
            materials.tabSelected = selected;
            if(materials.tabSelected == 1){
                $('#typeMaterialEdit').val('HERRAJE');
            }else{
                $('#typeMaterialEdit').val('COMPONENTE');
            }
        },
        typeClicked:function(selected){
            materials.tabSelected = selected;
            if(materials.tabSelected == 1){
                $('#typeMaterial').val('HERRAJE');
            }else{
                $('#typeMaterial').val('COMPONENTE');
            }
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        getCatalogs:function(){
            $.ajax({
                url:baseUrl + 'catalogsNewMaterial',
                type:'get',
                data:{},
                dataType:'json',
                success:function(response){
                    var boards = response.boards;
                    var warehouses = response.warehouses;
                    var units = response.units;
//                    var category = response.categories;

                    boards.forEach(function(board){
                        $('#IdTablero').append(new Option(board.Nombre, board.IdTablero, false, false));
                        $('#IdTableroEdit').append(new Option(board.Nombre, board.IdTablero, false, false));

                    });
                    warehouses.forEach(function(warehouse){
                        $('#IdAlmacen').append(new Option(warehouse.NombreAlmacen, warehouse.IdAlmacen, false, false));
                        $('#IdAlmacenEdit').append(new Option(warehouse.NombreAlmacen, warehouse.IdAlmacen, false, false));
                    });
                    units.forEach(function(unit){
                        $('#IdUnidadMedida').append(new Option(unit.Nombre, unit.IdUnidadMedida, false, false));
                        $('#IdUnidadMedidaEdit').append(new Option(unit.Nombre, unit.IdUnidadMedida, false, false));

                    });
                    /*category.forEach(function(category){
                        $('#IdCategoriaColor').append(new Option(category.Nombre, category.IdCategoriaColor, false, false));
                        $('#IdCategoriaColorEdit').append(new Option(category.Nombre, category.IdCategoriaColor, false, false));
                    });*/
                },
                error:function(response){
                    //error handler
                }
            })
        },
        updateMaterial:function(){
            $("#editMaterialForm").validate({
                rules: {
                    Descripcion: {required: true},
                    Nombre:{required:true}
                },submitHandler: function() {
                    var data = $('#editMaterialForm').serialize();
                    $.ajax({
                        url:'materials/'+materials.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalEditMaterial").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveMaterial:function(){

            $("#newMaterialForm").validate({
                rules: {
                    NombreComercial: {required: true},
                    Clave:{required:true},
                    IdUnidadMedida:{
                        required:function(element) {
                            return materials.tabSelected == 1;
                        }
                    },
                    CostoPorUnidad:{ required:function(element) {
                        return materials.tabSelected == 1;
                    }},
                    Ancho:{ required:function(element) {
                        return materials.tabSelected == 2;
                    }},
                    Largo:{ required:function(element) {
                        return materials.tabSelected == 2;
                    }},
                    IdTablero:{ required:function(element) {
                        return materials.tabSelected == 2;
                    }}


                },submitHandler: function() {
                    var data = $('#newMaterialForm').serialize();
                    $.ajax({
                        url:'materials',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewMaterial").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        materials.init()
    })
})();