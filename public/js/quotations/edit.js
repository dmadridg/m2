(function () {
    var quotationsEdit = {
        id: null,
        clients: [],
        currentClient: null,
        currentQuotation: null,
        currentAddProduct: null,
        productsInQuotation: [],
        current: null,
        idClient: null,
        tags: [],
        init: function () {

            $('#btnAddSpecialProductEdit').on('click', function () {
                $('#modalAddSpecialProductEdit').modal();
            });
            $('#buttonSaveProductSpecialEdit').on('click', function () {
                quotationsEdit.addSpecialModelToQuotation()
            });
            $('#buttonEditProductSpecialEdit').on('click', function () {
                quotationsEdit.EditSpecialModelToQuotation()
            });
            $('#buttonEditProductToQuotationEdit').on('click', function () {
                quotationsEdit.EditProductModelToQuotation()
            });
            ;
            $('#buttonAddProductToQuotationEdit').on('click', function () {
                quotationsEdit.addModelToQuotation()
            });
            $('#buttonShowObservationsEdit').on('click', function () {
                $('#modalObservationsEdit').modal();
            });
            $('#buttonSaveQuotationEdit').on('click', function () {
                quotationsEdit.saveQuotation();
            });
            $('#modalQuotationEdit').on("hidden.bs.modal", function () {
                quotationsEdit.currentClient = quotationsEdit.clients[0];
                $('#detailsInFormationEdit').html('');
                $('#detailsInFormationQuotationEdit').html('');
                quotationsEdit.currentAddProduct = null;
                quotationsEdit.currentQuotation = null;
            });
            $('#modalObservationsEdit').on("hidden.bs.modal", function () {
                // $('#observationsEdit').val('');
            });
            $("#modalAddProductQuotationEdit").on("hidden.bs.modal", function () {
                $('#containerColorCategoriesEdit').html('');
                $("#addProductQuotationFormEdit")[0].reset();
            });
            $("#modalAddSpecialProductEdit").on("hidden.bs.modal", function () {
                // TODO
            });
            $("#modalAddSpecialProductEdit").on("show.bs.modal", function () {
                quotationsEdit.currentAddProduct = {
                    IdModelo: 1452,
                    Nombre: 'ESP001',
                    Clave: 'ESP001',
                    Descripcion: 'Articulo Especial'
                }
            });
            $("#modalEditSpecialProductEdit").on("show.bs.modal", function () {
                quotationsEdit.currentAddProduct = {
                    IdModelo: 1452,
                    Nombre: 'ESP001',
                    Clave: 'ESP001',
                    Descripcion: 'Articulo Especial'
                }
            });
            $('#IdClienteEdit').select2({
                theme: 'bootstrap'
            });
            this.getCatalogs();
            this.setValidator();
            this.updateQuotation();
            $('#btnSearchModelsEdit').on('click', function () {
                quotationsEdit.getModels();
            });

            $('.editCotizacion').on('click', function (e) {

                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url: 'quotations/' + id,
                    type: 'get',
                    data: {},
                    dataType: 'json',
                    success: function (response) {
                        var quotation = response;
                        // console.log(quotation);
                        quotationsEdit.currentQuotation = quotation;
                        var models = quotation.detail;
                        quotationsEdit.idClient = quotation.client.IdCliente;
                        quotationsEdit.currentClient = quotation.client;
                        for (var property in quotationsEdit.currentClient) {
                            $('#' + property + 'Edit').val(quotationsEdit.currentClient[property])
                        }
                        $('#FechaEdit').val(quotation.FechaCotizacion);
                        $('#idQuotation').val(id);
                        $('#DireccionEdit').val(quotationsEdit.currentClient.DomicilioFiscal);
                        $('#CiudadEdit').val(quotationsEdit.currentClient.city.NombreCiudad);
                        $('#IdClienteEdit').val(quotationsEdit.currentClient.IdCliente).trigger('change');
                        $('#observationsEdit').val(quotation.Observaciones);


                        // detailsInFormationQuotationEarlyEdit
                        $('#detailsInFormationQuotationEarlyEdit').html('');
                        var detailConcat = '';

                        models.forEach(function (productInQuotation) {
                            var totalAmount = (parseInt(productInQuotation.Precio) * productInQuotation.Cantidad);
                            var substraction = totalAmount * (parseInt(productInQuotation.Descuento) / 100);
                            var totalWithDiscount = totalAmount - substraction;
                            totalWithDiscount = '$' + totalAmount.toLocaleString();
                            detailConcat += '<tr id="rowProductEditEarly' + productInQuotation.IdCotizacionDetalle + '" class="detailModelQuotationEdit" data-id ="' + productInQuotation.model.IdModelo + '">' +
                                '<td>' + productInQuotation.model.Clave + '</td>' +
                                '<td>' + productInQuotation.Descripcion + '</td>' +
                                '<td>' + (productInQuotation.Medida) + '</td>' +
                                '<td>' + (productInQuotation.Precio).toLocaleString() + '</td>' +
                                '<td>' + (productInQuotation.Cantidad) + '</td>' +
                                '<td>' + (productInQuotation.Descuento) + '</td>' +
                                '<td>' + (totalWithDiscount) + '</td>' +
                                '<td>' + (productInQuotation.Area) + '</td>' +
                                '<td class="text-center" style="display: inline-flex;"> <button class="btn btn-dark buttonEditProductEdit" data-id ="' + productInQuotation.IdCotizacionDetalle + '" ><i class="fa fa-pencil"></i></button><button class="btn btn-dark buttonDeleteProductEdit" data-id ="' + productInQuotation.IdCotizacionDetalle + '" ><i class="fa fa-trash"></i></button></td>' +
                                +'</tr>';
                        });
                        $('#detailsInFormationQuotationEarlyEdit').html(detailConcat);
                        $('.buttonDeleteProductEdit').on('click', function (e) {
                            e.preventDefault();
                            var idQuotationDetail = $(this).data('id');
                            swal({
                                title: "¿Seguro?",
                                text: "Deseas eliminar este producto de la cotización?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Si, estoy seguro!',
                                cancelButtonText: "No, cancelar"
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    $.ajax({
                                        url: 'quotationsDetail/' + idQuotationDetail,
                                        type: 'delete',
                                        data: {},
                                        dataType: 'json',
                                        success: function (response) {
                                            swal({
                                                title: "Exito",
                                                text: response.msg,
                                                type: "success"
                                            }, function () {
                                                $('#rowProductEditEarly' + idQuotationDetail).remove();
                                            });
                                        },
                                        error: function () {

                                        }
                                    });
                                }
                            })
                        });
                        $('.buttonEditProductEdit').on('click', function (e) {

                            e.preventDefault();
                            var idQuotationDetail = $(this).data('id');
                            $.ajax({
                                url: 'quotationsDetail/' + idQuotationDetail + '/edit',
                                type: 'get',
                                data: {},
                                dataType: 'json',
                                success: function (response) {
                                    console.log(response.data);

                                    if (response.data.IdModelo == 1452){
                                        $('#idProductEdit').val(idQuotationDetail);
                                        $('#idModelEdit').val(response.data.IdModelo);
                                        $('#idModeloEditEdit').val(response.data.IdModelo);
                                        $('#cboAreaEspecialEdit').val(response.data.Area);
                                        $('#descripcionProductoEspecialEdit').val(response.data.Descripcion);
                                        $('#cantidadProductoEspecialEdit').val(response.data.Cantidad);

                                        $('#colorBaseEspecialEdit').val(response.data.colorBase);
                                        $('#colorBaseEspecialEdit').change();

                                        $('#ladoProductoEspecialEdit').val(response.data.Lados);
                                        $('#medidaProductoEspecialEdit').val(response.data.Medida);
                                        $('#precioProductoEspecialEdit').val(response.data.Precio);
                                        $('#descuentoProductoEspecialEdit').val(response.data.Descuento);

                                        $('#colorCombinacionEspecialEdit').val(response.data.colorCombiando);
                                        $('#colorCombinacionEspecialEdit').change();

                                        // $('#imgPreview').val(response.data.Image);
                                        if (response.data.IdModelo == 1452) {
                                            if (response.data.Image != '') {
                                                $("#imgPreview").attr("src", response.data.ImagePath);
                                            } else {
                                                $("#imgPreview").hide();
                                            }
                                        } else {
                                            $("#imgPreview").hide();
                                            $("#fileDiv").hide();
                                        }

                                        $('#modalEditSpecialProductEdit').modal();
                                    }else{
                                        $('#idProductEdit').val(idQuotationDetail);
                                        $('#idModelEdit').val(response.data.IdModelo);
                                        $('#idModeloEditEdit').val(response.data.IdModelo);
                                        
                                        $('#cboEditProductAreaEdit').val(response.data.Area);
                                        $('#descripcionEditProductProductoEdit').val(response.data.Descripcion);
                                        $('#cantidadEditProductProductoEdit').val(response.data.Cantidad);

                                        // $('#colorBaseEspecialEdit').val(response.data.colorBase);
                                        // $('#colorBaseEspecialEdit').change();

                                        $('#ladoEditProductProductoEdit').val(response.data.Lados);
                                        $('#medidaEditProductProductoEdit').val(response.data.Medida);
                                        $('#precioEditProductProductoEdit').val(response.data.Precio);
                                        $('#descuentoEditProductProductoEdit').val(response.data.Descuento);

                                        if(response.data.Adicional != 0){
                                            $('#costoEditProductAdicionalProductoEdit').prop('checked', true);
                                        }

                                        // $('#colorCombinacionEspecialEdit').val(response.data.colorCombiando);
                                        // $('#colorCombinacionEspecialEdit').change();

                                        // $('#imgPreview').val(response.data.Image);
                                        if (response.data.IdModelo == 1452) {
                                            if (response.data.Image != '') {
                                                $("#imgPreview").attr("src", response.data.ImagePath);
                                            } else {
                                                $("#imgPreview").hide();
                                            }
                                        } else {
                                            $("#imgPreview").hide();
                                            $("#fileDiv").hide();
                                        }
                                        
                                        $('#modalEditProductQuotationEdit').modal();
                                    }

                                    

                                },
                                error: function () {

                                }
                            });
                            // console.log(idQuotationDetail);
                        });
                        $("#modalQuotationEdit").modal();
                    },
                    error: function () {}
                })
            })
        },

        getModels: function () {
            var txtClave = $('#txtClaveModeloEdit').val();
            var txtNombre = $('#txtNombreModeloEdit').val();
            var clave = $.trim(txtClave);
            var modelo = $.trim(txtNombre);
            var data = {
                clave: clave,
                nombre: modelo
            };
            $.ajax({
                url: baseUrl + 'catalogModels',
                type: 'get',
                data: data,
                dataType: 'json',
                success: function (response) {
                    $('#detailsInFormationEdit').html('');
                    var models = response;
                    var detailConcat = '';
                    var lengthModelsIsZero = (models.length == 0);
                    if (lengthModelsIsZero) {
                        detailConcat += '<tr>' + '<td  colspan="9" class="text-center">No se encontraron modelos, intenta otra búsqueda</td>' + '</tr>';
                        $('#detailsInFormationEdit').html(detailConcat);
                    } else {
                        models.forEach(function (model) {
                            detailConcat += '<tr id="row' + model.IdModelo + '" class="detailModelEdit" data-id ="' + model.IdModelo + '">' +
                                '<td>' + model.Clave + '</td>' +
                                '<td>' + model.Nombre + '</td>' +
                                '<td>' + (model.Descripcion) + '</td>' +
                                '<td>' + (model.Precio1) + '</td>' +
                                '<td>' + ((model.line != null) ? model.Nombre : 'N/A') + '</td>' +
                                '</tr>';

                        });
                        $('#detailsInFormationEdit').html(detailConcat);
                        $('.detailModelEdit').on('click', function () {
                            var id = $(this).data('id');
                            quotationsEdit.getDetailModelForQuotation(id);

                        });

                    }
                },
                error: function (response) {

                }
            })
        },
        errorHandleMessage: function (message) {
            swal({
                title: "Lo sentimos",
                text: message,
                type: "warning"
            }, function () {
                //TODO
                //set focus on correct input
            });

        },
        addSpecialModelToQuotation() {

            // console.log(quotationsEdit.currentAddProduct);

            var responsename = "";
            var file = document.forms['saveImageAdd']['imgPathAdd'].files[0];
            var form_data = new FormData();
            form_data.append("file", file);
            // var id =  $(this).data("id");
            var url = "quotations/saveImage";
            var imgPathAjax = "";

            $.ajax({
                url: url,
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                async: false,
                type: 'post',
                success: function (data) {
                    // console.log(data);
                    // responsename = data;

                    if (data != '') {
                        imgPathAjax = data;
                    }
                }
            });

            var discount = $('#AdddescuentoProductoEspecialEdit').val();
            var area = $('#AddcboAreaEspecialEdit').val();
            var description = $('#AdddescripcionProductoEspecialEdit').val();
            var quantity = $('#AddcantidadProductoEspecialEdit').val();
            var side = $('#AddladoProductoEspecialEdit').val();
            var metering = $('#AddmedidaProductoEspecialEdit').val();
            var price = $('#AddprecioProductoEspecialEdit').val();
            var colorBase = $('#AddcolorBaseEspecialEdit').find('option:selected').text();
            var colorBaseId = $('#AddcolorBaseEspecialEdit').find('option:selected').val();
            var colorCombination = $('#AddcolorCombinacionEspecialEdit').find('option:selected').text();
            var colorCombinationId = $('#AddcolorCombinacionEspecialEdit').find('option:selected').val();

            if (price == 0 || $.trim(price) == '') {
                quotationsEdit.errorHandleMessage("Agrega un precio al producto especial.");
                return false;
            }
            if (quantity == 0 || $.trim(quantity) == '') {
                quotationsEdit.errorHandleMessage("Agrega una cantidad al producto especial.");
                return false;
            }
            if ($.trim(metering) == '') {
                quotationsEdit.errorHandleMessage("Agrega una medida al producto especial.");
                return false;
            }
            if ($.trim(description) == '') {
                quotationsEdit.errorHandleMessage("Agrega una descripción al producto especial.");
                return false;
            }
            if ($.trim(discount) == '') {
                quotationsEdit.errorHandleMessage("El campo de descuento no puede estar vacio.");
                return false;
            }
            discount = parseInt(discount);
            if (discount > quotationsEdit.currentClient.Descuento) {
                quotationsEdit.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este cliente que es de: " + quotationsEdit.currentClient.Descuento + "%");
                return false;
            }
            if ($.trim(area) == '') {
                quotationsEdit.errorHandleMessage("Agrega un area al producto");
                return false;
            }
            var totalAmount = (price * quantity);
            var substraction = totalAmount * (discount / 100);
            var totalWithDiscount = totalAmount - substraction;

            var objModelQuotation = {
                'discount': discount,
                'area': area,
                'description': description,
                'quantity': quantity,
                'side': side,
                'metering': metering,
                'subtotal': totalAmount,
                'model': quotationsEdit.currentAddProduct,
                'total': totalWithDiscount,
                'price': price,
                'colorBase': colorBase,
                'colorBaseId': colorBaseId,
                'colorComb': colorCombination,
                'colorCombId': colorCombinationId,
                'prodEsp': 'yes',
                'image': imgPathAjax,
                'colorCategories': []
            };

            $('#AdddescuentoProductoEspecialEdit').val('');
            $('#AddcboAreaEspecialEdit').val('');
            $('#AdddescripcionProductoEspecialEdit').val('');
            $('#AddcantidadProductoEspecialEdit').val('');
            $('#AddladoProductoEspecialEdit').val('');
            $('#AddmedidaProductoEspecialEdit').val('');
            $('#AddprecioProductoEspecialEdit').val('');
            $('#AddcolorBaseEspecialEdit').find('option:selected').val(0);
            $('#AddcolorCombinacionEspecialEdit').find('option:selected').val(0);
            $('#imgPathAdd').val(null);

            console.log(quotationsEdit.productsInQuotation);
            quotationsEdit.productsInQuotation.push(objModelQuotation);
            $("#modalAddSpecialProductEdit").modal('hide');
            quotationsEdit.drawProductsInQuotation();


        },
        EditSpecialModelToQuotation() {

            var responsename = "";
            var file = document.forms['saveImageEdit']['imgPathEdit'].files[0];
            var form_data = new FormData();
            form_data.append("file", file);
            // var id =  $(this).data("id");
            var url = "quotations/saveImage";
            var imgPathAjax = "";

            // console.log(quotationsEdit.currentAddProduct);
            // break;

            $.ajax({
                url: url,
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                async: false,
                type: 'post',
                success: function (data) {
                    // console.log(data);
                    // responsename = data;

                    if (data != '') {
                        imgPathAjax = data;
                    }
                }
            });

            var idModel = $('#idModelEdit').val();
            var data = {
                'idModel': idModel
            };

            if (idModel != 1452) {
                $.ajax({
                    url: "quotations/getModel",
                    dataType: 'json',
                    data: data,
                    type: 'post',
                    async: false,
                    success: function (data) {
                        // console.log(data);
                        model = data.data;
                    }
                });
            }

            var idProduct = $('#idProductEdit').val();

            var discount = $('#descuentoProductoEspecialEdit').val();
            var area = $('#cboAreaEspecialEdit').val();
            var description = $('#descripcionProductoEspecialEdit').val();
            var quantity = $('#cantidadProductoEspecialEdit').val();
            var side = $('#ladoProductoEspecialEdit').val();
            var metering = $('#medidaProductoEspecialEdit').val();
            var price = $('#precioProductoEspecialEdit').val();
            var colorBase = $('#colorBaseEspecialEdit').find('option:selected').text();
            var colorBaseId = $('#colorBaseEspecialEdit').find('option:selected').val();
            var colorCombination = $('#colorCombinacionEspecialEdit').find('option:selected').text();
            var colorCombinationId = $('#colorCombinacionEspecialEdit').find('option:selected').val();

            if (price == 0 || $.trim(price) == '') {
                quotationsEdit.errorHandleMessage("Agrega un precio al producto especial.");
                return false;
            }
            if (quantity == 0 || $.trim(quantity) == '') {
                quotationsEdit.errorHandleMessage("Agrega una cantidad al producto especial.");
                return false;
            }
            if ($.trim(metering) == '') {
                quotationsEdit.errorHandleMessage("Agrega una medida al producto especial.");
                return false;
            }
            if ($.trim(description) == '') {
                quotationsEdit.errorHandleMessage("Agrega una descripción al producto especial.");
                return false;
            }
            if ($.trim(discount) == '') {
                quotationsEdit.errorHandleMessage("El campo de descuento no puede estar vacio.");
                return false;
            }
            discount = parseInt(discount);
            if (discount > quotationsEdit.currentClient.Descuento) {
                quotationsEdit.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este cliente que es de: " + quotationsEdit.currentClient.Descuento + "%");
                return false;
            }
            if ($.trim(area) == '') {
                quotationsEdit.errorHandleMessage("Agrega un area al producto");
                return false;
            }
            var totalAmount = (price * quantity);
            var substraction = totalAmount * (discount / 100);
            var totalWithDiscount = totalAmount - substraction;

            if (idModel != 1452) {
                var objModelQuotation = {
                    'idProduct': idProduct,
                    'discount': discount,
                    'area': area,
                    'description': description,
                    'quantity': quantity,
                    'side': side,
                    'metering': metering,
                    'subtotal': totalAmount,
                    'model': model,
                    'total': totalWithDiscount,
                    'price': price,
                    'colorBase': colorBase,
                    'colorBaseId': colorBaseId,
                    'colorComb': colorCombination,
                    'colorCombId': colorCombinationId,
                    'prodEsp': 'yes',
                    'image': imgPathAjax,
                    'colorCategories': []
                };
            } else {
                var objModelQuotation = {
                    'idProduct': idProduct,
                    'discount': discount,
                    'area': area,
                    'description': description,
                    'quantity': quantity,
                    'side': side,
                    'metering': metering,
                    'subtotal': totalAmount,
                    'model': quotationsEdit.currentAddProduct,
                    'total': totalWithDiscount,
                    'price': price,
                    'colorBase': colorBase,
                    'colorBaseId': colorBaseId,
                    'colorComb': colorCombination,
                    'colorCombId': colorCombinationId,
                    'prodEsp': 'yes',
                    'image': imgPathAjax,
                    'colorCategories': []
                };
            }



            $('#descuentoProductoEspecialEdit').val('');
            $('#cboAreaEspecialEdit').val('');
            $('#descripcionProductoEspecialEdit').val('');
            $('#cantidadProductoEspecialEdit').val('');
            $('#ladoProductoEspecialEdit').val('');
            $('#medidaProductoEspecialEdit').val('');
            $('#precioProductoEspecialEdit').val('');
            $('#colorBaseEspecialEdit').find('option:selected').val(0);
            $('#colorCombinacionEspecialEdit').find('option:selected').val(0);
            $('#imgPathEdit').val(null);

            // console.log(objModelQuotation);
            quotationsEdit.productsInQuotation.push(objModelQuotation);
            $('#rowProductEditEarly' + idProduct).remove();
            $("#modalEditSpecialProductEdit").modal('hide');
            quotationsEdit.drawProductsInQuotation();


        },
        EditProductModelToQuotation() {

            var responsename = "";
            // var file = document.forms['saveImageEdit']['imgPathEdit'].files[0];
            // var form_data = new FormData();
            // form_data.append("file", file);
            // var id =  $(this).data("id");
            var url = "quotations/saveImage";
            var imgPathAjax = "";

            // console.log(quotationsEdit.currentAddProduct);
            // break;

            // $.ajax({
            //     url: url,
            //     dataType: 'script',
            //     cache: false,
            //     contentType: false,
            //     processData: false,
            //     data: form_data,
            //     async: false,
            //     type: 'post',
            //     success: function (data) {
            //         // console.log(data);
            //         // responsename = data;

            //         if (data != '') {
            //             imgPathAjax = data;
            //         }
            //     }
            // });

            var idModel = $('#idModelEdit').val();
            var data = {
                'idModel': idModel
            };

            if (idModel != 1452) {
                $.ajax({
                    url: "quotations/getModel",
                    dataType: 'json',
                    data: data,
                    type: 'post',
                    async: false,
                    success: function (data) {
                        // console.log(data);
                        model = data.data;
                    }
                });
            }

            var idProduct = $('#idProductEdit').val();

            var discount = $("#descuentoEditProductProductoEdit").val();
            var area = $("#cboEditProductAreaEdit").val();
            var description = $("#descripcionEditProductProductoEdit").val();
            var quantity = $("#cantidadEditProductProductoEdit").val();
            var side = $("#ladoEditProductProductoEdit").val();
            var metering = $("#medidaEditProductProductoEdit").val();
            var price = $("#precioEditProductProductoEdit").val();
            // var colorBase = $('#colorBaseEspecialEdit').find('option:selected').text();
            // var colorBaseId = $('#colorBaseEspecialEdit').find('option:selected').val();
            // var colorCombination = $('#colorCombinacionEspecialEdit').find('option:selected').text();
            // var colorCombinationId = $('#colorCombinacionEspecialEdit').find('option:selected').val();

            if (price == 0 || $.trim(price) == '') {
                quotationsEdit.errorHandleMessage("Agrega un precio al producto especial.");
                return false;
            }
            if (quantity == 0 || $.trim(quantity) == '') {
                quotationsEdit.errorHandleMessage("Agrega una cantidad al producto especial.");
                return false;
            }
            if ($.trim(metering) == '') {
                quotationsEdit.errorHandleMessage("Agrega una medida al producto especial.");
                return false;
            }
            if ($.trim(description) == '') {
                quotationsEdit.errorHandleMessage("Agrega una descripción al producto especial.");
                return false;
            }
            if ($.trim(discount) == '') {
                quotationsEdit.errorHandleMessage("El campo de descuento no puede estar vacio.");
                return false;
            }
            discount = parseInt(discount);
            if (discount > quotationsEdit.currentClient.Descuento) {
                quotationsEdit.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este cliente que es de: " + quotationsEdit.currentClient.Descuento + "%");
                return false;
            }
            if ($.trim(area) == '') {
                quotationsEdit.errorHandleMessage("Agrega un area al producto");
                return false;
            }
            var totalAmount = (price * quantity);
            var substraction = totalAmount * (discount / 100);
            var totalWithDiscount = totalAmount - substraction;

            if (idModel != 1452) {
                var objModelQuotation = {
                    'idProduct': idProduct,
                    'discount': discount,
                    'area': area,
                    'description': description,
                    'quantity': quantity,
                    'side': side,
                    'metering': metering,
                    'subtotal': totalAmount,
                    'model': model,
                    'total': totalWithDiscount,
                    'price': price,
                    'prodEsp': 'yes',
                    // 'image': imgPathAjax,
                    'colorCategories': []
                };
            } else {
                var objModelQuotation = {
                    'idProduct': idProduct,
                    'discount': discount,
                    'area': area,
                    'description': description,
                    'quantity': quantity,
                    'side': side,
                    'metering': metering,
                    'subtotal': totalAmount,
                    'model': quotationsEdit.currentAddProduct,
                    'total': totalWithDiscount,
                    'price': price,
                    'colorBase': colorBase,
                    'colorBaseId': colorBaseId,
                    'colorComb': colorCombination,
                    'colorCombId': colorCombinationId,
                    'prodEsp': 'yes',
                    'image': imgPathAjax,
                    'colorCategories': []
                };
            }



            $("#descuentoEditProductProductoEdit").val("");
            $("#cboEditProductAreaEdit").val("");
            $("#descripcionEditProductProductoEdit").val("");
            $("#cantidadEditProductProductoEdit").val("");
            $("#ladoEditProductProductoEdit").val("");
            $("#medidaEditProductProductoEdit").val("");
            $("#precioEditProductProductoEdit").val("");
            // $('#colorBaseEspecialEdit').find('option:selected').val(0);
            // $('#colorCombinacionEspecialEdit').find('option:selected').val(0);
            // $('#imgPathEdit').val(null);

            // console.log(objModelQuotation);
            quotationsEdit.productsInQuotation.push(objModelQuotation);
            $('#rowProductEditEarly' + idProduct).remove();
            $("#modalEditProductQuotationEdit").modal("hide");
            quotationsEdit.drawProductsInQuotation();


        },
        addModelToQuotation() {

            var discount = $('#descuentoProductoEdit').val();
            var area = $('#cboAreaEdit').val();
            var description = $('#descripcionProductoEdit').val();
            var quantity = $('#cantidadProductoEdit').val();
            var side = $('#ladoProductoEdit').val();
            var metering = $('#medidaProductoEdit').val();
            var price = $('#precioProductoEdit').val();
            var additionalCost = $('#costoAdicionalProductoEdit').is(":checked");
            if (additionalCost) {
                var percent = quotationsEdit.currentAddProduct.line.Incremento / 100;
                var additional = price * percent;
                price = price + additional;
            }
            if ($.trim(discount) == '') {
                quotationsEdit.errorHandleMessage("El campo de descuento no puede estar vacio.");
                return false;
            }
            discount = parseInt(discount);
            if (discount > quotationsEdit.currentClient.Descuento) {
                quotationsEdit.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este cliente que es de: " + quotationsEdit.currentClient.Descuento + "%");
                return false;
            }
            if (discount > quotationsEdit.currentAddProduct.line.Descuento) {
                quotationsEdit.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este producto que es de: " + quotationsEdit.currentAddProduct.line.Descuento + "%");
                return false;
            }
            if ($.trim(area) == '') {
                quotationsEdit.errorHandleMessage("Agrega un area al producto");
                return false;
            }

            var totalAmount = (price * quantity);
            var substraction = totalAmount * (discount / 100);
            var totalWithDiscount = totalAmount - substraction;

            var objModelQuotation = {
                'discount': discount,
                'area': area,
                'description': description,
                'quantity': quantity,
                'side': side,
                'metering': metering,
                'subtotal': totalAmount,
                'model': quotationsEdit.currentAddProduct,
                'total': totalWithDiscount,
                'price': price,
                'prodEsp': 'no',
                'colorCategories': []
            };


            var selects = $('#containerColorCategoriesEdit > .form-group > select');

            for (var x = 0; x < selects.length; x++) {
                var categoryColor = $(selects[x]).data('idcategoria');
                var nameCategoryColor = $(selects[x]).data('namecategoria');
                var colorId = $(selects[x]).val();
                var colorName = $(selects[x]).find('option:selected').text();
                var categoryAndColor = {
                    'categoryColor': categoryColor,
                    'nameCategoryColor': nameCategoryColor,
                    'colorId': parseInt(colorId),
                    'colorName': colorName
                };
                objModelQuotation.colorCategories.push(categoryAndColor);
            }
            quotationsEdit.productsInQuotation.push(objModelQuotation);
            $("#modalAddProductQuotationEdit").modal('hide');
            quotationsEdit.drawProductsInQuotation();
        },
        drawProductsInQuotation: function () {
            $('#detailsInFormationQuotationEdit').html('');
            var detailConcat = '';
            var lengthModelsIsZero = (quotationsEdit.productsInQuotation.length == 0);
            if (lengthModelsIsZero) {
                detailConcat += '<tr>' + '<td  colspan="9" class="text-center">No se han agregado modelos a esta cotización</td>' + '</tr>';
                $('#detailsInFormationQuotationEdit').html(detailConcat);
            } else {
                quotationsEdit.productsInQuotation.forEach(function (productInQuotation) {
                    var totalAmount = (productInQuotation.price * productInQuotation.quantity);
                    var substraction = totalAmount * (productInQuotation.discount / 100);
                    var totalWithDiscount = totalAmount - substraction;
                    totalWithDiscount = '$' + totalAmount.toLocaleString();
                    detailConcat += '<tr id="rowProductEdit' + productInQuotation.model.IdModelo + '" class="detailModelQuotationEdit" data-id ="' + productInQuotation.model.IdModelo + '">' +
                        '<td>' + productInQuotation.model.Clave + '</td>' +
                        '<td>' + productInQuotation.description + '</td>' +
                        '<td>' + (productInQuotation.metering) + '</td>' +
                        '<td>' + (productInQuotation.price).toLocaleString() + '</td>' +
                        '<td>' + (productInQuotation.quantity) + '</td>' +
                        '<td>' + (productInQuotation.discount) + '</td>' +
                        '<td>' + (totalWithDiscount) + '</td>' +
                        '<td>' + (productInQuotation.area) + '</td>' +
                        '<td class="text-center"> <button class="btn btn-dark buttonDeleteProductEdit" data-id ="' + productInQuotation.model.IdModelo + '" ><i class="fa fa-trash"></i></button></td>' +
                        +'</tr>';
                });
                $('#detailsInFormationQuotationEdit').html(detailConcat);
                $('.buttonDeleteProductEdit').on('click', function (e) {
                    e.preventDefault();
                    var idProduct = $(this).data('id');
                    swal({
                        title: "¿Seguro?",
                        text: "Deseas eliminar este producto?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Si, estoy seguro!',
                        cancelButtonText: "No, cancelar"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            var myIndex = null;
                            quotationsEdit.productsInQuotation.forEach(function (currentValue, index) {
                                if (quotationsEdit.productsInQuotation[index].model.IdModelo == idProduct) {
                                    quotationsEdit.productsInQuotation.splice(index, 1);
                                    $('#rowProductEdit' + idProduct).remove();
                                }
                            });
                        }
                    })
                });
            }
        },
        getDetailModelForQuotation: function (id) {
            $.ajax({
                url: baseUrl + 'detailModelForQuotation/' + id,
                type: 'get',
                data: {},
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $percentIncrement = 0;
                    if (quotationsEdit.currentClient.Precio === 2) {
                        $percentIncrement = response.Precio1 * 0.10;
                    } else if (quotationsEdit.currentClient.Precio === 3) {
                        $percentIncrement = response.Precio1 * 0.15;
                    } else if (quotationsEdit.currentClient.Precio === 4) {
                        $percentIncrement = response.Precio1 * 0.20;
                    }
                    response.Precio1 = response.Precio1 + $percentIncrement;
                    quotationsEdit.currentAddProduct = response;
                    $('#descripcionProductoEdit').val(response.Descripcion);
                    $('#medidaProductoEdit').val(response.Nombre);
                    $('#precioProductoEdit').val(response.Precio1);
                    $('#porcentajeAdicionalEdit').text(response.line.Incremento + '%');
                    $('#modalAddProductQuotationEdit').modal({
                        show: true
                    });
                    var formations = response.formations;

                    formations.forEach(function (formation) {
                        if (!$('#form-group-colorCategoryEdit' + formation.categoria_color.IdCategoriaColor).length) {
                            var colorContainer = '<div class="form-group" id="form-group-colorCategoryEdit' + formation.categoria_color.IdCategoriaColor + '">\n' +
                                '                            <label for="categoriaColor">' + formation.categoria_color.Nombre + '</label>\n' +
                                '                            <select data-idcategoria="' + formation.categoria_color.IdCategoriaColor +
                                '" data-namecategoria="' + formation.categoria_color.Nombre + '" name="categoriaColorSelectEdit' + formation.categoria_color.IdCategoriaColor +
                                '" id="categoriaColorSelectEdit' + formation.categoria_color.IdCategoriaColor + '" class="form-control"></select>\n' +
                                '                        </div>';
                            $('#containerColorCategoriesEdit').append(colorContainer);

                            formation.categoria_color.colors.forEach(function (color) {
                                $('#categoriaColorSelectEdit' + formation.categoria_color.IdCategoriaColor).append(new Option(color.Nombre, color.IdColor, false, false));
                            });
                        }
                    });
                }
            })
        },
        getCatalogs: function () {
            $.ajax({
                url: baseUrl + 'catalogsNewQuotation',
                type: 'get',
                data: {},
                dataType: 'json',
                success: function (response) {
                    var clients = response.clients;
                    quotationsEdit.clients = clients;
                    var colors = response.colors;
                    //assign cliente from db
                    clients.forEach(function (client) {
                        //$('#IdClienteEdit').append(new Option(client.Nombre, client.IdCliente, false, false));
                        $('#IdClienteEdit').append(new Option(client.Nombre, client.IdCliente, false, false));
                    });
                    colors.forEach(function (color) {
                        $('#colorBaseEspecialEdit').append(new Option(color.Nombre, color.IdColor, false, false));
                        $('#AddcolorBaseEspecialEdit').append(new Option(color.Nombre, color.IdColor, false, false));
                        $('#colorCombinacionEspecialEdit').append(new Option(color.Nombre, color.IdColor, false, false));
                        $('#AddcolorCombinacionEspecialEdit').append(new Option(color.Nombre, color.IdColor, false, false));
                    });
                },
                error: function (response) {

                }
            })
        },
        setValidator: function () {
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        updateQuotation: function () {
            $("#editClientForm").validate({
                rules: {
                    ClaveCliente: {
                        required: true
                    },
                    Nombre: {
                        required: true
                    },
                    Telefono: {
                        required: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    RFC: {
                        required: true
                    },
                    Condiciones: {
                        required: true
                    },
                    Contacto: {
                        required: true
                    },
                    DomicilioEntrega: {
                        required: true
                    }

                },
                submitHandler: function () {
                    var data = $('#editClientForm').serialize();
                    $.ajax({
                        url: 'clients/' + clients.id,
                        type: 'put',
                        data: data,
                        dataType: 'json',
                        beforeSend: function () {
                            swal({
                                title: 'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html: true
                            });
                        },
                        success: function (response) {
                            swal({
                                title: "Exito",
                                text: response.msg,
                                type: "success"
                            }, function () {
                                $("#modalNewClient").modal('hide');
                            });
                        },
                        error: function (response) {

                        }
                    })
                }
            });
        },
        saveQuotation: function () {
            // if(quotationsEdit.productsInQuotation.length == 0){
            //     quotationsEdit.errorHandleMessage("No has agregado productos extra a la cotización");
            //     return false;
            // }
            quotationsEdit.current = {
                'idQuotation': $('#idQuotation').val(),
                'client': quotationsEdit.currentClient,
                'products': quotationsEdit.productsInQuotation,
                'model': quotationsEdit.currentAddProduct,
                'observations': $('#observationsEdit').val() || 'Ninguna'
            };
            data = (quotationsEdit.current);
            $.ajax({
                url: 'quotations/' + quotationsEdit.currentQuotation.IdCotizacion,
                type: 'put',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    swal({
                        title: 'Procesando',
                        text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                        type: 'warning',
                        confirmButtonColor: "#00444f",
                        showCancelButton: false,
                        showConfirmButton: false,
                        html: true
                    });
                },
                success: function (response) {
                    swal({
                        title: "Exito",
                        text: response.msg,
                        type: "success"
                    }, function () {
                        $('#modalObservationsEdit').modal('hide');
                        $("#modalQuotationEdit").modal('hide');
                        location.reload();
                    });
                },
                error: function (response) {

                }
            });
        }
    };
    $(document).ready(function () {
        quotationsEdit.init()
    });
})();