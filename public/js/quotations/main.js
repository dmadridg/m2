(function () {
    var quotations = {
        id: null,
        clients: [],
        currentClient: null,
        currentAddProduct: null,
        productsInQuotation: [],
        current: null,
        idClient: null,
        tags: [],
        init: function () {

            $('#buttonNewQuotation').on('click', function () {
                $('#modalNewQuotation').modal();
            });
            $('#btnAddSpecialProduct').on('click', function () {
                $('#modalAddSpecialProduct').modal();
            });
            $('#buttonSaveProductSpecial').on('click', function () {
                quotations.addSpecialModelToQuotation()
            });
            $('#buttonAddProductToQuotation').on('click', function () {
                quotations.addModelToQuotation()
            });
            $('#buttonShowObservations').on('click', function () {
                $('#modalObservations').modal();
            });
            $('#buttonSaveQuotation').on('click', function () {
                quotations.saveQuotation();
            });
            $('#modalNewQuotation').on("hidden.bs.modal", function () {
                quotations.currentClient = quotations.clients[0];
                $('#detailsInFormation').html('');
                $('#detailsInFormationQuotation').html('');
                quotations.currentAddProduct = null;
            });
            $('#modalObservations').on("hidden.bs.modal", function () {
                $('#observations').val('');
            });
            $("#modalAddProductQuotation").on("hidden.bs.modal", function () {
                $('#containerColorCategories').html('');
                $("#addProductQuotationForm")[0].reset();
            });
            $("#modalAddSpecialProduct").on("hidden.bs.modal", function () {
                // TODO
            });
            $("#modalAddSpecialProduct").on("show.bs.modal", function () {
                quotations.currentAddProduct = {
                    IdModelo: 1452,
                    Nombre: 'ESP001',
                    Clave: 'ESP001',
                    Descripcion: 'Articulo Especial'
                }
            });
            $('#IdCliente').select2({
                theme: 'bootstrap'
            });
            this.getCatalogs();
            this.setValidator();
            this.updateQuotation();
            $('#btnSearchModels').on('click', function () {
                quotations.getModels();
            });
            $('#IdCliente').on('select2:select', function (e) {
                var IdCliente = $(this).val();
                quotations.clients.forEach(function (client) {
                    if (client.IdCliente == IdCliente) {
                        quotations.idClient = client.IdCliente;
                        quotations.currentClient = client;
                        $('#ClaveCliente').val(client.ClaveCliente);
                        $('#Contacto').val(client.Contacto);
                        $('#Direccion').val(client.DomicilioFiscal);
                        $('#Ciudad').val(client.city.NombreCiudad);
                        $('#RFC').val(client.RFC);
                        $('#Telefono').val(client.Telefono);
                    }
                })
            });
            $('.editQuotation').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url: 'quotations/' + id,
                    type: 'get',
                    data: {},
                    dataType: 'json',
                    success: function (response) {

                        var client = response;
                        clients.id = client.IdCliente;
                        for (var property in client) {
                            $('#' + property + 'Edit').val(client[property])
                        }
                        if (client.IdEstado != "" || client.IdEstado != null) {
                            clients.getCity(client.IdEstado)
                        }

                        $("#modalEditClient").modal();
                    },
                    error: function () {

                    }
                })
            })
        },
        getModels: function () {
            var txtClave = $('#txtClaveModelo').val();
            var txtNombre = $('#txtNombreModelo').val();
            var clave = $.trim(txtClave);
            var modelo = $.trim(txtNombre);
            var data = {
                clave: clave,
                nombre: modelo
            };
            $.ajax({
                url: baseUrl + 'catalogModels',
                type: 'get',
                data: data,
                dataType: 'json',
                success: function (response) {
                    $('#detailsInFormation').html('');
                    var models = response;
                    var detailConcat = '';
                    var lengthModelsIsZero = (models.length == 0);
                    if (lengthModelsIsZero) {
                        detailConcat += '<tr>' + '<td  colspan="9" class="text-center">No se encontraron modelos, intenta otra búsqueda</td>' + '</tr>';
                        $('#detailsInFormation').html(detailConcat);
                    } else {
                        models.forEach(function (model) {
                            detailConcat += '<tr id="row' + model.IdModelo + '" class="detailModel" data-id ="' + model.IdModelo + '">' +
                                '<td>' + model.Clave + '</td>' +
                                '<td>' + model.Nombre + '</td>' +
                                '<td>' + (model.Descripcion) + '</td>' +
                                '<td>' + (model.Precio1) + '</td>' +
                                '<td>' + ((model.line != null) ? model.Nombre : 'N/A') + '</td>' +
                                '</tr>';

                        });
                        $('#detailsInFormation').html(detailConcat);
                        $('.detailModel').on('click', function () {
                            var id = $(this).data('id');
                            quotations.getDetailModelForQuotation(id);

                        });

                    }
                },
                error: function (response) {

                }
            })
        },
        errorHandleMessage: function (message) {
            swal({
                title: "Lo sentimos",
                text: message,
                type: "warning"
            }, function () {
                //TODO
                //set focus on correct input
            });

        },
        addSpecialModelToQuotation() {

            var responsename = "";
            var file = document.forms['saveImage']['imgPathNewAdd'].files[0];
            var form_data = new FormData();
            form_data.append("file", file);
            // var id =  $(this).data("id");
            var url = "quotations/saveImage";
            // console.log(file);
            var imgPathAjax = "";

            $.ajax({
                url: url,
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                async: false,
                data: form_data,
                type: 'post',
                success: function (data) {
                    if (data != '') {
                        imgPathAjax = data;
                    }
                }
            });

            var discount = $('#descuentoProductoEspecial').val();
            var area = $('#cboAreaEspecial').val();
            var description = $('#descripcionProductoEspecial').val();
            var quantity = $('#cantidadProductoEspecial').val();
            var side = $('#ladoProductoEspecial').val();
            var metering = $('#medidaProductoEspecial').val();
            var price = $('#precioProductoEspecial').val();
            var colorBase = $('#colorBaseEspecial').find('option:selected').text();
            var colorBaseId = $('#colorBaseEspecial').find('option:selected').val();
            var colorCombination = $('#colorCombinacionEspecial').find('option:selected').text();
            var colorCombinationId = $('#colorCombinacionEspecial').find('option:selected').val();

            if (price == 0 || $.trim(price) == '') {
                quotations.errorHandleMessage("Agrega un precio al producto especial.");
                return false;
            }
            if (quantity == 0 || $.trim(quantity) == '') {
                quotations.errorHandleMessage("Agrega una cantidad al producto especial.");
                return false;
            }
            if ($.trim(metering) == '') {
                quotations.errorHandleMessage("Agrega una medida al producto especial.");
                return false;
            }
            if ($.trim(description) == '') {
                quotations.errorHandleMessage("Agrega una descripción al producto especial.");
                return false;
            }
            if ($.trim(discount) == '') {
                quotations.errorHandleMessage("El campo de descuento no puede estar vacio.");
                return false;
            }
            discount = parseInt(discount);
            if (discount > quotations.currentClient.Descuento) {
                quotations.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este cliente que es de: " + quotations.currentClient.Descuento + "%");
                return false;
            }
            if ($.trim(area) == '') {
                quotations.errorHandleMessage("Agrega un area al producto");
                return false;
            }
            var totalAmount = (price * quantity);
            var substraction = totalAmount * (discount / 100);
            var totalWithDiscount = totalAmount - substraction;

            var objModelQuotation = {
                'discount': discount,
                'area': area,
                'description': description,
                'quantity': quantity,
                'side': side,
                'metering': metering,
                'subtotal': totalAmount,
                'model': quotations.currentAddProduct,
                'total': totalWithDiscount,
                'price': price,
                'colorBase': colorBase,
                'colorBaseId': colorBaseId,
                'colorComb': colorCombination,
                'colorCombId': colorCombinationId,
                'prodEsp': 'yes',
                'image': imgPathAjax,
                'colorCategories': []
            };

            $('#descuentoProductoEspecial').val('');
            $('#cboAreaEspecial').val('');
            $('#descripcionProductoEspecial').val('');
            $('#cantidadProductoEspecial').val('');
            $('#ladoProductoEspecial').val('');
            $('#medidaProductoEspecial').val('');
            $('#precioProductoEspecial').val('');
            $('#colorBaseEspecial').find('option:selected').val(0);
            $('#colorCombinacionEspecial').find('option:selected').val(0);
            $('#imgPathNewAdd').val(null);


            quotations.productsInQuotation.push(objModelQuotation);
            console.log(quotations.productsInQuotation);
            $("#modalAddSpecialProduct").modal('hide');
            quotations.drawProductsInQuotation();



        },
        addModelToQuotation() {

            var discount = $('#descuentoProducto').val();
            var area = $('#cboArea').val();
            var description = $('#descripcionProducto').val();
            var quantity = $('#cantidadProducto').val();
            var side = $('#ladoProducto').val();
            var metering = $('#medidaProducto').val();
            var price = $('#precioProducto').val();
            var additionalCost = $('#costoAdicionalProducto').is(":checked");
            if (additionalCost) {
                var percent = quotations.currentAddProduct.line.Incremento / 100;
                var additional = price * percent;
                price = price + additional;
            }
            if ($.trim(discount) == '') {
                quotations.errorHandleMessage("El campo de descuento no puede estar vacio.");
                return false;
            }
            discount = parseInt(discount);
            if (discount > quotations.currentClient.Descuento) {
                quotations.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este cliente que es de: " + quotations.currentClient.Descuento + "%");
                return false;
            }
            if (discount > quotations.currentAddProduct.line.Descuento) {
                quotations.errorHandleMessage("El descuento que intentas aplicar es mayor al descuento para este producto que es de: " + quotations.currentAddProduct.line.Descuento + "%");
                return false;
            }
            if ($.trim(area) == '') {
                quotations.errorHandleMessage("Agrega un area al producto");
                return false;
            }

            var totalAmount = (price * quantity);
            var substraction = totalAmount * (discount / 100);
            var totalWithDiscount = totalAmount - substraction;

            var objModelQuotation = {
                'discount': discount,
                'area': area,
                'description': description,
                'quantity': quantity,
                'side': side,
                'metering': metering,
                'subtotal': totalAmount,
                'model': quotations.currentAddProduct,
                'total': totalWithDiscount,
                'price': price,
                'prodEsp': 'no',
                'colorCategories': []
            };


            var selects = $('#containerColorCategories > .form-group > select');

            for (var x = 0; x < selects.length; x++) {
                var categoryColor = $(selects[x]).data('idcategoria');
                var nameCategoryColor = $(selects[x]).data('namecategoria');
                var colorId = $(selects[x]).val();
                var colorName = $(selects[x]).find('option:selected').text();
                var categoryAndColor = {
                    'categoryColor': categoryColor,
                    'nameCategoryColor': nameCategoryColor,
                    'colorId': parseInt(colorId),
                    'colorName': colorName
                };
                objModelQuotation.colorCategories.push(categoryAndColor);
            }
            quotations.productsInQuotation.push(objModelQuotation);
            $("#modalAddProductQuotation").modal('hide');
            quotations.drawProductsInQuotation();
        },
        drawProductsInQuotation: function () {
            $('#detailsInFormationQuotation').html('');
            var detailConcat = '';
            var lengthModelsIsZero = (quotations.productsInQuotation.length == 0);
            if (lengthModelsIsZero) {
                detailConcat += '<tr>' + '<td  colspan="9" class="text-center">No se han agregado modelos a esta cotización</td>' + '</tr>';
                $('#detailsInFormationQuotation').html(detailConcat);
            } else {
                quotations.productsInQuotation.forEach(function (productInQuotation) {
                    var totalAmount = (productInQuotation.price * productInQuotation.quantity);
                    var substraction = totalAmount * (productInQuotation.discount / 100);
                    var totalWithDiscount = totalAmount - substraction;
                    totalWithDiscount = '$' + totalAmount.toLocaleString();
                    detailConcat += '<tr id="rowProduct' + productInQuotation.model.IdModelo + '" class="detailModelQuotation" data-id ="' + productInQuotation.model.IdModelo + '">' +
                        '<td>' + productInQuotation.model.Clave + '</td>' +
                        '<td>' + productInQuotation.description + '</td>' +
                        '<td>' + (productInQuotation.metering) + '</td>' +
                        '<td>' + (productInQuotation.price).toLocaleString() + '</td>' +
                        '<td>' + (productInQuotation.quantity) + '</td>' +
                        '<td>' + (productInQuotation.discount) + '</td>' +
                        '<td>' + (totalWithDiscount) + '</td>' +
                        '<td>' + (productInQuotation.area) + '</td>' +
                        '<td class="text-center"> <button class="btn btn-dark buttonDeleteProduct" data-id ="' + productInQuotation.model.IdModelo + '" ><i class="fa fa-trash"></i></button></td>' +
                        +'</tr>';
                });
                $('#detailsInFormationQuotation').html(detailConcat);
                $('.buttonDeleteProduct').on('click', function (e) {
                    e.preventDefault();
                    var idProduct = $(this).data('id');
                    swal({
                        title: "¿Seguro?",
                        text: "Deseas eliminar este producto?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Si, estoy seguro!',
                        cancelButtonText: "No, cancelar"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            var myIndex = null;
                            quotations.productsInQuotation.forEach(function (currentValue, index) {
                                if (quotations.productsInQuotation[index].model.IdModelo == idProduct) {
                                    quotations.productsInQuotation.splice(index, 1);
                                    $('#rowProduct' + idProduct).remove();
                                }
                            });
                        }
                    })
                });
            }
        },
        getDetailModelForQuotation: function (id) {
            $.ajax({
                url: baseUrl + 'detailModelForQuotation/' + id,
                type: 'get',
                data: {},
                dataType: 'json',
                success: function (response) {
                    $percentIncrement = 0;
                    if (quotations.currentClient.Precio === 2) {
                        $percentIncrement = response.Precio1 * 0.10;
                    } else if (quotations.currentClient.Precio === 3) {
                        $percentIncrement = response.Precio1 * 0.15;
                    } else if (quotations.currentClient.Precio === 4) {
                        $percentIncrement = response.Precio1 * 0.20;
                    }
                    response.Precio1 = response.Precio1 + $percentIncrement;
                    quotations.currentAddProduct = response;
                    $('#descripcionProducto').val(response.Descripcion);
                    $('#medidaProducto').val(response.Nombre);
                    $('#precioProducto').val(response.Precio1);
                    $('#porcentajeAdicional').text(response.line.Incremento + '%');
                    $('#modalAddProductQuotation').modal({
                        show: true
                    });
                    var formations = response.formations;

                    formations.forEach(function (formation) {
                        if (!$('#form-group-colorCategory' + formation.categoria_color.IdCategoriaColor).length) {
                            var colorContainer = '<div class="form-group" id="form-group-colorCategory' + formation.categoria_color.IdCategoriaColor + '">\n' +
                                '                            <label for="categoriaColor">' + formation.categoria_color.Nombre + '</label>\n' +
                                '                            <select data-idcategoria="' + formation.categoria_color.IdCategoriaColor +
                                '" data-namecategoria="' + formation.categoria_color.Nombre + '" name="categoriaColorSelect' + formation.categoria_color.IdCategoriaColor +
                                '" id="categoriaColorSelect' + formation.categoria_color.IdCategoriaColor + '" class="form-control"></select>\n' +
                                '                        </div>';
                            $('#containerColorCategories').append(colorContainer);

                            formation.categoria_color.colors.forEach(function (color) {
                                $('#categoriaColorSelect' + formation.categoria_color.IdCategoriaColor).append(new Option(color.Nombre, color.IdColor, false, false));
                            });
                        }
                    });
                }
            })
        },
        getCatalogs: function () {
            $.ajax({
                url: baseUrl + 'catalogsNewQuotation',
                type: 'get',
                data: {},
                dataType: 'json',
                success: function (response) {
                    var clients = response.clients;
                    quotations.clients = clients;
                    var colors = response.colors;
                    quotations.idClient = clients[0].idClient;
                    quotations.currentClient = clients[0];
                    clients.forEach(function (client) {
                        $('#IdCliente').append(new Option(client.Nombre, client.IdCliente, false, false));
                        $('#IdClienteEdit').append(new Option(client.Nombre, client.IdCliente, false, false));
                    });
                    colors.forEach(function (color) {
                        $('#colorBaseEspecial').append(new Option(color.Nombre, color.IdColor, false, false));
                        $('#colorCombinacionEspecial').append(new Option(color.Nombre, color.IdColor, false, false));
                    });
                },
                error: function (response) {

                }
            })
        },
        setValidator: function () {
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        updateQuotation: function () {
            $("#editClientForm").validate({
                rules: {
                    ClaveCliente: {
                        required: true
                    },
                    Nombre: {
                        required: true
                    },
                    Telefono: {
                        required: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    RFC: {
                        required: true
                    },
                    Condiciones: {
                        required: true
                    },
                    Contacto: {
                        required: true
                    },
                    DomicilioEntrega: {
                        required: true
                    }

                },
                submitHandler: function () {
                    var data = $('#editClientForm').serialize();
                    $.ajax({
                        url: 'clients/' + clients.id,
                        type: 'put',
                        data: data,
                        dataType: 'json',
                        beforeSend: function () {
                            swal({
                                title: 'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html: true
                            });
                        },
                        success: function (response) {
                            swal({
                                title: "Exito",
                                text: response.msg,
                                type: "success"
                            }, function () {
                                $("#modalNewClient").modal('hide');
                            });
                        },
                        error: function (response) {

                        }
                    })
                }
            });
        },
        saveQuotation: function () {
            // if(quotations.productsInQuotation.length == 0){
            //     quotations.errorHandleMessage("No has agregado productos a la cotización");
            //     return false;
            // }
            quotations.current = {
                'client': quotations.currentClient,
                'products': quotations.productsInQuotation,
                'model': quotations.currentAddProduct,
                'observations': $('#observations').val()
            };
            data = (quotations.current);
            $.ajax({
                url: 'quotations',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    swal({
                        title: 'Procesando',
                        text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                        type: 'warning',
                        confirmButtonColor: "#00444f",
                        showCancelButton: false,
                        showConfirmButton: false,
                        html: true
                    });
                },
                success: function (response) {
                    swal({
                        title: "Exito",
                        text: response.msg,
                        type: "success"
                    }, function () {
                        $('#modalObservations').modal('hide');
                        $("#modalNewQuotation").modal('hide');
                        location.reload();
                    });
                },
                error: function (response) {

                }
            });
        }
    };
    $(document).ready(function () {
        quotations.init()
    })
})();