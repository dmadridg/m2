(function(){
    var clients = {
        id:null,
        init:function () {

            $('#buttonNewClient').on('click',function(){
                $('#modalNewClient').modal()
            });
            $('#IdEstado,#IdEstadoEdit').on('change',function(){
                var id = $(this).val()
                clients.getCity(id);
            })
            $('#IdEstado,#IdCiudad,#IdAgente,#IdTransportista').select2({
                theme:'bootstrap'
            });
            this.getCatalogs();
            this.setValidator();
            this.saveClient();
            this.updateClient();

            $('.editClient').on('click',function(e){
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url:'clients/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var client = response
                        clients.id = client.IdCliente;
                        for (var property in client){
                            $('#'+property+'Edit').val(client[property])
                        }
                        if(client.IdEstado != "" || client.IdEstado != null){
                            clients.getCity(client.IdEstado,client.IdCiudad)
                        }

                        $("#modalEditClient").modal();
                    },error:function(){

                    }
                })
            })
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        getCity:function(id, idCiudad = null){
            $.ajax({
                url:baseUrl + 'cities/'+id,
                type:'get',
                data:{},
                dataType:'json',
                success:function(response){
                    $('#IdCiudad option').remove();
                    $('#IdCiudadEdit option').remove();
                    var cities = response;
                    cities.forEach(function(city){
                        $('#IdCiudad').append(new Option(city.NombreCiudad, city.IdCiudad, false, false));
                        $('#IdCiudadEdit').append(new Option(city.NombreCiudad, city.IdCiudad, false, false));
                    });
                    if(idCiudad != null) {
                        setTimeout(function(){
                            $('#IdCiudadEdit').val(idCiudad);
                        },1000);

                    }

                },
                error:function(response){

                }
            })
        },
        getCatalogs:function(){
            $.ajax({
                url:baseUrl + 'catalogsNewClient',
                type:'get',
                data:{},
                dataType:'json',
                success:function(response){
                    var states = response.states;
                    var carriers = response.carriers;
                    var agents = response.agents;

                    clients.getCity(states[0].IdEstado);
                    states.forEach(function(state){
                        $('#IdEstado').append(new Option(state.NombreEstado, state.IdEstado, false, false));
                        $('#IdEstadoEdit').append(new Option(state.NombreEstado, state.IdEstado, false, false));
                    });
                    carriers.forEach(function(carrier){
                        $('#IdTransportista').append(new Option(carrier.Nombre, carrier.IdTransportista, false, false));
                        $('#IdTransportistaEdit').append(new Option(carrier.Nombre, carrier.IdTransportista, false, false));
                    });
                    agents.forEach(function(agent){
                        $('#IdAgente').append(new Option(agent.Nombre, agent.IdEmpleado, false, false));
                        $('#IdAgenteEdit').append(new Option(agent.Nombre, agent.IdEmpleado, false, false));
                    });
                },
                error:function(response){

                }
            })
        },
        updateClient:function(){
            $("#editClientForm").validate({
                rules: {
                    Nombre:{required:true},
                    DomicilioFiscal:{required:true},
                    Descuento:{required:true},
                    Telefono:{required:true},
                    Email:{required:true,email:true},
                    RFC:{required:true},
                    Condiciones:{required:true},
                    Contacto:{required:true},
                    DomicilioEntrega:{required:true}

                },submitHandler: function() {
                    var data = $('#editClientForm').serialize();
                    $.ajax({
                        url:'clients/'+clients.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewClient").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveClient:function(){

            $("#newClientForm").validate({
                rules: {
                    Nombre:{required:true},
                    DomicilioFiscal:{required:true},
                    Descuento:{required:true},
                    Telefono:{required:true},
                    Email:{required:true,email:true},
                    RFC:{required:true},
                    Condiciones:{required:true},
                    Contacto:{required:true},
                    DomicilioEntrega:{required:true}

                },submitHandler: function() {
                    var data = $('#newClientForm').serialize();
                    $.ajax({
                        url:'clients',
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewClient").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        clients.init()
    })
})();