(function(){
    var models = {
        id:null,
        lines:[],
        init:function () {

            $('#buttonNewModel').on('click',function(){
                $('#modalNewModel').modal()
            });
            this.getCatalogs();
            this.setValidator();
            this.saveModel();
            this.updateModel();
            this.addModelFormation();
            $('#material,#formacion').select2({theme:'bootstrap'});
            $('#btnUpdateImage').on('click',function() {
                models.updateImage();
            });
            $('#ChapasChk').on('change',function(){
                var isChecked = $(this).is(":checked");
                if(!isChecked){
                    $('#Chapas').attr('disabled',true);
                }else{
                    $('#Chapas').attr('disabled',false);
                }
            });
            $('#ChapasPVC').on('change',function(){
                var isChecked = $(this).is(":checked");
                if(!isChecked){
                    $('#PVC').attr('disabled',true);
                }else{
                    $('#PVC').attr('disabled',false);
                }
            });

            $('#esTablero').on('change',function () {
               var value = $(this).val();
               if(value == 0){
                   $('#contBoardOptions').css('display','none');
               }else{
                   $('#contBoardOptions').css('display','block');
               }
            });
            $("input:radio[name=typeModelRadios]").click(function(){
                var value = $(this).val();
                if(value == "formacion") {
                    $('#material,#Cantidad,#IdProceso').attr('disabled','disabled');
                    $('#formacion').attr('disabled',false);
                    $('#IdCategoriaColor').attr('disabled','disabled');
                }else if(value == "material"){
                    $('#material,#Cantidad,#IdProceso').attr('disabled',false);
                    $('#formacion').attr('disabled','disabled');
                    $('#IdCategoriaColor').attr('disabled',false);
                }
            });
            $('.editModel').on('click',function(e){
                e.preventDefault();

                var id = $(this).data('id');

                $.ajax({
                    url:'models/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){

                        var model = response
                        models.id = model.IdModelo;
                        for (var property in model){
                            $('#'+property+'Edit').val(model[property])
                        }
                        $('#modelImage').attr('src',model.Imagen);
                        if(window.USER_ROLE != 1) {
                            $('#Precio1Edit').attr('disabled','disabled')
                        }
                        $("#modalEditModel").modal();
                    },error:function(){

                    }
                })
            });
            $('.buttonAddModelFormation').on('click',function(e){
                if(window.USER_ROLE != 1){
                    swal({
                        title : "Lo sentimos",
                        text : 'No tienes permitido crear formaciones de modelo',
                        type : "warning"
                    },function(){
                        $("#modalEditModel").modal('hide');
                    });
                    return;
                }
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url:'models/'+id,
                    type:'get',
                    data:{},
                    dataType:'json',
                    success:function(response){
                        models.id = response.IdModelo;
                        var details = response.formations;
                        var detailConcat = '';
                        details.forEach(function(detail){
                            detailConcat +='<tr id="row'+detail.IdFormacionModelo+'"><td>'+ '<div class="form-group" style="display: flex;"> <input type="number" class="form-control " id="quantityModel'+detail.IdFormacionModelo+'" value="'+detail.Cantidad+'">'+'<button  data-id ="'+detail.IdFormacionModelo+'" class="btn btn-sm btn-success buttonChangeQuantity"><i class="fa fa-save"></i></button></div>'+'</td>'+
                                '<td>'+detail.material.Clave+'</td>'+
                                '<td>'+detail.material.NombreComercial+'</td>'+
                                '<td>'+detail.process.NombreProceso+'</td>'+
                                '<td>'+((detail.EsTablero==0)?'No':'Si')+'</td>'+
                                '<td>Chapas:'+(detail.Chapas || '/NA' )+',PVC:'+(detail.PVC || 'N/A' )+'</td>'+
                                '<td>'+detail.material.Largo+'x'+detail.material.Ancho+'</td>'+
                                '<td> <button class="btn btn-dark buttonDeleteDetail" data-id ="'+detail.IdFormacionModelo+'" ><i class="fa fa-trash"></i></button></td>'+
                                +'</tr>';
                        });
                        $('#detailsInModelFormation').html(detailConcat);
                        $('.buttonChangeQuantity').on('click',function(){
                            var id = $(this).data('id');
                            var quantity=$('#quantityModel'+id).val();
                            swal({
                                title: "¿Seguro?",
                                text: "Deseas cambiar la cantidad de la formación del modelo?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Si, estoy seguro!',
                                cancelButtonText: "No, cancelar"
                            },function(isConfirm){
                                if(isConfirm){
                                    $.ajax({
                                        url:'changeQuantityModelFormation/'+models.id+'/formation/'+id+'/'+quantity,
                                        type:'patch',
                                        data:{},
                                        dataType:'json',
                                        success:function(response){
                                            swal({
                                                title : "Exito",
                                                text : response.msg,
                                                type : "success"
                                            },function(){});

                                        },error:function(){}
                                    })
                                }
                            })

                        });

                        $('.buttonDeleteDetail').on('click',function(){
                            var id = $(this).data('id');
                            swal({
                                title: "¿Seguro?",
                                text: "Deseas eliminar el material de la formacion del modelo?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Si, estoy seguro!',
                                cancelButtonText: "No, cancelar"
                            },function(isConfirm){
                                if(isConfirm){
                                    $.ajax({
                                         url:'removeModelFormation/'+models.id+'/formation/'+id,
                                         type:'delete',
                                         data:{},
                                         dataType:'json',
                                         success:function(response){
                                             swal({
                                             title : "Exito",
                                             text : response.msg,
                                             type : "success"
                                         },function(){});
                                            $('#row'+id).remove();
                                         },error:function(){}
                                     })
                                }
                            })

                        });
                        $("#modalAddModelFormation").modal();
                    },error:function(){
                    }
                });
            });
        },
        updateImage: function() {
            var fileData  = $('#fileEditing').prop("files")[0];

            var myFormData = new FormData();
            myFormData.append('file', fileData);

            $.ajax({
                url: 'models/updateImage/'+models.id,
                type: 'POST',
                processData: false, // important
                contentType: false, // important
                dataType : 'json',
                data: myFormData,
                beforeSend:function(){
                    swal({
                        title:'Procesando',
                        text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                        type: 'warning',
                        confirmButtonColor: "#00444f",
                        showCancelButton: false,
                        showConfirmButton: false,
                        html:true
                    });
                },
                success:function(response){
                    swal({
                        title : "Exito",
                        text : response.msg,
                        type : "success"
                    },function(){
                        $("#modalEditModel").modal('hide');
                        location.reload();
                    });
                },
                error:function(response){

                }
            });
        },
        addModelFormation:function(){
            $("#addModelFormationForm").validate({
                rules: {

                },submitHandler: function() {
                    var data = $('#addModelFormationForm').serialize();

                    $.ajax({
                        url:'addModelFormation/'+models.id,
                        type:'post',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){

                            $('#detailsInModelFormation').html('');
                            var details = response.model.formations;
                            var detailConcat = '';
                            details.forEach(function(detail){
                                detailConcat +='<tr id="row'+detail.IdFormacionModelo+'">' +
                                    '<td>'+ '<div class="form-group" style="display: flex;"> <input type="number" id="quantityModel'+detail.IdFormacionModelo+'" class="form-control "   value="'+detail.Cantidad+'">'+'<button data-id ="'+detail.IdFormacionModelo+'"  class="btn btn-sm btn-success buttonChangeQuantity"><i class="fa fa-save"></i></button></div>'+'</td>'+
                                    '<td>'+detail.material.Clave+'</td>'+
                                    '<td>'+detail.material.NombreComercial+'</td>'+
                                    '<td>'+detail.process.NombreProceso+'</td>'+
                                    '<td>'+((detail.EsTablero==0)?'No':'Si')+'</td>'+
                                    '<td>Chapas:'+(detail.Chapas || '/NA' )+',PVC:'+(detail.PVC || 'N/A' )+'</td>'+
                                    '<td>'+detail.material.Largo+'x'+detail.material.Ancho+'</td>'+
                                    '<td> <button class="btn btn-dark buttonDeleteDetail" data-id ="'+detail.IdFormacionModelo+'" ><i class="fa fa-trash"></i></button></td>'+
                                    +'</tr>';
                            });
                            $('#detailsInModelFormation').html(detailConcat);
                            $('.buttonChangeQuantity').on('click',function(){
                                var id = $(this).data('id');
                                var quantity=$('#quantityModel'+id).val();

                                swal({
                                    title: "¿Seguro?",
                                    text: "Deseas cambiar la cantidad de la formación del modelo?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: '#DD6B55',
                                    confirmButtonText: 'Si, estoy seguro!',
                                    cancelButtonText: "No, cancelar"
                                },function(isConfirm){
                                    if(isConfirm){
                                        $.ajax({
                                            url:'changeQuantityModelFormation/'+models.id+'/formation/'+id+'/'+quantity,
                                            type:'patch',
                                            data:{},
                                            dataType:'json',
                                            success:function(response){
                                                swal({
                                                    title : "Exito",
                                                    text : response.msg,
                                                    type : "success"
                                                },function(){});

                                            },error:function(){}
                                        })
                                    }
                                })

                            });

                            $('.buttonDeleteDetail').on('click',function(){
                                var id = $(this).data('id');
                                swal({
                                    title: "¿Seguro?",
                                    text: "Deseas eliminar el material de la formacion del modelo?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: '#DD6B55',
                                    confirmButtonText: 'Si, estoy seguro!',
                                    cancelButtonText: "No, cancelar"
                                },function(isConfirm){
                                    if(isConfirm){
                                        $.ajax({
                                            url:'removeModelFormation/'+models.id+'/formation/'+id,
                                            type:'delete',
                                            data:{},
                                            dataType:'json',
                                            success:function(response){
                                                swal({
                                                    title : "Exito",
                                                    text : response.msg,
                                                    type : "success"
                                                },function(){});
                                                $('#row'+id).remove();
                                            },error:function(){}
                                        })
                                    }
                                })

                            });
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                // $("#modalAddFormation").modal('hide');
                                //location.reload();
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        setValidator:function(){
            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                email: "Por favor, escribe una dirección de email válida.",
                remote: "Por favor, rellena este campo.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });
            $.validator.setDefaults({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        },
        getCatalogs:function(){
            $.ajax({
                url:baseUrl + 'catalogsNewModelFormation',
                type:'get',
                data:{},
                dataType:'json',
                success:function(response){
                    var baseFormations = response.baseFormations;
                    var materials  = response.materials;
                    var processes = response.processes;
                    var lines = response.lines;
                    var category = response.categories;
                    baseFormations.forEach(function(formation){
                        $('#formacion').append(new Option( (formation.Clave+' - '+formation.Nombre),formation.IdFormacionBase, false, false));
                    });
                    materials.forEach(function(material){
                       $('#material').append(new Option( (material.Clave+' - '+material.NombreComercial),material.IdMaterial, false, false));
                    });
                    processes.forEach(function(process){
                        $('#IdProceso').append(new Option( (process.NombreProceso),process.IdProceso, false, false));
                    });
                    lines.forEach(function(line){
                       $('#IdLinea').append(new Option( (line.Nombre),line.IdLinea,false,false))
                        $('#IdLineaEdit').append(new Option( (line.Nombre),line.IdLinea,false,false))
                    });
                    category.forEach(function(category){
                        $('#IdCategoriaColor').append(new Option(category.Nombre, category.IdCategoriaColor, false, false));
                        $('#IdCategoriaColorEdit').append(new Option(category.Nombre, category.IdCategoriaColor, false, false));
                    });

                   // materials.forEach(function(material){
                     //   $('#IdMaterial').append(new Option(material.Clave, material.IdMaterial, false, false));
                    //});
                },
                error:function(response){

                }
            })
        },
        updateModel:function(){

            $("#editModelForm").validate({
                rules: {
                    Clave:{required:true},
                    Nombre: {required: true},
                    Descripcion:{required:true},
                    IdLinea:{required:true},
                    Precio1:{required:true}
                },submitHandler: function() {
                    if(window.USER_ROLE != 1){
                        swal({
                            title : "Lo sentimos",
                            text : 'No tienes permitido editar modelos',
                            type : "warning"
                        },function(){
                            $("#modalEditModel").modal('hide');
                        });
                        return;
                    }
                    var data = $('#editModelForm').serialize();
                    $.ajax({
                        url:'models/'+models.id,
                        type:'put',
                        data:data,
                        dataType:'json',
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewModel").modal('hide');
                            });
                        },
                        error:function(response){

                        }
                    })
                }
            });
        },
        saveModel:function(){

            $("#newModelForm").validate({
                rules: {
                    Clave:{required:true},
                    Nombre: {required: true},
                    Descripcion:{required:true},
                    IdLinea:{required:true},
                    Precio1:{required:true},
                    file:{required:true}
                },submitHandler: function() {
                    if(window.USER_ROLE != 1){
                        swal({
                            title : "Lo sentimos",
                            text : 'No tienes permitido crear modelos',
                            type : "warning"
                        },function(){
                            $("#modalEditModel").modal('hide');
                        });
                        return;
                    }
                    var data = new FormData($('#newModelForm')[0]);
                    $.ajax({
                        url:'models',
                        type:'post',
                        data:data,
                        cache:false,
                        contentType: false,
                        processData: false,
                        beforeSend:function(){
                            swal({
                                title:'Procesando',
                                text: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                type: 'warning',
                                confirmButtonColor: "#00444f",
                                showCancelButton: false,
                                showConfirmButton: false,
                                html:true
                            });
                        },
                        success:function(response){
                            swal({
                                title : "Exito",
                                text : response.msg,
                                type : "success"
                            },function(){
                                $("#modalNewModel").modal('hide');
                                location.reload();
                            });
                        },
                        error:function(response){
                            var myError = response.responseJSON.error;
                            swal({
                                title: "Atención",
                                text: myError,
                                type:"warning"
                            });
                        }
                    })
                }
            });
        }
    };
    $(document).ready(function(){
        models.init()
    })
})();