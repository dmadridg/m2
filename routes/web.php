<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', ['uses' => 'LoginController@getLogin', 'as' => 'login']);
//Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
Route::post('login', ['uses' => 'LoginController@authenticate', 'as' => 'login']);


Route::middleware(['auth'])->group(function(){
    Route::group(['prefix'=>'dashboard','as'=>'dashboard.'],function(){
        Route::get('/',['uses'=>'DashboardController@index']);
        Route::post('addColor','ColorsCategoryController@addColor')->name('addColor');
        Route::post('addBaseFormation/{id}','FormationsController@addBaseFormation')->name('addBaseFormation');
        Route::post('addModelFormation/{id}','ModelsController@addModelFormation')->name('addModelFormation');
        Route::delete('remove/{id}/detail/{detail}','FormationsController@removeDetailFormation')->name('removeDetailFormation');
        Route::delete('removeModelFormation/{id}/formation/{formation}','ModelsController@removeModelFormation')->name('removeModelFormation');
        Route::delete('removeColorFromCategory/{id}/color/{color}','ColorsCategoryController@removeColor')->name('removeColorFromCategory');


        Route::patch('changeQuantityModelFormation/{id}/formation/{formation}/{quantity}','ModelsController@changeQuantityModelFormation')->name('changeQuantityModelFormation');
        Route::resource('clients','ClientsController');
        Route::resource('users','UserController');
        Route::resource('carriers','CarrierController');
        Route::resource('processes','ProcessesController');
        Route::resource('boards','BoardController');
        Route::resource('lines','LinesController');
        Route::resource('units','UnitsOfMessureController');
        Route::resource('colors','ColorsController');
        Route::resource('quotations','QuotationController');
        Route::resource('quotationsDetail','QuotationDetailController');
        Route::resource('categories','ColorsCategoryController');
        Route::resource('models','ModelsController');

        Route::post('quotations/sendEmail', [
            'as' => 'quotations.sendEmail',
            'uses' => 'QuotationController@sendEmail'
        ]);
        
        Route::post('quotations/saveImage', [
            'as' => 'quotations.saveImage',
            'uses' => 'QuotationController@saveImage'
        ]);

        Route::post('quotations/getModel', [
            'as' => 'quotations.getModel',
            'uses' => 'QuotationController@getModel'
        ]);

        Route::post('models/destroy', [
            'as' => 'models.destroy',
            'uses' => 'ModelsController@destroy'
        ]);

        Route::post('models/updateImage/{id}','ModelsController@updateImage')->name('updateImage');
        Route::resource('materials','MaterialsController');

        //Route::get('categories/{id}/color','ColorsCategoryController@getColors');
        Route::resource('formations','FormationsController');

        Route::get('logout',[function(){
            Auth::logout();
            session()->flush();
            return redirect()->route('login')->withErrors(['msg' => 'La sesión ha terminado']);
        },'as'=>'logout']);

    });
});

Route::get('catalogsNewClient','UtilsController@getCatalogsForNewClient');
Route::get('catalogColors','UtilsController@getColors');
Route::get('catalogLines','UtilsController@getLines');
Route::get('catalogsNewMaterial','UtilsController@catalogsForNewMaterial');
Route::get('catalogsNewFormation','UtilsController@catalogsForNewFormation');
Route::get('catalogsNewQuotation','UtilsController@catalogsNewQuotation');
Route::get('catalogModels','UtilsController@getModels');
Route::get('catalogsNewModelFormation','UtilsController@catalogsForNewModelFormation');
Route::get('cities/{id}','UtilsController@getCities');
Route::get('detailModelForQuotation/{id}','ModelsController@detailForQuotation');
Route::get('printQuotation/{id}',['uses'=>'QuotationController@printQuotation','as'=>'printQuotation']);