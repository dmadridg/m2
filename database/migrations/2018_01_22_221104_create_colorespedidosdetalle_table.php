<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColorespedidosdetalleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('colorespedidosdetalle', function(Blueprint $table)
		{
			$table->integer('IdColorPedidoDetalle', true);
			$table->integer('IdPedidoDetalle');
			$table->integer('IdCategoriasColores');
			$table->integer('IdColores');
			$table->string('NombreColor', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('colorespedidosdetalle');
	}

}
