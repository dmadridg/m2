<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriascoloresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categoriascolores', function(Blueprint $table)
		{
			$table->integer('IdCategoriaColor', true);
			$table->string('Clave', 10)->unique('Clave');
			$table->string('Nombre', 100);
			$table->integer('Estatus');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categoriascolores');
	}

}
