<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormacionesbaseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('formacionesbase', function(Blueprint $table)
		{
			$table->integer('IdFormacionBase', true);
			$table->string('Clave', 20)->unique('Clave');
			$table->string('Nombre', 100);
			$table->integer('Estatus');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('formacionesbase');
	}

}
