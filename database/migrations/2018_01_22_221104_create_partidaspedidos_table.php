<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartidaspedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('partidaspedidos', function(Blueprint $table)
		{
			$table->integer('IdPartidaPedido', true);
			$table->integer('IdPartida');
			$table->integer('IdPedido');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('partidaspedidos');
	}

}
