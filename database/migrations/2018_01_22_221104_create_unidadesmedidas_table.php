<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnidadesmedidasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unidadesmedidas', function(Blueprint $table)
		{
			$table->integer('IdUnidadMedida', true);
			$table->string('Nombre', 30);
			$table->string('Descripcion', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unidadesmedidas');
	}

}
