<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLineasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lineas', function(Blueprint $table)
		{
			$table->integer('IdLinea', true);
			$table->string('Codigo', 20)->nullable();
			$table->string('Nombre', 150)->unique('Nombre');
			$table->integer('Descuento')->nullable();
			$table->integer('Incremento')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lineas');
	}

}
