<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos', function(Blueprint $table)
		{
			$table->integer('IdPedido', true);
			$table->integer('IdCotizacion');
			$table->integer('IdCliente');
			$table->integer('IdTransportista');
			$table->dateTime('FechaPedido');
			$table->integer('Estatus');
			$table->string('Referencia', 150);
			$table->text('Observaciones', 16777215);
			$table->integer('Lote');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos');
	}

}
