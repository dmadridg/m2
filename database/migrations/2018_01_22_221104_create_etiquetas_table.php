<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEtiquetasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('etiquetas', function(Blueprint $table)
		{
			$table->integer('IdEtiqueta', true);
			$table->string('CodigoEtiqueta', 50);
			$table->integer('IdCliente');
			$table->integer('IdTransportista');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('etiquetas');
	}

}
