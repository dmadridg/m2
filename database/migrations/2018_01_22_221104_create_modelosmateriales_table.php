<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModelosmaterialesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modelosmateriales', function(Blueprint $table)
		{
			$table->integer('IdModeloMaterial', true);
			$table->integer('IdModelo');
			$table->integer('IdMaterial');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modelosmateriales');
	}

}
