<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaterialesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('materiales', function(Blueprint $table)
		{
			$table->integer('IdMaterial', true);
			$table->enum('TipoMaterial', array('Herraje','Componente','',''));
			$table->string('Clave', 100)->unique('Clave');
			$table->string('NombreComercial', 300);
			$table->string('Clasificacion', 100)->nullable();
			$table->integer('IdCategoriaColor');
			$table->string('Familia', 100)->nullable();
			$table->integer('IdUnidadMedida')->nullable();
			$table->decimal('CostoPorUnidad', 10)->nullable();
			$table->integer('IdTablero')->nullable();
			$table->float('Ancho', 10, 0)->nullable();
			$table->float('Largo', 10, 0)->nullable();
			$table->float('CostoCalculado', 10, 0)->nullable();
			$table->integer('IdAlmacen');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('materiales');
	}

}
