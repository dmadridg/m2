<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColorescategoriascoloresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('colorescategoriascolores', function(Blueprint $table)
		{
			$table->integer('IdColorCategoriaColor', true);
			$table->integer('IdCategoriaColor');
			$table->integer('IdColor');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('colorescategoriascolores');
	}

}
