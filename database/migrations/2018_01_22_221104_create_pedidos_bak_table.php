<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosBakTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos_bak', function(Blueprint $table)
		{
			$table->integer('IdPedido', true);
			$table->integer('FolioPedido');
			$table->integer('IdCotizacion');
			$table->integer('IdCliente');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos_bak');
	}

}
