<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExplosionmaterialesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('explosionmateriales', function(Blueprint $table)
		{
			$table->integer('IdExplosionMaterial');
			$table->integer('Lote');
			$table->integer('SubLote');
			$table->integer('IdModelo');
			$table->integer('IdMaterial');
			$table->integer('IdProceso');
			$table->integer('Cantidad');
			$table->boolean('EsTablero');
			$table->boolean('Corte');
			$table->boolean('Router');
			$table->boolean('Curvos');
			$table->boolean('Puertas');
			$table->boolean('Molduras');
			$table->boolean('Costados');
			$table->boolean('Pantallas');
			$table->boolean('Entrepanos');
			$table->boolean('Divisiones');
			$table->boolean('Archiveros');
			$table->string('Chapas', 8);
			$table->string('PVC', 8);
			$table->boolean('Barrenos');
			$table->boolean('Zoclos');
			$table->boolean('Lado');
			$table->boolean('Color1');
			$table->boolean('Color2');
			$table->boolean('Color3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('explosionmateriales');
	}

}
