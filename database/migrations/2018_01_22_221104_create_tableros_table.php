<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTablerosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tableros', function(Blueprint $table)
		{
			$table->integer('IdTablero', true);
			$table->string('Nombre', 50)->unique('Nombre');
			$table->string('Descripcion', 100);
			$table->string('Espesor', 5);
			$table->decimal('Costo', 10, 0);
			$table->string('Tipo', 30);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tableros');
	}

}
