<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePruebaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prueba', function(Blueprint $table)
		{
			$table->integer('idprueba', true);
			$table->integer('nombre');
			$table->integer('nombre2');
			$table->integer('nombre3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prueba');
	}

}
