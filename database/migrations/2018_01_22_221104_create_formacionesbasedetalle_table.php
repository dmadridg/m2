<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormacionesbasedetalleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('formacionesbasedetalle', function(Blueprint $table)
		{
			$table->integer('IdFormacionBaseDetalle', true);
			$table->integer('IdFormacionBase');
			$table->integer('IdMaterial');
			$table->integer('IdProceso');
			$table->integer('IdCategoriaColor')->nullable();
			$table->integer('Cantidad');
			$table->boolean('EsTablero');
			$table->boolean('Corte');
			$table->boolean('Router');
			$table->boolean('Curvos');
			$table->boolean('Puertas');
			$table->boolean('Molduras');
			$table->boolean('Costados');
			$table->boolean('Pantallas');
			$table->boolean('Entrepanos');
			$table->boolean('Divisiones');
			$table->boolean('Archiveros');
			$table->string('Chapas', 8);
			$table->string('PVC', 8);
			$table->boolean('Barrenos');
			$table->boolean('Zoclos');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('formacionesbasedetalle');
	}

}
