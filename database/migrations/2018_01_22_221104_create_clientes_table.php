<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			$table->integer('IdCliente', true);
			$table->string('ClaveCliente', 50)->unique('ClaveCliente');
			$table->string('Nombre', 100)->unique('Nombre');
			$table->integer('TipoCliente');
			$table->integer('Descuento');
			$table->integer('IdEstado');
			$table->integer('IdCiudad');
			$table->string('Telefono', 50);
			$table->string('RFC', 20);
			$table->string('Email', 100);
			$table->integer('IdAgente');
			$table->integer('IdTransportista');
			$table->integer('Precio');
			$table->string('DomicilioFiscal', 200);
			$table->string('CP', 5);
			$table->string('CURP', 30);
			$table->string('Contacto', 100);
			$table->integer('Consignar');
			$table->integer('Zona');
			$table->string('DomicilioEntrega', 200);
			$table->string('Condiciones', 200);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
