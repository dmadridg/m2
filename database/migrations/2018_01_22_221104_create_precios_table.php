<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreciosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('precios', function(Blueprint $table)
		{
			$table->integer('IdPrecio', true);
			$table->integer('IdModelo')->nullable();
			$table->integer('Precio1');
			$table->integer('Precio2');
			$table->integer('Precio3');
			$table->integer('Precio4');
			$table->integer('Precio5');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('precios');
	}

}
