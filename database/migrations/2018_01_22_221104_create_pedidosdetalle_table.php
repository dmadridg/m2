<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosdetalleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidosdetalle', function(Blueprint $table)
		{
			$table->integer('IdPedidoDetalle', true);
			$table->integer('IdPedido');
			$table->integer('IdModelo');
			$table->integer('Cantidad');
			$table->decimal('Precio', 10)->nullable();
			$table->string('Area', 20);
			$table->string('Lados', 10);
			$table->decimal('Descuento', 10, 0);
			$table->decimal('Adicional', 10, 0);
			$table->decimal('SubTotal', 10, 0);
			$table->decimal('Total', 10, 0);
			$table->string('Observaciones', 400);
			$table->integer('Tipo');
			$table->integer('SubLote');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidosdetalle');
	}

}
