<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCotizacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cotizaciones', function(Blueprint $table)
		{
			$table->integer('IdCotizacion', true);
			$table->integer('IdPedido');
			$table->integer('IdCliente');
			$table->integer('IdTransportista');
			$table->dateTime('FechaCotizacion');
			$table->integer('Estatus');
			$table->string('Referencia', 150);
			$table->text('Observaciones', 16777215);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cotizaciones');
	}

}
