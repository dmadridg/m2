<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColorescotizacionesdetalleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('colorescotizacionesdetalle', function(Blueprint $table)
		{
			$table->integer('IdColorCotizacionDetalle', true);
			$table->integer('IdCotizacionDetalle');
			$table->integer('IdCategoriasColores');
			$table->integer('IdColores');
			$table->string('NombreColor', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('colorescotizacionesdetalle');
	}

}
