<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePerfilespermisosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('perfilespermisos', function(Blueprint $table)
		{
			$table->integer('IdPerfilesPermisos', true);
			$table->integer('IdPerfil');
			$table->integer('IdPermiso');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('perfilespermisos');
	}

}
