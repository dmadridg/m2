<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login - Euroespacio</title>
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
</head>

<body class="bg-dark">
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Inicio de sesión Euroespacio</div>
        <div class="card-body">
            @include('partials.errors')
            {!! Form::open(['route'=>'login', 'method' => 'POST']) !!}
                <div class="form-group">
                    <label for="exampleInputEmail1">Usuario</label>
                    <input name="email" class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="Ingrese su nombre de usuario">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input name="password" class="form-control" id="exampleInputPassword1" type="password" placeholder="Contraseña">
                </div>
                {!! Form::submit('Ingresar',['class' => 'btn btn-primary btn-block']) !!}
            <div class="text-center">

            </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('assets/plugins/validate/jquery.validate.min.js')}}"></script>
</body>
</html>







