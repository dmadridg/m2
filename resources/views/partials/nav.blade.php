<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="#"><img style="    height: 90px;
    width: 146px;
    position: absolute;
    vertical-align: top;
    display: block;
    margin-top: -33px;
    left: 6px;
    top: 16px;" src="{{asset('img/logoeuro.svg')}}" style="" alt="Logo de Euroespacio" class="img-admin-logo"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
                <a class="nav-link" href="{{route('dashboard.')}}">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>

                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion" aria-expanded="false">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">Catalogos generales</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents" style="">
                    <li>
                        {{ link_to_route('dashboard.clients.index', $title = "Clientes", $parameters = array(), $attributes = array()) }}
                    </li>
                    <li>
                        <a href="{{route('dashboard.carriers.index')}}">Transportistas</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.users.index')}}">Usuarios</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Example Pages">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion" aria-expanded="false">
                    <i class="fa fa-fw fa-file"></i>
                    <span class="nav-link-text">Catalogos modelos</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseExamplePages" style="">
                    <li>
                        <a href="{{route('dashboard.models.index')}}">Modelos productos</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.materials.index')}}">Materiales</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.formations.index')}}">Formaciones Base</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.colors.index')}}">Colores</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.categories.index')}}">Categorías de colores</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.units.index')}}">Unidades de medida</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.lines.index')}}">Lineas</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.boards.index')}}">Tableros</a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.processes.index')}}">Procesos</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseVentas" data-parent="#exampleAccordion" aria-expanded="false">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">Ventas</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseVentas" style="">
                    <li>
                        <a href="{{route('dashboard.quotations.index')}}">Cotizaciones</a>
                    </li>
                    <li>
                        <a href="cards.html">Pedidos</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseProduccion" data-parent="#exampleAccordion" aria-expanded="false">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">Producción</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseProduccion" style="">
                    <li>
                        <a href="navbar.html">Formar partidas</a>
                    </li>
                    <li>
                        <a href="cards.html">Partidas</a>
                    </li>
                </ul>
            </li>
            <!--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Menu Levels">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion" aria-expanded="false">
                    <i class="fa fa-fw fa-sitemap"></i>
                    <span class="nav-link-text">Menu Levels</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseMulti" style="">
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Third Level</a>
                        <ul class="sidenav-third-level collapse" id="collapseMulti2">
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>-->

        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('dashboard.logout')}}">
                    <i class="fa fa-fw fa-sign-out"></i>Salir</a>
            </li>
        </ul>
    </div>
</nav>
