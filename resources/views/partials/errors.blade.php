@if(count($errors) > 0)
<div class="alert alert-warning" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    @foreach($errors->all() as $error)
    <ul>
        <li> {{ $error }} </li>
    </ul>
    @endforeach
</div>
@endif

@if(!session('success') == null)
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    {{session('success')}}
</div>
@endif
