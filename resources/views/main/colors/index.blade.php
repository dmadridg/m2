@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewColor" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo color</button>

    {!! Form::open(['url'=>'dashboard/colors','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($colors)>0)
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($colors as $color)
                    <tr>
                        <td><a href="#" id="editColor{{$color->IdColor}}" data-id="{{$color->IdColor}}" class="editColor btn-light">{{$color->Nombre}}</a></td>
                        <td>{{$color->Descripcion}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron Colores, intenta una nueva búsqueda
            </div>
        </div>
        @endif

    {{ $colors->links() }}
    @include('main.colors.modalNew')
    @include('main.colors.modalEdit')
@endsection
@section('js')
    <script src="{{asset('js/colors/main.js')}}"></script>
@endsection