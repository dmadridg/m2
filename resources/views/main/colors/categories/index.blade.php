@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewCategory" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nueva categoría</button>

    {!! Form::open(['url'=>'dashboard/categories','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
   @if(count($categories)>0)
       <div class="table-responsive">
           <table class="table table-bordered table-hover">
               <thead class="thead-dark">
               <tr>
                   <th scope="col">Nombre</th>
                   <th scope="col">Clave</th>
                   <th scope="col">Relacionar Colores</th>
                </tr>
               </thead>
               <tbody>
               @foreach($categories as $category)
                   <tr>
                       <td><a href="#" id="editCategory{{$category->IdCategoriaColor}}" data-id="{{$category->IdCategoriaColor}}" class="editCategory btn-light">{{$category->Nombre}}</a></td>
                       <td>{{$category->Clave}}</td>
                       <td class="text-center"><button class="btn btn-dark buttonAddColor" data-id="{{$category->IdCategoriaColor}}" ><i class="fa fa-plus-square"></i></button></td>
                   </tr>
               @endforeach
               </tbody>
           </table>
       </div>
       @else
       <div class="table-responsive">
           <div class="alert alert-danger" role="alert">
               No se encontraron Categorias, intenta una nueva búsqueda
           </div>
       </div>
       @endif
    {{ $categories->links() }}
    @include('main.colors.categories.modalNew')
    @include('main.colors.categories.modalEdit')
    @include('main.colors.categories.modalAddColor')
@endsection
@section('js')
    <script src="{{asset('js/categories/main.js')}}"></script>
@endsection