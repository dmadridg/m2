<!-- Modal -->
<div class="modal fade" id="modalAddColorCategory" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">RELACIONAR CATEGORIA DE COLORES</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="addRelateColorForm" class="form row">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="ClaveColor">Clave</label>
                            <input  class="form-control" name="ClaveColor" id="ClaveColor" placeholder="Ex:1230" disabled>
                        </div>
                        <div class="form-group">
                            <label for="NombreColor">Nombre</label>
                            <input  class="form-control" name="NombreColor" id="NombreColor" placeholder="Ex:Colores" disabled>
                        </div>
                        <div class="form-group">
                            <label for="idColor">Color</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="color" id="idColor"></select>
                        </div>
                        <button type="button" id="buttonConfirmColor" class="btn btn-primary">Agregar color</button>
                    </div>
                </form>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Eliminar</th>
                             </tr>
                            </thead>
                            <tbody id="colorsInCategory">

                            </tbody>
                        </table>
                    </div>
                    <!--first column -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

            </div>

        </div>
    </div>
</div>
