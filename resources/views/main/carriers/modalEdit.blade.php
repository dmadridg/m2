<!-- Modal modalEditCarrier -->
<div class="modal fade" id="modalEditCarrier" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editando Transportista</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="editCarrierForm" class="form row">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="Codigo">Código</label>
                            <input  class="form-control" name="Codigo" id="CodigoEdit" placeholder="Ex:12323">
                        </div>
                        <div class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input  class="form-control" name="Nombre" id="NombreEdit" placeholder="Ex:FEDEX">
                        </div>


                    </div>
                    <!--first column -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
