@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewCarrier" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo Transportista</button>

    {!! Form::open(['url'=>'dashboard/carriers','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($carriers)>0)
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Código</th>
                </tr>
                </thead>
                <tbody>
                @foreach($carriers as $carrier)
                    <tr>
                        <td><a href="#" id="editCarrier{{$carrier->IdTransportista}}" data-id="{{$carrier->IdTransportista}}" class="editCarrier btn-light">{{$carrier->Nombre}}</a></td>
                        <th scope="row">{{$carrier->Codigo}}</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron Transportistas, intenta una nueva búsqueda
            </div>
        </div>
        @endif

    {{ $carriers->links() }}
    @include('main.carriers.modalNew')
    @include('main.carriers.modalEdit')
@endsection
@section('js')
    <script src="{{asset('js/carriers/main.js')}}"></script>
@endsection