<!-- Modal modalEditCarrier -->
<div class="modal fade" id="modalEditLine" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editando linea</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="editLineForm" class="form row">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input  class="form-control" name="Nombre" id="NombreEdit" placeholder="Ex:Unidad">
                        </div>
                        <div class="form-group">
                            <label for="Descuento">Descuento</label>
                            <input type="number"  class="form-control" name="Descuento" id="DescuentoEdit" placeholder="Ex:10">
                        </div>
                        <div class="form-group">
                            <label for="Incremento">Incremento</label>
                            <input type="number"  class="form-control" name="Incremento" id="IncrementoEdit" placeholder="Ex:5">
                        </div>
                    </div>
                    <!--first column -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
