<!-- Modal -->
<div class="modal fade" id="modalNewLine" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nueva linea</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  id="newLineForm" class="form row">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input  class="form-control" name="Nombre" id="Nombre" placeholder="Ex:Unidad">
                        </div>
                        <div class="form-group">
                            <label for="Descripcion">Descuento</label>
                            <input type="number"  class="form-control" name="Descuento" id="Descuento" placeholder="Ex:10">
                        </div>
                        <div class="form-group">
                            <label for="Descripcion">Incremento</label>
                            <input type="number"  class="form-control" name="Incremento" id="Incremento" placeholder="Ex:5">
                        </div>
                    </div>
                    <!--first column -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
