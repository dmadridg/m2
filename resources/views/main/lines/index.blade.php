@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewLine" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nueva unidad de medida</button>

    {!! Form::open(['url'=>'dashboard/lines','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($lines)>0)
        <div class="responsive-table">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Código</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descuento</th>
                    <th scope="col">Incremento</th>

                </tr>
                </thead>
                <tbody>
                @foreach($lines as $line)
                    <tr>
                        <th scope="row">{{($line->Codigo != null)?$line->Codigo:'Sin información'}}</th>
                        <td><a href="#" id="editLine{{$line->IdLinea}}" data-id="{{$line->IdLinea}}" class="editLine btn-light">{{$line->Nombre}}</a></td>
                        <td>{{$line->Descuento}}%</td>
                        <td>{{$line->Incremento}}%</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron Lineas, intenta una nueva búsqueda
            </div>
        </div>
        @endif
    {{ $lines->links() }}
    @include('main.lines.modalNew')
    @include('main.lines.modalEdit')
@endsection
@section('js')
    <script src="{{asset('js/lines/main.js')}}"></script>
@endsection