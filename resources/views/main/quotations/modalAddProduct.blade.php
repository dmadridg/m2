<!-- Modal -->
<div class="modal fade" id="modalAddProductQuotation" data-backdrop="static" role="dialog" aria-labelledby="modalNewProductQuotationTitle" aria-hidden="true">
    <div class="modal-dialog  modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalNewProductQuotationTitle">Ingresar Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >

                <form  id="addProductQuotationForm" class="form row">
                    <!--first column -->
                    <div class="col-6">
                        <h2>Modelo / Producto</h2>
                        <hr>
                        <div class="form-group">
                            <label for="cboArea">Area</label>
                            <input style="width: 100%" class="form-control input-100-percent" name="area" id="cboArea" />
                        </div>

                        <div class="form-group">
                            <label for="IdCliente">Descripción</label>
                            <textarea name="descripcionProducto" id="descripcionProducto" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="cantidadProducto">Cantidad</label>
                            <input  class="form-control" type="number" name="cantidadProducto" id="cantidadProducto"  value="1" min="1">
                        </div>
                        <div class="form-group">
                            <label for="Fecha">Lado</label>
                            <select name="ladoProducto" id="ladoProducto" class="form-control">
                                <option value="Izquierdo">Izquierdo</option>
                                <option value="Derecho">Derecho</option>
                                <option value="Ambos">Ambos</option>
                                <option value="No aplica" selected>No aplica</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="medidaProducto">Medida</label>
                            <input  class="form-control" name="medidaProducto" id="medidaProducto" disabled>
                        </div>
                        <div class="form-group">
                            <label for="Fecha">Precio</label>
                            <input  class="form-control"  name="precioProducto" id="precioProducto"  disabled>
                        </div>
                        <div class="form-group">
                            <label for="Descuento">Descuento</label>
                            <input type="number" class="form-control" min="0" value="0"  name="descuentoProducto" id="descuentoProducto"  >
                        </div>
                        <div class="divider"></div>
                        <div class="form-group">
                           <div class="row">

                               <div class="col-sm-6">
                                   <label for="costoadicionalTitle">Costo Adicional(Porcentaje)</label>
                                   <label for="costoadicionalPrice" id="porcentajeAdicional">5%</label>
                               </div>
                               <div class="col-sm-6">
                                   <div class="form-check" style="padding-left: 1.25rem;">
                                       <input class="form-check-input" name="costoAdicionalProducto" type="checkbox" id="costoAdicionalProducto">
                                       <label class="form-check-label" for="costoAdicionalProducto">
                                           ¿Aplicar costo adicional?
                                       </label>
                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>
                    <!--first column -->

                    <!--second column this column is dynamic-->
                    <div class="col-6">
                        <h2>Colores</h2>
                        <hr>
                        <div id="containerColorCategories"></div>

                    </div>
                    <!--second column -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonAddProductToQuotation" class="btn btn-primary">Guardar</button>
            </div>

        </div>
    </div>
</div>
