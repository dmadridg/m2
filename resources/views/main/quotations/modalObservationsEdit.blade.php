<!-- Modal -->
<div class="modal fade" id="modalObservationsEdit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Observaciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="observationsFormEdit" class="form row">

                    <!--second column -->
                    <div class="col-12">
                        <div class="form-group">
                            <label for="observationsEdit">Observaciones</label>
                            <textarea name="observations" class="form-control" id="observationsEdit" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonSaveQuotationEdit" class="btn btn-primary">Guardar</button>
            </div>

        </div>
    </div>
</div>
