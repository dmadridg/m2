<!-- Modal -->
<div class="modal fade" id="modalNewQuotation" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nueva cotización</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="newQuotationForm" class="form row">
                    <!--first column -->
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ClaveCliente">Clave Cliente</label>
                            <input  class="form-control" name="ClaveCliente" id="ClaveCliente" placeholder="Ex:12323" disabled>
                        </div>
                        <div class="form-group">
                            <label for="IdCliente">Nombre del cliente</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdCliente" id="IdCliente"></select>
                        </div>
                        <div class="form-group">
                            <label for="Contacto">Contacto</label>
                            <input  class="form-control" name="Contacto" id="Contacto" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="Fecha">Fecha de cotización</label>
                            <input  class="form-control" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}" name="Fecha" id="Fecha" placeholder="" disabled>
                        </div>
                    </div>
                    <!--first column -->

                    <!--second column -->
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Direccion">Dirección</label>
                            <input  class="form-control" name="Direccion" id="Direccion" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="Ciudad">Ciudad</label>
                            <input  class="form-control" name="Ciudad" id="Ciudad" placeholder=""  disabled>
                        </div>
                        <div class="form-group">
                            <label for="Telefono">Teléfono</label>
                            <input  class="form-control" name="Telefono" id="Telefono" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="RFC">RFC</label>
                            <input  class="form-control" name="RFC" id="RFC" placeholder="" disabled>
                        </div>
                    </div>
                    <!--second column -->
                    <div class="col-12">
                        <hr>
                        <h3>Modelos y Productos</h3>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" name="claveModelo" id="txtClaveModelo" class="form-control" placeholder="Clave Modelo" style="margin-right: 10px;">
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        <input type="text" id="txtNombreModelo" name="clave" class="form-control" placeholder="Nombre Modelo">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="btnSearchModels" class="btn btn-secondary">Buscar</button>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-inline pull-right">
                                    <div class="form-group pull-right">
                                        <button type="button" id="btnAddSpecialProduct" class="btn btn-secondary">Agregar producto especial</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive" style="max-height: 300px;">
                            <hr>
                            <table class="table table-bordered table-hover" >
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Clave</th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th>Precio</th>
                                    <th>Linea</th>
                                </tr>
                                </thead>
                                <tbody id="detailsInFormation" style="overflow: auto">
                                    <tr>
                                        <td colspan="5" class="text-center">Haga una búsqueda para mostrar modelos</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12" style="margin-top: 50px">
                        <hr>
                        <h3>Cotización en progreso</h3>
                        <div class="table-responsive" style="max-height: 300px;">
                            <hr>
                            <table class="table table-bordered table-hover" >
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Código</th>
                                    <th>Descripción</th>
                                    <th>Medida</th>
                                    <th>Precio Unit</th>
                                    <th>Cantidad</th>
                                    <th>Descuento</th>
                                    <th>Total</th>
                                    <th>Area</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody id="detailsInFormationQuotation" style="overflow: auto">
                                    <tr>
                                        <td colspan="9" class="text-center">No se han agregado modelos a esta cotización</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonShowObservations" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
