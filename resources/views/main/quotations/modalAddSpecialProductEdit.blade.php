<!-- Modal -->
<div class="modal fade" id="modalAddSpecialProductEdit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Ingresar articulo especial</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form name="saveImageAdd" id="addSpecialProductQuotationFormEdit" class="form row">
                    <div class="col-12">
                        <h2>Caracteristicas del producto</h2>
                        <hr>
                        <div class="form-group">
                            <label for="AddcboAreaEspecial">Area</label>
                            <input style="width: 100%" class="form-control input-100-percent" name="AddcboAreaEspecial" id="AddcboAreaEspecialEdit" />
                        </div>
                        <div class="form-group">
                            <label for="IdCliente">Descripción</label>
                            <textarea name="descripcionProductoEspecial" id="AdddescripcionProductoEspecialEdit" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                       <div class="form-row">
                           <div class="form-group col-6">
                               <label for="cantidadProductoEspecial">Cantidad</label>
                               <input  class="form-control" type="number" name="cantidadProductoEspecial" id="AddcantidadProductoEspecialEdit"  value="1" min="1">
                           </div>
                           <div class="form-group col-6">
                               <label for="colorBaseEspecial">Color Base</label>
                               <select style="width: 100%" class="form-control input-100-percent" name="colorBaseEspecial" id="AddcolorBaseEspecialEdit"></select>
                           </div>
                       </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="colorCombinacionEspecial">Color Combinación</label>
                                <select style="width: 100%" class="form-control input-100-percent" name="colorCombinacionEspecial" id="AddcolorCombinacionEspecialEdit"></select>
                            </div>
                            <div class="form-group col-6">
                                <label for="ladoProductoEspecialEdit">Lado</label>
                                <select name="ladoProductoEspecial" id="AddladoProductoEspecialEdit" class="form-control">
                                    <option value="Izquierdo">Izquierdo</option>
                                    <option value="Derecho">Derecho</option>
                                    <option value="Ambos">Ambos</option>
                                    <option value="No aplica" selected>No aplica</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="medidaProductoEspecialEdit">Medida</label>
                                <input  class="form-control" name="medidaProductoEspecial" id="AddmedidaProductoEspecialEdit">
                            </div>
                            <div class="form-group col-6">
                                <label for="AddprecioProductoEspecialEdit">Precio</label>
                                <input type="number" min="0" value="0" class="form-control"  name="precioProductoEspecial" id="AddprecioProductoEspecialEdit">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="descuentoProductoEspecialEdit">Descuento</label>
                                <input type="number" class="form-control" min="0" value="0"  name="descuentoProductoEspecial" id="AdddescuentoProductoEspecialEdit">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <div style="width: 200px; height: 200px; margin: 0 auto;">
                                    <!-- <img id="imgPreview" src="" alt="imgPreview" style="width: 200px; height: 200px;"> -->
                                </div>
                                <label for="descuentoProductoEspecial">Imagen</label>
                                <input type="file" class="form-control" name="imgPathAdd" id="imgPathAdd"  >
                            </div>
                        </div>

                        <div class="divider"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonSaveProductSpecialEdit" class="btn btn-primary">Guardar</button>
            </div>

        </div>
    </div>
</div>
