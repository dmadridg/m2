<!-- Modal -->
<div class="modal fade" id="modalQuotationEdit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editando cotización</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="editQuotationForm" class="form row">
                    <!--first column -->
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ClaveCliente">Clave Cliente</label>
                            <input  class="form-control" name="ClaveCliente" id="ClaveClienteEdit" placeholder="Ex:12323" disabled>
                            <input  class="form-control" name="" id="idQuotation" hidden>
                        </div>
                        <div class="form-group">
                            <label for="IdCliente">Nombre del cliente</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdCliente" id="IdClienteEdit" disabled></select>
                        </div>
                        <div class="form-group">
                            <label for="Contacto">Contacto</label>
                            <input  class="form-control" name="Contacto" id="ContactoEdit" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="Fecha">Fecha de cotización</label>
                            <input  class="form-control"  name="Fecha" id="FechaEdit" placeholder="" disabled>
                        </div>
                    </div>
                    <!--first column -->

                    <!--second column -->
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Direccion">Dirección</label>
                            <input  class="form-control" name="Direccion" id="DireccionEdit" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="Ciudad">Ciudad</label>
                            <input  class="form-control" name="Ciudad" id="CiudadEdit" placeholder=""  disabled>
                        </div>
                        <div class="form-group">
                            <label for="Telefono">Teléfono</label>
                            <input  class="form-control" name="Telefono" id="TelefonoEdit" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="RFC">RFC</label>
                            <input  class="form-control" name="RFC" id="RFCEdit" placeholder="" disabled>
                        </div>
                    </div>
                    <!--second column -->
                    <div class="col-12">
                        <hr>
                        <h3>Modelos y Productos</h3>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" name="claveModelo" id="txtClaveModeloEdit" class="form-control" placeholder="Clave Modelo" style="margin-right: 10px;">
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        <input type="text" id="txtNombreModeloEdit" name="clave" class="form-control" placeholder="Nombre Modelo">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="btnSearchModelsEdit" class="btn btn-secondary">Buscar</button>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-inline pull-right">
                                    <div class="form-group pull-right">
                                        <button type="button" id="btnAddSpecialProductEdit" class="btn btn-secondary">Agregar producto especial</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive" style="max-height: 300px;">
                            <hr>
                            <table class="table table-bordered table-hover" >
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Clave</th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th>Precio</th>
                                    <th>Linea</th>
                                </tr>
                                </thead>
                                <tbody id="detailsInFormationEdit" style="overflow: auto">
                                    <tr>
                                        <td colspan="5" class="text-center">Haga una búsqueda para mostrar modelos</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12" style="margin-top: 50px">
                        <hr>
                        <h3>Nuevos productos agregados a esta cotización</h3>
                        <div class="table-responsive" style="max-height: 300px;">
                            <hr>
                            <table class="table table-bordered table-hover" >
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Código</th>
                                    <th>Descripción</th>
                                    <th>Medida</th>
                                    <th>Precio Unit</th>
                                    <th>Cantidad</th>
                                    <th>Descuento</th>
                                    <th>Total</th>
                                    <th>Area</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody id="detailsInFormationQuotationEdit" style="overflow: auto">
                                    <tr>
                                        <td colspan="9" class="text-center">No se han agregado modelos a esta cotización</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-sm-12" style="margin-top: 50px">
                        <hr>
                        <h3>Productos agregados anteriormente a esta cotización</h3>
                        <div class="table-responsive" style="max-height: 300px;">
                            <hr>
                            <table class="table table-bordered table-hover" >
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Código</th>
                                    <th>Descripción</th>
                                    <th>Medida</th>
                                    <th>Precio Unit</th>
                                    <th>Cantidad</th>
                                    <th>Descuento</th>
                                    <th>Total</th>
                                    <th>Area</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody id="detailsInFormationQuotationEarlyEdit" style="overflow: auto">
                                <tr>
                                    <td colspan="9" class="text-center">No se han agregado modelos a esta cotización</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonShowObservationsEdit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>

</script>
