<!-- Modal -->
<div class="modal fade" id="modalEditSpecialProductEdit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar Articulo Especial</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form name="saveImageEdit" id="editSpecialProductQuotationFormEdit" class="form row">
                    <div class="col-12">
                        <h2>Caracteristicas del producto</h2>
                        <hr>
                        <div class="form-group">
                            <!-- <label for="idProduct">Area</label> -->
                            <input style="width: 100%" class="form-control input-100-percent" hidden name="idProduct" id="idProductEdit" />
                            <input style="width: 100%" class="form-control input-100-percent" hidden name="idModel" id="idModelEdit" />
                        </div>
                        <div class="form-group">
                            <label for="cboAreaEspecial">Area</label>
                            <input style="width: 100%" class="form-control input-100-percent" name="cboAreaEspecial" id="cboAreaEspecialEdit" />
                        </div>
                        <div class="form-group">
                            <label for="IdCliente">Descripción</label>
                            <textarea name="descripcionProductoEspecial" id="descripcionProductoEspecialEdit" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                       <div class="form-row">
                           <div class="form-group col-6">
                               <label for="cantidadProductoEspecial">Cantidad</label>
                               <input  class="form-control" type="number" name="cantidadProductoEspecial" id="cantidadProductoEspecialEdit"  value="1" min="1">
                           </div>
                           <div class="form-group col-6">
                               <label for="colorBaseEspecial">Color Base</label>
                               <select style="width: 100%" class="form-control input-100-percent" name="colorBaseEspecial" id="colorBaseEspecialEdit"></select>
                           </div>
                       </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="colorCombinacionEspecial">Color Combinación</label>
                                <select style="width: 100%" class="form-control input-100-percent" name="colorCombinacionEspecial" id="colorCombinacionEspecialEdit"></select>
                            </div>
                            <div class="form-group col-6">
                                <label for="ladoProductoEspecialEdit">Lado</label>
                                <select name="ladoProductoEspecial" id="ladoProductoEspecialEdit" class="form-control">
                                    <option value="Izquierdo">Izquierdo</option>
                                    <option value="Derecho">Derecho</option>
                                    <option value="Ambos">Ambos</option>
                                    <option value="No aplica" selected>No aplica</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="medidaProductoEspecialEdit">Medida</label>
                                <input  class="form-control" name="medidaProductoEspecial" id="medidaProductoEspecialEdit">
                            </div>
                            <div class="form-group col-6">
                                <label for="precioProductoEspecialEdit">Precio</label>
                                <input type="number" min="0" value="0" class="form-control"  name="precioProductoEspecial" id="precioProductoEspecialEdit">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="descuentoProductoEspecialEdit">Descuento</label>
                                <input type="number" class="form-control" min="0" value="0"  name="descuentoProductoEspecial" id="descuentoProductoEspecialEdit">
                            </div>
                        </div>

                        <div class="form-row" id="fileDiv">
                            <div class="form-group col-12">
                                <div style="width: 200px; height: 200px; margin: 0 auto;">
                                    <img id="imgPreview" src="" alt="imgPreview" style="width: 200px; height: 200px;">
                                </div>
                                <label for="imgPathEdit">Imagen</label>
                                <input type="file" class="form-control" name="imgPathEdit" id="imgPathEdit"  >
                            </div>
                        </div>

                        <div class="divider"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonEditProductSpecialEdit" class="btn btn-primary">Actualizar</button>
            </div>

        </div>
    </div>
</div>
