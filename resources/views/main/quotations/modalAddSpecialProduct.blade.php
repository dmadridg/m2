<!-- Modal -->
<div class="modal fade" id="modalAddSpecialProduct" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Ingresar Articulo Especial</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form name="saveImage" id="addSpecialProductQuotationForm" class="form row">
                    <div class="col-12">
                        <h2>Caracteristicas del producto</h2>
                        <hr>
                        <div class="form-group">
                            <label for="cboAreaEspecial">Area</label>
                            <input style="width: 100%" class="form-control input-100-percent" name="cboAreaEspecial" id="cboAreaEspecial" />
                        </div>
                        <div class="form-group">
                            <label for="IdCliente">Descripción</label>
                            <textarea name="descripcionProductoEspecial" id="descripcionProductoEspecial" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                       <div class="form-row">
                           <div class="form-group col-6">
                               <label for="cantidadProductoEspecial">Cantidad</label>
                               <input  class="form-control" type="number" name="cantidadProductoEspecial" id="cantidadProductoEspecial"  value="1" min="1">
                           </div>
                           <div class="form-group col-6">
                               <label for="colorBaseEspecial">Color Base</label>
                               <select style="width: 100%" class="form-control input-100-percent" name="colorBaseEspecial" id="colorBaseEspecial"></select>
                           </div>
                       </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="colorCombinacionEspecial">Color Combinación</label>
                                <select style="width: 100%" class="form-control input-100-percent" name="colorCombinacionEspecial" id="colorCombinacionEspecial"></select>
                            </div>
                            <div class="form-group col-6">
                                <label for="ladoProductoEspecial">Lado</label>
                                <select name="ladoProductoEspecial" id="ladoProductoEspecial" class="form-control">
                                    <option value="Izquierdo">Izquierdo</option>
                                    <option value="Derecho">Derecho</option>
                                    <option value="Ambos">Ambos</option>
                                    <option value="No aplica" selected>No aplica</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="medidaProductoEspecial">Medida</label>
                                <input  class="form-control" name="medidaProductoEspecial" id="medidaProductoEspecial">
                            </div>
                            <div class="form-group col-6">
                                <label for="precioProductoEspecial">Precio</label>
                                <input type="number" min="0" value="0" class="form-control"  name="precioProductoEspecial" id="precioProductoEspecial">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="descuentoProductoEspecial">Descuento</label>
                                <input type="number" class="form-control" min="0" value="0"  name="descuentoProductoEspecial" id="descuentoProductoEspecial"  >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="descuentoProductoEspecial">Imagen</label>
                                <input type="file" class="form-control" name="imgPathNewAdd" id="imgPathNewAdd"  >
                            </div>
                        </div>

                        <div class="divider"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonSaveProductSpecial" class="btn btn-primary">Agregar</button>
            </div>

        </div>
    </div>
</div>