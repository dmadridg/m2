@extends('main.main')
@section('title',$title)
@section('css')
    <link rel="stylesheet" href="{{asset('css/quotations.css')}}">
@endsection
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewQuotation" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nueva cotizacion</button>
    {!! Form::open(['url'=>'dashboard/quotations','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('clave',null,['class'=>'form-control','placeholder'=>'Buscar por clave','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
    {!! Form::close() !!}
    @if(count($quotations)>0)
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Clave</th>
                    <th scope="col">Pedido</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Fecha de cotización</th>
                    <th scope="col">Transportista</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($quotations as $quotation)
                    <tr>
                        <td><a href="#" id="editCotizacion{{$quotation->IdCotizacion}}" data-id="{{$quotation->IdCotizacion}}" class="editCotizacion btn-light">{{$quotation->IdCotizacion}}</a></td>
                        <td>{{($quotation->IdPedido ==0)? 'N/A':$quotation->IdPedido}}</td>
                        <td>{{ $quotation->client->Nombre }}</td>
                        <td>{{  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$quotation->FechaCotizacion)->toFormattedDateString() }}</td>
                        <td>{{ (isset($quotation->client->carrier))?$quotation->client->carrier->Nombre:'N/A' }}</td>
                        <td>
                            <a href="#" data-id="{{$quotation->IdCotizacion}}" class="btnSendEmail btn btn-sm btn-success"><i class="fa fa-envelope"></i></a>
                            <a href="{{route('printQuotation',['id'=>$quotation->IdCotizacion])}}" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Imprimir</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron cotizaciones, intenta una nueva búsqueda
            </div>
        </div>
    @endif
    {{ $quotations->appends(request()->query())->links() }}
    @include('main.quotations.modalNew')
    @include('main.quotations.modalAddProduct')
    @include('main.quotations.modalAddSpecialProduct')
    @include('main.quotations.modalObservations')

    @include('main.quotations.modalEdit')
    @include('main.quotations.modalAddProductEdit')
    
    @include('main.quotations.modalEditSpecialProductEdit')
    @include('main.quotations.modalEditProductEdit')

    @include('main.quotations.modalAddSpecialProductEdit')
    @include('main.quotations.modalObservationsEdit')

@endsection
@section('js')
    <script>
        $('.btnSendEmail').on("click",function(){
            var id =  $(this).data("id");
            var url = "quotations/sendEmail";
            console.log(id, url);

            $.post( url, { id: id })
            .done(function( data ) {
                // console.log(data);
                if(data){
                    alert( "Se ha mandado el email correctamente.");
                }else{
                    alert( "No se ha podido mandar el email. Intente de nuevo más tarde.");
                }
            });
        });
    </script>
    <script src="{{asset('js/quotations/main.js')}}"></script>
    <script src="{{asset('js/quotations/edit.js')}}"></script>
@endsection