@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewUser" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo usuario</button>

    {!! Form::open(['url'=>'dashboard/users','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($users) > 0)
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Cargo</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td><a href="#" id="editUser{{$user->IdEmpleado}}" data-id="{{$user->IdEmpleado}}" class="editUser btn-light">{{$user->Nombre}}</a></td>
                        <td>{{$user->Email}}</td>
                        @if($user->Cargo === 1 )
                            <td>Administrador</td>
                        @elseif($user->Cargo == 2)
                            <td>Vendedor</td>
                        @else
                            <td>Producción</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron usuarios , intenta una nueva búsqueda
            </div>
        </div>
    @endif
    {{ $users->links() }}
    @include('main.users.new')
    @include('main.users.edit')
@endsection
@section('js')
    <script src="{{asset('js/users/main.js')}}"></script>
@endsection