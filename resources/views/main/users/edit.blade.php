<!-- Modal -->
<div class="modal fade" id="modalEditUser" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editando Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  id="editUserForm" class="form row">
                    <div class="col">
                        <div class="form-group">
                            <label for="NombreEdit">Nombre</label>
                            <input  class="form-control" name="Nombre" id="NombreEdit" placeholder="Ex:1230">
                        </div>
                        <div class="form-group">
                            <label for="EmailEdit">Email</label>
                            <input  class="form-control" name="Email" id="EmailEdit" placeholder="Ex:hola@gmail.com">
                        </div>
                        <div class="form-group">
                            <label for="CargoEdit">Tipo de usuario</label>
                            <select name="Cargo" id="CargoEdit" class="form-control">
                                <option value="1">Administrador</option>
                                <option value="2">Vendedor</option>
                                <option value="3">Producción</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="form-check" style="padding-left: 1.25rem;">
                                <input class="form-check-input" name="EditPassword" type="checkbox" id="EditPassword">
                                <label class="form-check-label" for="EditPassword">
                                    ¿Editar Contraseña?
                                </label>
                            </div>
                        </div>
                        <div class="form group">
                            <label for="PasswordEdit">Contraseña</label>
                            <input type="password"  class="form-control" name="Password" id="PasswordEdit" placeholder="Ex:****">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmitEdit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>


