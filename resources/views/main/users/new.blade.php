<!-- Modal -->
<div class="modal fade" id="modalNewUser" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  id="newUserForm" class="form row">
                    <div class="col">
                        <div class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input  class="form-control" name="Nombre" id="Nombre" placeholder="Ex:1230">
                        </div>
                        <div class="form-group">
                            <label for="Email">Email</label>
                            <input  class="form-control" name="Email" id="Email" placeholder="Ex:hola@gmail.com">
                        </div>
                        <div class="form-group">
                            <label for="Cargo">Tipo de usuario</label>
                            <select name="Cargo" id="Cargo" class="form-control">
                                <option value="1">Administrador</option>
                                <option value="2">Vendedor</option>
                                <option value="">Producción</option>
                            </select>
                        </div>
                        <div class="form group">
                            <label for="Email">Contraseña</label>
                            <input type="password"  class="form-control" name="Password" id="Password" placeholder="Ex:****">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>


