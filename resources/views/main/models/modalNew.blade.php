<!-- Modal -->
<div class="modal fade" id="modalNewModel" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="newModelForm" class="form row" enctype="multipart/form-data">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="Clave">Clave</label>
                            <input  class="form-control" name="Clave" id="Clave" placeholder="Ex:12323">
                        </div>
                        <div class="form-group">
                            <label for="Nombre">Medida</label>
                            <input  class="form-control" name="Nombre" id="Nombre" placeholder="Ex:Formación">
                        </div>
                        <div class="form-group">
                            <label for="Descripcion">Descripción</label>
                            <input  class="form-control" name="Descripcion" id="Descripcion" placeholder="Ex:Descripción">
                        </div>
                        <div class="form-group">
                            <label for="IdLinea">Linea</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdLinea" id="IdLinea"></select>
                        </div>
                        <div class="form-group">
                            <label for="Descripcion">Precio 1</label>
                            <input type="number"  class="form-control" name="Precio1" id="Precio1" placeholder="Ex:1500.00">
                        </div>
                        <div class="form-group">
                            <label for="file">Imagen</label>
                            <input type="file" class="form-control" name="file" id="file">
                            <hr>
                            <p class="text-muted">Imagenes png ó jpg, máximo 2MB</p>
                            <p class="text-muted">Imagen optima 50x80</p>
                        </div>

                    </div>
                    <!--first column -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
