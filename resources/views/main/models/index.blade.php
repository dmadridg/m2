@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dashboard / '.$title)
@section('content')
    <button type="button" id="buttonNewModel" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo Producto</button>
    {!! Form::open(['url'=>'dashboard/models','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('clave',old('clave'),['class'=>'form-control','placeholder'=>'Buscar por clave','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
    {!! Form::close() !!}
    @if(count($models)>0)
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="colo">Imagen</th>
                    <th scope="col">Clave</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Medida</th>
                    <th scope="col">linea</th>
                    <th scope="col">Precio Base</th>
                    @if(\Illuminate\Support\Facades\Auth::user()->Cargo == 1)
                    <th scope="col">Precio 2</th>
                    <th scope="col">Precio 3</th>
                    <th scope="col">Precio 4</th>
                    @endif
                    <th scope="col">F. Modelos</th>
						<th scope="col">Eliminar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($models as $model)
                    <tr>
                        <td>
                            @if($model->Imagen != null)
                                <img src="{{$model->Imagen}}" alt="" width="100" heigh="100">
                                @else
                                N/A
                                @endif

                        </td>
                        <td><a href="#" id="editModel{{$model->IdModelo}}" data-id="{{$model->IdModelo}}" class="editModel btn-light">{{$model->Clave}}</a></td>
                        <td>{{$model->Descripcion}}</td>
                        <td>{{$model->Nombre}}</td>
                        <td>{{($model->line != null)?$model->line->Nombre:'N/A'}}</td>
                        <td>${{$model->Precio1}}</td>
                        @if(\Illuminate\Support\Facades\Auth::user()->Cargo == 1)
                        <td>${{$model->Precio2}}</td>
                        <td>${{$model->Precio3}}</td>
                        <td>${{$model->Precio4}}</td>
                        @endif
                        <td class="text-center"><button class="btn btn-dark buttonAddModelFormation" data-id="{{$model->IdModelo}}" ><i class="fa fa-plus-square"></i></button></td>
							<td class="text-center"><button onClick="deleteModel({{$model->IdModelo}});" class="btn buttonDestroy" data-id="{{$model->IdModelo}}" ><i class="fa fa-warning"></i></button></td>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron modelos, intenta una nueva búsqueda
            </div>
        </div>
    @endif
    {{ $models->appends(request()->query())->links() }}
    @include('main.models.modalNew')
    @include('main.models.modalEdit')
    @include('main.models.modalFormationModels')
@endsection
@section('js')
    <script>
        window.ADMIN_ROLE = "{{env('ADMIN_ROLE')}}";
        window.USER_ROLE = "{{\Illuminate\Support\Facades\Auth::user()->Cargo}}"

        function deleteModel(model) {
            console.log(model);
            bootbox.confirm("¿Desea eliminar éste modelo?", function(result){
                console.log(result);
                console.log('This was logged in the callback: ' + result);

                if(result){
                    $.ajax({
                        url: 'models/destroy',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: model
                        },
                    })
                    .done(function(resp) {
                        if (resp.success) {
                            // swal.hideLoading();
                            console.log("success");
                            location.reload();
                        }else{
                            bootbox.alert("No se pudo eliminar el modelo. Intente de nuevo más tarde.");
                        }
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
                }
            });
        }
    </script>
    <script src="{{asset('js/models/main.js')}}"></script>
@endsection