<!-- Modal -->
<div class="modal fade" id="modalAddModelFormation" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Formación de modelo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form  id="addModelFormationForm">
                        <!--first column -->
                        <div class="col-sm-12">
                            <h3><strong>Por formaciones base</strong></h3>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typeModelRadios" id="exampleRadios1" value="formacion" checked>
                                <label class="form-check-label" for="exampleRadios1">
                                    Formación base
                                </label>
                            </div>
                            <select  class="form-control" name="formacion" style="width:100%;"  id="formacion">

                            </select>
                        </div>
                        <hr>
                        <div class="col-sm-12">
                            <h3><strong>Por Materiales</strong></h3>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typeModelRadios" id="exampleRadios2" value="material">
                                <label class="form-check-label" for="exampleRadios2">
                                    Material
                                </label>
                            </div>
                            <select  class="form-control" name="material" id="material" style="width: 100%" disabled>

                            </select>
                            <hr>
                            <div class="form-group">
                            <label for="IdCategoriaColor" >Categoría de Color</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdCategoriaColor" id="IdCategoriaColor" disabled></select>
                        </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Cantidad">Cantidad</label>
                                <input type="number"  class="form-control" name="Cantidad" id="Cantidad"  value="1" placeholder="Ex:Cantidad" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="IdProceso">Procesos</label>
                                <select name="IdProceso" class="form-control" id="IdProceso" disabled></select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="NombreColor">Es tablero?</label>
                                <select name="EsTablero" class="form-control" id="esTablero">
                                    <option value="0">NO</option>
                                    <option value="1">SI</option>
                                </select>
                            </div>
                        </div>
                        <div id="contBoardOptions" style="display: none;">
                            <div class="form-row">
                                <div class="col-md-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Corte" type="checkbox" id="Corte">
                                        <label class="form-check-label" for="Corte">
                                            Corte
                                        </label>
                                    </div>
                                </div>
                                <div class=" col-3">
                                    <div class="form-check"style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Router" type="checkbox" id="Router">
                                        <label class="form-check-label" for="Router">
                                            Router
                                        </label>
                                    </div>
                                </div>
                                <div class="  col-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Curvos" type="checkbox" id="Curvos">
                                        <label class="form-check-label" for="Curvos">
                                            Curvos
                                        </label>
                                    </div>
                                </div>
                                <div class=" col-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Puertas" type="checkbox" id="Puertas">
                                        <label class="form-check-label" for="Puertas">
                                            Puertas
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Molduras" type="checkbox" id="Molduras">
                                        <label class="form-check-label" for="Molduras">
                                            Molduras
                                        </label>
                                    </div>
                                </div>
                                <div class=" col-3">
                                    <div class="form-check"style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Costados" type="checkbox" id="Costados">
                                        <label class="form-check-label" for="Costados">
                                            Costados
                                        </label>
                                    </div>
                                </div>
                                <div class="  col-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Pantallas" type="checkbox" id="Pantallas">
                                        <label class="form-check-label" for="Pantallas">
                                            Pantallas
                                        </label>
                                    </div>
                                </div>
                                <div class=" col-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Entrepanos" type="checkbox" id="Entrepanos">
                                        <label class="form-check-label" for="Entrepanos">
                                            Entrepaños
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Divisiones" type="checkbox" id="Divisiones">
                                        <label class="form-check-label" for="Divisiones">
                                            Divisiones
                                        </label>
                                    </div>
                                </div>
                                <div class=" col-3">
                                    <div class="form-check"style="padding-left: 1.25rem;">
                                        <input class="form-check-input" name="Archiveros" type="checkbox" id="Archiveros">
                                        <label class="form-check-label" for="Archiveros">
                                            Archiveros
                                        </label>
                                    </div>
                                </div>
                                <div class="  col-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" type="checkbox" name="Barrenos" id="Barrenos">
                                        <label class="form-check-label" for="Barrenos">
                                            Barrenos
                                        </label>
                                    </div>
                                </div>
                                <div class=" col-3">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" type="checkbox" name="Zoclos" id="Zoclos">
                                        <label class="form-check-label" for="Zoclos">
                                            Zoclos
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" type="checkbox" name="ChapasChk" id="ChapasChk" checked>
                                        <label class="form-check-label" for="ChapasChk">
                                            Chapas
                                        </label>
                                    </div>
                                    <select name="Chapas" class="form-control" id="Chapas">
                                        <option value="1">1LC</option>
                                        <option value="2">2LC</option>
                                        <option value="3">1LL</option>
                                        <option value="4">2LL</option>
                                        <option value="5">1LL1LC</option>
                                        <option value="6">1LL2LC</option>
                                        <option value="7">2LL1LC</option>
                                        <option value="8">4L</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="form-check" style="padding-left: 1.25rem;">
                                        <input class="form-check-input" type="checkbox" name="ChapasPVC" id="ChapasPVC">
                                        <label class="form-check-label" for="ChapasPVC">
                                            PVC
                                        </label>
                                    </div>
                                    <select name="PVC" class="form-control" id="PVC" disabled>
                                        <option value="1">1LC</option>
                                        <option value="2">2LC</option>
                                        <option value="3">1LL</option>
                                        <option value="4">2LL</option>
                                        <option value="5">1LL1LC</option>
                                        <option value="6">1LL2LC</option>
                                        <option value="7">2LL1LC</option>
                                        <option value="8">4L</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                      <button type="submit" id="buttonConfirmAddFormation" class="btn btn-primary">Agregar formación</button>
                    </form>
                </div>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Cantidad</th>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>Proceso</th>
                            <th>Es tablero</th>
                            <th>Elementos</th>
                            <th>AxL</th>
                            <th>Eliminar</th>
                        </tr>
                        </thead>
                        <tbody id="detailsInModelFormation">

                        </tbody>
                    </table>
                </div>
                <!--first column -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
