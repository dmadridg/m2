@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewUnit" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nueva unidad de medida</button>

    {!! Form::open(['url'=>'dashboard/units','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($units) > 0)
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($units as $unit)
                    <tr>
                        <td><a href="#" id="editUnit{{$unit->IdUnidadMedida}}" data-id="{{$unit->IdUnidadMedida}}" class="editUnit btn-light">{{$unit->Nombre}}</a></td>
                        <td>{{$unit->Descripcion}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron Unidades de medida, intenta una nueva búsqueda
            </div>
        </div>
        @endif
    {{ $units->links() }}
    @include('main.units.modalNew')
    @include('main.units.modalEdit')
@endsection
@section('js')
    <script src="{{asset('js/units/main.js')}}"></script>
@endsection