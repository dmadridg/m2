<!-- Modal -->
<div class="modal fade" id="modalNewBoard" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Tablero</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  id="newBoardForm" class="form row">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input  class="form-control" name="Nombre" id="Nombre" placeholder="Ex:Mi tablero">
                        </div>
                        <div class="form-group">
                            <label for="Descripcion">Descripcion</label>
                            <input  class="form-control" name="Descripcion" id="Descripcion" placeholder="Ex:Tablero">
                        </div>
                        <div class="form-group">
                            <label for="Espesor">Espesor</label>
                            <input  class="form-control" name="Espesor" id="Espesor" placeholder="Ex:10mm">
                        </div>
                        <div class="form-group">
                            <label for="Costo">Costo</label>
                            <input type="number"  class="form-control" name="Costo" id="Costo" placeholder="Ex:100">
                        </div>
                        <div class="form-group">
                            <label for="Tipo">Tipo</label>
                            <input   class="form-control" name="Tipo" id="Tipo" placeholder="Ex:Tablero">
                        </div>
                    </div>
                    <!--first column -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
