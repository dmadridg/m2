@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewBoard" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nueva Tablero</button>

    {!! Form::open(['url'=>'dashboard/boards','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
   @if(count($boards)>0)
       <div class="table-responsive">
           <table class="table">
               <thead class="thead-dark">
               <tr>
                   <th scope="col">Nombre</th>
                   <th scope="col">Descripción</th>
                   <th scope="col">Espesor</th>
                   <th scope="col">Costo</th>
                   <th scope="col">Tipo</th>
               </tr>
               </thead>
               <tbody>
               @foreach($boards as $board)
                   <tr>
                       <td><a href="#" id="editBoard{{$board->IdTablero}}" data-id="{{$board->IdTablero}}" class="editBoard btn-light">{{$board->Nombre}}</a></td>
                       <th scope="row">{{$board->Descripcion}}</th>
                       <th scope="row">{{$board->Espesor}}</th>
                       <th scope="row">${{ number_format($board->Costo,2,".",",")}}</th>
                       <th scope="row">{{$board->Tipo}}</th>
                   </tr>
               @endforeach
               </tbody>
           </table>
       </div>
       @else
       <div class="table-responsive">
           <div class="alert alert-danger" role="alert">
               No se encontraron Tableros, intenta una nueva búsqueda
           </div>
       </div>
       @endif
    {{ $boards->links() }}
    @include('main.boards.modalNew')
    @include('main.boards.modalEdit')
@endsection
@section('js')
    <script src="{{asset('js/boards/main.js')}}"></script>
@endsection