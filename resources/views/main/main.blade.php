<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Euroespacio | @yield('title')</title>
    <!-- Bootstrap core CSS-->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="{{asset('assets/plugins/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <!-- <link href="{{asset('css/sweetalert2.min.css')}}" rel="stylesheet"> -->
    <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    @yield('css')
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include('partials.nav')
<div class="content-wrapper">
    <div class="preloader">
        <div class="status-preloader">&nbsp;</div>
    </div>
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                @yield('name_section')
            </li>
        </ol>
        <div class="row d-flex justify-content-end my-3">
            <div class="col-md-4" id="search-date">
                @yield('search-space')
                @yield('search-date')
            </div>
        </div>
        @yield('content')
    </div><!-- /.container-fluid-->
</div>    <!-- /.content-wrapper-->
<!-- Bootstrap core JavaScript-->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- <script src="{{asset('assets/app.js')}}"></script> -->

<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Core plugin JavaScript-->
<script src="{{asset('assets/plugins/jquery-easing/jquery.easing.min.js')}}"></script>
<!-- Page level plugin JavaScript-->
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('assets/plugins/jquery/jquery.number.js')}}"></script>
<script src="{{asset('assets/plugins/validate/jquery.validate.min.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="{{asset('js/sb-admin.js')}}"></script>
<!-- Custom scripts for this page-->
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/select2/js/select2.full.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<script>$.fn.bootstrapSwitch.defaults.size = 'mini';</script>
<script>
    const baseUrl = "{{asset('/')}}"
</script>

@yield('js')
</body>
</html>