@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewProcess" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo Proceso</button>

    {!! Form::open(['url'=>'dashboard/processes','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($processes)>0)
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>

                </tr>
                </thead>
                <tbody>
                @foreach($processes as $process)
                    <tr>
                        <td><a href="#" id="editProcess{{$process->IdProceso}}" data-id="{{$process->IdProceso}}" class="editProcess btn-light">{{$process->NombreProceso}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron Procesos, intenta una nueva búsqueda
            </div>
        </div>
        @endif


    {{ $processes->links() }}
    @include('main.processes.modalNew')
    @include('main.processes.modalEdit')
@endsection
@section('js')
    <script src="{{asset('js/processes/main.js')}}"></script>
@endsection