@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
    @section('content')
        <button type="button" id="buttonNewClient" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo cliente</button>

        {!! Form::open(['url'=>'dashboard/clients','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
            <div class="input-group mb-3">
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
                </div>

            </div>
        {!! Form::close() !!}
        @if(count($clients) > 0)
            <div class="table-responsive ">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Clave</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Correo</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <th scope="row">{{$client->ClaveCliente}}</th>
                            <td><a href="#" id="editClient{{$client->IdCliente}}" data-id="{{$client->IdCliente}}" class="editClient btn-light">{{$client->Nombre}}</a></td>
                            <td>{{$client->Telefono}}</td>
                            <td>{{$client->Email}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <div class="table-responsive">
                <div class="alert alert-danger" role="alert">
                    No se encontraron clientes, intenta una nueva búsqueda
                </div>
            </div>

            @endif

    {{ $clients->links() }}
    @include('main.clients.modalNew')
    @include('main.clients.modalEdit')
    @endsection
@section('js')
    <script src="{{asset('js/clients/main.js')}}"></script>
@endsection