<!-- Modal -->
<div class="modal fade" id="modalEditClient" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editando Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="editClientForm" class="form row">
                    <!--first column -->
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input  class="form-control" name="Nombre" id="NombreEdit" placeholder="Ex:Juan Perez">
                        </div>
                        <div class="form-group">
                            <label for="IdEstado">Estado</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdEstado" id="IdEstadoEdit"></select>
                        </div>
                        <div class="form-group">
                            <label for="idCiudad">Ciudad</label>
                            <select style="width: 100%" class="form-control input-100-percen" name="IdCiudad" id="IdCiudadEdit"></select>
                        </div>
                        <div class="form-group">
                            <label for="Telefono">Teléfono</label>
                            <input  class="form-control" name="Telefono" id="TelefonoEdit" placeholder="Ex:3317026377">
                        </div>
                        <div class="form-group">
                            <label for="RFC">RFC</label>
                            <input  class="form-control" name="RFC" id="RFCEdit" placeholder="Ex:FOAF876501XXX">
                        </div>

                    </div>
                    <!--first column -->

                    <!--second column -->
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email">Email</label>
                            <input  class="form-control" name="Email" id="EmailEdit" placeholder="Ex:example@mail.com">
                        </div>
                        <div class="form-group">
                            <label for="idAgente">Agente</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdAgente" id="IdAgenteEdit"></select>
                        </div>
                        <div class="form-group">
                            <label for="Precio">Precio</label>
                            <select class="form-control" name="Precio" id="PrecioEdit">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="Condiciones">Condiciones</label>
                            <input  class="form-control" name="Condiciones" id="CondicionesEdit" placeholder="Ex:----">
                        </div>
                        <div class="form-group">
                            <label for="Descuento">Descuento(%)</label>
                            <input type="number"  class="form-control" name="Descuento" id="DescuentoEdit" placeholder="Ex:10">
                        </div>
                        <div class="form-group">
                            <label for="Consignar">Consignar</label>
                            <select class="form-control" name="Consignar" id="ConsignarEdit">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                    </div>
                    <!--second column -->
                    <!--third column -->
                    <div class="col-4">
                        <div class="form-group">
                            <label for="DomicilioFiscal">Domicilio Fiscal</label>
                            <input  class="form-control" name="DomicilioFiscal" id="DomicilioFiscalEdit" placeholder="Ex:Av. Washington 123">
                        </div>
                        <div class="form-group">
                            <label for="CP">Código postal</label>
                            <input  class="form-control" name="CP" id="CPEdit" placeholder="Ex:44670">
                        </div>
                        <div class="form-group">
                            <label for="CURP">CURP</label>
                            <input  class="form-control" name="CURP" id="CURPEdit" placeholder="Ex:12323">
                        </div>
                        <div class="form-group">
                            <label for="Contacto">Contacto</label>
                            <input  class="form-control" name="Contacto" id="ContactoEdit" placeholder="Ex:Jorge Medina">
                        </div>
                        <div class="form-group">
                            <label for="idTransportista">Transportista</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdTransportista" id="IdTransportistaEdit"></select>
                        </div>
                        <div class="form-group">
                            <label for="DomicilioEntrega">Domicilio de entrega</label>
                            <input  class="form-control" name="DomicilioEntrega" id="DomicilioEntregaEdit" placeholder="Ex:Av. Washington 123">

                        </div>
                    </div>
                    <!--third column -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmitOne" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
