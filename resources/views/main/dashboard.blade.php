@extends('main.main')
@section('title','Panel de control')
@section('css')

@endsection
@section('content')
    <div class="container-fluid mt-5">
        <div class="row text-mobile-center text-tablet-center mx-3">
            <div class="col-md-12">
                <h3 class="text-uppercase title-section">Panel de control</h3>
                <div class="border-line"></div>
            </div>
            <div class="col-md-12 p-0"><div class="msg-alert"></div></div>
            <div class="col-md-12 b-grey-card mt-5">
                <div class="row py-3">
                    <div class="col-md-6 border-right">
                        <div class="ml-sm-5 ml-md-5 ml-lg-5">
                            <div class="row d-flex">
                                <div class="col-md-2 col-sm-12 col-12">
                                    <div class="user-icon-content"><img src="" class="user-icon"></div>
                                </div>
                                <div class="col-md-10 col-sm-12 col-12 data_profile_1">
                                    <h5 class="name-full-user">{{Auth::user()->Nombre}} </h5>
                                    <h6 class="grey-minus">{{Auth::user()->Email}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-9 col-sm-9 data_profile_2">
                                <h6>Contraseña:**********</h6>
                                <span class="engrane"><img src="{{asset('assets/img/front/engrane.png')}}" class="c-pointer" data-toggle="modal" data-target=".update-profile"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/plugins/validate/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/plugins/files/bootstrap-filestyle.js')}}"></script>


@endsection
