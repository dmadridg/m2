@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewMaterial" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nuevo Material</button>
    {!! Form::open(['url'=>'dashboard/materials','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('clave',null,['class'=>'form-control','placeholder'=>'Buscar por clave','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($materials)>0)
   <div class="table-responsive">
       <table class="table">
           <thead class="thead-dark">
           <tr>
               <th scope="col">Clave</th>
               <th scope="col">Nombre</th>
               <th scope="col">Medidas</th>
               <th scope="col">Tipo</th>
               <th scope="col">Costo Unidad</th>
               <th scope="col">Costo Calculado</th>
           </tr>
           </thead>
           <tbody>
           @foreach($materials as $material)
               <tr>
                   <td><a href="#" id="editMaterial{{$material->IdMaterial}}" data-id="{{$material->IdMaterial}}" class="editMaterial btn-light">{{$material->Clave}}</a></td>
                   <td>{{$material->NombreComercial}}</td>
                   @if(($material->Ancho != 0 || $material->Ancho != null) && ($material->Largo != 0 || $material->Largo != null))
                       <td>{{$material->Largo}} x {{$material->Ancho}}</td>
                   @else
                       <td>N/A</td>
                   @endif
                   <td>{{$material->TipoMaterial}}</td>
                   <td>{{($material->TipoMaterial == 'Componente')?'N/A':'$'.$material->CostoPorUnidad}}</td>
                   <td>{{($material->TipoMaterial != 'Componente')?'N/A':'$'.$material->costo_calculado}}</td>
               </tr>
           @endforeach
           </tbody>
       </table>
   </div>
    @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron materiales, intenta una nueva búsqueda
            </div>
        </div>
    @endif
    {{ $materials->appends(request()->query())->links() }}
    @include('main.materials.modalNew')
    @include('main.materials.modalEdit')

@endsection
@section('js')
    <script src="{{asset('js/materials/main.js')}}"></script>
@endsection