<!-- Modal -->
<div class="modal fade" id="modalNewMaterial" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Material</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form  id="newMaterialForm" class="form row">
                    <!--first column -->
                    <div class="col">
                        <div class="form-group">
                            <label for="Clave">Clave</label>
                            <input  class="form-control" name="Clave" id="Clave" placeholder="Ex:12323">
                        </div>
                        <div class="form-group">
                            <label for="Nombre">Nombre comercial</label>
                            <input  class="form-control" name="NombreComercial" id="NombreComercial" placeholder="Ex:Nombre comercial">
                        </div>
                        <div class="form-group">
                            <label for="IdAlmacen">Almacén</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdAlmacen" id="IdAlmacen"></select>
                        </div>
                        <!--<div class="form-group">
                            <label for="IdCategoriaColor">Categoría de Color</label>
                            <select style="width: 100%" class="form-control input-100-percent" name="IdCategoriaColor" id="IdCategoriaColor"></select>
                        </div>-->
                        <hr>
                        <!--TABS-->
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-herrajes-tab" data-selected="1" data-toggle="pill" href="#pills-herrajes" role="tab" aria-controls="pills-herrajes" aria-selected="true">Herrajes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-componentes-tab" data-toggle="pill" data-selected = "2" href="#pills-componentes" role="tab" aria-controls="pills-componentes" aria-selected="false">Componentes</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-herrajes" role="tabpanel" aria-labelledby="pills-herrajes-tab">
                                <div class="form-group">
                                    <label for="IdUnidadMedida">Unidad de medida</label>
                                    <select style="width: 100%" class="form-control input-100-percent" name="IdUnidadMedida" id="IdUnidadMedida"></select>
                                </div>
                                <div class="form-group">
                                    <label for="CostoPorUnidad">Costo x unidad</label>
                                    <input type="number"  class="form-control" name="CostoPorUnidad" id="CostoPorUnidad" placeholder="Ex:1500.00">
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-componentes" role="tabpanel" aria-labelledby="pills-componentes-tab">
                                <div class="form-group">
                                    <label for="Ancho">Ancho</label>
                                    <input type="number"  class="form-control" name="Ancho" id="Ancho" placeholder="Ex:10">
                                </div>
                                <input type="hidden" name="typeMaterial" id="typeMaterial">
                                <div class="form-group">
                                    <label for="Largo">Largo</label>
                                    <input type="number"  class="form-control" name="Largo" id="Largo" placeholder="Ex:20">
                                </div>
                                <div class="form-group">
                                    <label for="IdTablero">Tablero</label>
                                    <select style="width: 100%" class="form-control input-100-percent" name="IdTablero" id="IdTablero"></select>
                                </div>
                                <div class="form-group">
                                    <label for="IdTablero">Costo calculado</label>
                                    <input type="number"  class="form-control" name="CostoCalculado" id="CostoCalculado" placeholder="Ex:1500.00" disabled>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!--first column -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="buttonSubmit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
