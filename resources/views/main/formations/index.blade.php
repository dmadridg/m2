@extends('main.main')
@section('title',$title)
@section('css')
@section('name_section','Dasboard / '.$title)
@section('content')
    <button type="button" id="buttonNewFormation" class="btn btn-outline-primary" style="margin-bottom: 10px;cursor: pointer">Nueva Formación Base</button>

    {!! Form::open(['url'=>'dashboard/formations','method'=>'GET','class'=>'navbar-form pull-right','style'=>'margin-top: 3px;padding:10px 3px;']) !!}
    <div class="input-group mb-3">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar por nombre','aria-describeddy'=>'search']) !!}
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
        </div>

    </div>
    {!! Form::close() !!}
    @if(count($formations)>0)
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Clave</th>
                    <th scope="col">Formación base</th>
                </tr>
                </thead>
                <tbody>
                @foreach($formations as $formation)
                    <tr>
                        <td><a href="#" id="editFormation{{$formation->IdFormacionBase}}" data-id="{{$formation->IdFormacionBase}}" class="editFormation btn-light">{{$formation->Nombre}}</a></td>
                        <th scope="row">{{$formation->Clave}}</th>
                        <td class="text-center"><button class="btn btn-dark buttonAddFormation" data-id="{{$formation->IdFormacionBase}}" ><i class="fa fa-plus-square"></i></button></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert">
                No se encontraron Formaciones base, intenta una nueva búsqueda
            </div>
        </div>
    @endif

    {{ $formations->links() }}
    @include('main.formations.modalNew')
    @include('main.formations.modalEdit')
    @include('main.formations.modalAdd')
@endsection
@section('js')
    <script src="{{asset('js/formations/main.js')}}"></script>
@endsection